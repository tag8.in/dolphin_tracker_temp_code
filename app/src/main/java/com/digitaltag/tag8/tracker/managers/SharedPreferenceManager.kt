package com.digitaltag.tag8.tracker.managers

import android.content.Context
import android.content.SharedPreferences
import com.digitaltag.tag8.tracker.BuildConfig
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.model.BeaconDevice
import com.digitaltag.tag8.tracker.ui.endless.log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object SharedPreferenceManager {

    object TAG8Preferences {

        val PREF_NAME = BuildConfig.APPLICATION_ID

        val prefs: SharedPreferences =
            Tag8Application.getInstance().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

        /**
         * Clears all data in SharedPreferences
         */
        fun clearPrefs() {
            val editor = prefs.edit()
            editor.clear()
            editor.apply()
        }

        /**
         * Remove saved sharedpreference key and value
         */
        fun removeKey(key: String) {
            prefs.edit().remove(key).apply()
        }

        /**
         * Checks if this key exist in sharedpreference
         */
        fun containsKey(key: String): Boolean {
            return prefs.contains(key)
        }

        /**
         * Returns value as string for given specified key
         * and if not exist will return default passed value
         */
        fun getString(key: String, defValue: String?): String? {
            return prefs.getString(key, defValue)
        }

        internal fun setString(key: String, value: String) {
            val editor = prefs.edit()
            editor.putString(key, value)
            editor.apply()
        }

        /**
         * Returns value as int for given specified key
         * and if not exist will return default passed value
         */
        fun getInt(key: String, defValue: Int): Int {
            return prefs.getInt(key, defValue)
        }


        fun setInt(key: String, value: Int) {
            val editor = prefs.edit()
            editor.putInt(key, value)
            editor.apply()
        }

        /**
         * Returns value as long for given specified key
         * and if not exist will return default passed value
         */
        fun getLong(key: String, defValue: Long): Long {
            return prefs.getLong(key, defValue)
        }

        fun setLong(key: String, value: Long) {
            val editor = prefs.edit()
            editor.putLong(key, value)
            editor.apply()
        }

        /**
         * Returns value as boolean for given specified key
         * and if not exist will return default passed value
         */
        fun getBoolean(key: String, defValue: Boolean): Boolean {
            return prefs.getBoolean(key, defValue)
        }

        internal fun setBoolean(key: String, value: Boolean) {
            val editor = prefs.edit()
            editor.putBoolean(key, value)
            editor.apply()
        }

        /**
         * Returns value as float for given specified key
         * and if not exist will return default passed value
         */
        fun getFloat(key: String, defValue: Float): Float {
            return prefs.getFloat(key, defValue)
        }

        fun setFloat(key: String, value: Float?) {
            val editor = prefs.edit()
            editor.putFloat(key, value!!)
            editor.apply()
        }

    }

    private val tag8Preference = TAG8Preferences
    private const val BEACON_DEVICE_LIST = "beacon_device_list"
    private const val BEACON_BLEDEVICE_LIST = "beacon_bledevice_list"
    private const val BEACON_TYPE_IMAGE_LIST = "beacon_type_image_list"
    private const val IS_FIRST_LAUNCH = "is_first_launch"
    private const val RINGTONE_URI = "ringtone_uri"
    private const val RINGTONE_NAME = "ringtone_name"
    private const val SSID = "ssid"
    private const val FROM_TIMER = "from_timer"
    private const val TO_TIMER = "to_timer"
    private const val FIND_MY_PHONE = "find_my_phone"
    private const val TRUSTED_WIFI = "trusted_wifi"
    private const val TRUSTED_WIFI_NAME = "trusted_wifi_name"
    private const val BEARER_TOKEN = "bearer_token"
    private const val LOST_BEACONS = "lost_beacons"
    private const val NEAR_BEACONS = "near_beacons"
    private const val FAR_BEACONS = "far_beacons"
    private const val VERY_FAR_BEACONS = "very_far_beacons"
    private const val BEACON_CONNECTED = "beacon_connected"
    private const val BEACON_DISCONNECTED = "beacon_disconnected"
    private const val BEACON_BOTH = "both"
    private const val TRACKER_OFF = "tracker_off"
    private const val STATUS_TEXT = "status_text"
    private const val TAG_NAME = "tagname"
    private const val BEACON_PACKAGE = "beacon_package_status"
    private const val BEACON_RING_REMEMBER = "beacon_ring_remember"
    private const val BEACON_RING_STATUS = "beacon_ring_status_"
    private const val BEACON_SNOOZE_TIME = "beacon_snooze_time"
    private const val ENABLE_ALERT_BOX = "enable_alert_box"
    private const val USER_FIRST_NAME = "user_first_name"
    private const val USER_LAST_NAME = "user_last_name"
    private const val USER_PHONE_NUMBER = "user_phone_number"
    private const val LAST_ALERT_TYPE = "last_alert_type"
    private const val RINGTONE_URIS = "ringtone_uri"
    private const val RINGTONE_DURATION = "ringtone_duration"
    private const val SLEEP_SWITCH = "sleep_switch"
    private const val USER_PHONE_NUMBER_C_C = "user_phone_number_country_code"
    private const val IS_DISCONNECTED = "is_disconnected"
    private const val ARRIVAL_ALERT = "arrival_alert"
    private const val CONNECTED_BEACON_TIME = "connected_beacon_time"

    fun addOrUpdateDevice(beaconDevice: BeaconDevice?) {
        var devices: HashMap<String, BeaconDevice>? = getDevices()
        if (devices == null) {
            devices = HashMap()
        }
        if (beaconDevice != null) {
            var existingBeaconDevice = devices[beaconDevice.macAddress]
            log(" becon ring enavble ${beaconDevice.enableBeaconRing!!}")
            if (existingBeaconDevice !== null) {
                existingBeaconDevice.type = beaconDevice.type
                existingBeaconDevice.connectionState = beaconDevice.connectionState
                existingBeaconDevice.bleDeviceName = beaconDevice.bleDeviceName
                existingBeaconDevice.enablePhoneRing = beaconDevice.enablePhoneRing
                existingBeaconDevice.enableBeaconRing = beaconDevice.enableBeaconRing
                existingBeaconDevice.latitude = beaconDevice.latitude
                existingBeaconDevice.longitude = beaconDevice.longitude
                existingBeaconDevice.lastUpdated = beaconDevice.lastUpdated
            } else {
                devices[beaconDevice.macAddress] = beaconDevice
            }
            val gsonString = Gson().toJson(devices)
            tag8Preference.setString(BEACON_DEVICE_LIST, gsonString)
        }

    }

    fun getDevices(): HashMap<String, BeaconDevice>? {
        val jsonString = tag8Preference.getString(BEACON_DEVICE_LIST, "") ?: ""
        if (jsonString.isNotEmpty()) {
            return Gson().fromJson(
                jsonString,
                object : TypeToken<HashMap<String, BeaconDevice>>() {}.type
            )
        }
        return null
    }

    fun addTagNameKey(tag8name: HashMap<String, HashMap<String, String>>) {
        val gsonString = Gson().toJson(tag8name)
        tag8Preference.setString(TAG_NAME, gsonString)
    }

    fun getTagNameKey(): HashMap<String, HashMap<String, String>>? {
        val jsonString = tag8Preference.getString(TAG_NAME, "") ?: ""
        if (jsonString.isNotEmpty()) {
            return Gson().fromJson(
                jsonString,
                object : TypeToken<HashMap<String, HashMap<String, String>>>() {}.type
            )
        }
        return null
    }

    fun removeBeacon(beaconDevice: BeaconDevice) {
        var devices: HashMap<String, BeaconDevice>? = getDevices()
        if (devices == null) {
            devices = HashMap()
        }
        devices.remove(beaconDevice.macAddress)
        val gsonString = Gson().toJson(devices)
        tag8Preference.setString(BEACON_DEVICE_LIST, gsonString)
    }

    fun addTypeImage(image: String, type: String) {
        var images: HashMap<String, String>? = getTypeImages()
        if (images == null) {
            images = HashMap()
        }
        images[image] = type
        val gsonString = Gson().toJson(images)
        tag8Preference.setString(BEACON_TYPE_IMAGE_LIST, gsonString)
    }

    fun removeTypeImage(image: String) {
        var images: HashMap<String, String>? = getTypeImages()
        if (images == null) {
            images = HashMap()
        }
        images.remove(image)
        val gsonString = Gson().toJson(images)
        tag8Preference.setString(BEACON_TYPE_IMAGE_LIST, gsonString)
    }

    fun getTypeImages(): HashMap<String, String>? {
        val jsonString = tag8Preference.getString(BEACON_TYPE_IMAGE_LIST, "") ?: ""
        if (jsonString.isNotEmpty()) {
            return Gson().fromJson(
                jsonString,
                object : TypeToken<HashMap<String, String>>() {}.type
            )
        }
        return null
    }

    fun setFirstLaunch() {
        tag8Preference.setBoolean(IS_FIRST_LAUNCH, false)
    }

    fun getFirstLaunch(): Boolean {
        return tag8Preference.getBoolean(IS_FIRST_LAUNCH, true)
    }

    fun setRingtoneUri(uri: String) {
        tag8Preference.setString(RINGTONE_URI, uri)
    }

    fun getRingtoneUri(): String? {
        return tag8Preference.getString(RINGTONE_URI, null)
    }

    fun setRingtoneName(name: String) {
        tag8Preference.setString(RINGTONE_NAME, name)
    }

    fun getRingtoneName(): String? {
        return tag8Preference.getString(RINGTONE_NAME, null)
    }

    fun setSSid(ssid: String) {
        tag8Preference.setString(SSID, ssid)
    }

    fun getSSid(): String? {
        return tag8Preference.getString(SSID, null)
    }

    fun setFromTimer(fromttime: String) {
        tag8Preference.setString(FROM_TIMER, fromttime)
    }

    fun getFromTimer(): String? {
        return tag8Preference.getString(FROM_TIMER, "0")
    }

    fun removeFromTimer() {
        return tag8Preference.removeKey(FROM_TIMER)
    }

    fun setToTimer(fromttime: String) {
        tag8Preference.setString(TO_TIMER, fromttime)
    }

    fun getToTimer(): String? {
        return tag8Preference.getString(TO_TIMER, "0")
    }

    fun removeToTimer() {
        return tag8Preference.removeKey(TO_TIMER)
    }

    fun addStatusText(macAddress: String?, status: String) {
        return tag8Preference.setString(macAddress + STATUS_TEXT, status)
    }

    fun getStatusText(macAddress: String?): String? {
        return tag8Preference.getString(macAddress + STATUS_TEXT, "")
    }

    fun addFindmyPhone(macAddress: String?) {
        return tag8Preference.setBoolean(macAddress + FIND_MY_PHONE, true)
    }

    fun getFindmyPhone(macAddress: String?): Boolean? {
        return tag8Preference.getBoolean(macAddress + FIND_MY_PHONE, false)
    }

    fun removeFindmyPhone(macAddress: String?) {
        return tag8Preference.setBoolean(macAddress + FIND_MY_PHONE, false)
    }

    fun addTrustedwifiswitch() {
        return tag8Preference.setBoolean(TRUSTED_WIFI, true)
    }

    fun getTrustedwifiswitch(): Boolean? {
        return tag8Preference.getBoolean(TRUSTED_WIFI, false)
    }

    fun removeTrustedwifiswitch() {
        return tag8Preference.setBoolean(TRUSTED_WIFI, false)
    }

    fun addTrustedwifi(name: String, type: String?) {
        return tag8Preference.setString(type + TRUSTED_WIFI_NAME, name)
    }

    fun getTrustedwifi(type: String?): String? {
        return tag8Preference.getString(type + TRUSTED_WIFI_NAME, "")
    }

    fun removeTrustedwifi(type: String?) {
        return tag8Preference.removeKey(type + TRUSTED_WIFI_NAME)
    }

    fun getBrearertoken(): String? {
        return tag8Preference.getString(BEARER_TOKEN, "")
    }

    fun addBrearertoken(token: String) {
        return tag8Preference.setString(BEARER_TOKEN, token)
    }


    fun getLostAs(macAddress: String): String? {
        return tag8Preference.getString(macAddress + "lostmacaddress", "")
    }

    fun removeLostAs(macAddress: String): String? {
        return tag8Preference.removeKey(macAddress + "lostmacaddress").toString()
    }

    fun addLostAs(macAddress: String) {
        return tag8Preference.setString(macAddress + "lostmacaddress", LOST_BEACONS)
    }

    fun addFarBeacon(macAddress: String) {
        return tag8Preference.setString(macAddress + "farbeacon", FAR_BEACONS)
    }

    fun getFarBeacon(macAddress: String): String? {
        return tag8Preference.getString(macAddress + "farbeacon", "")
    }

    fun removeFarBeacon(macAddress: String): String? {
        return tag8Preference.removeKey(macAddress + "farbeacon").toString()
    }

    fun addNearBeacon(macAddress: String) {
        return tag8Preference.setString(macAddress + "nearbeacon", NEAR_BEACONS)
    }

    fun getNearBeacon(macAddress: String): String? {
        return tag8Preference.getString(macAddress + "nearbeacon", "")
    }

    fun removeNearBeacon(macAddress: String): String? {
        return tag8Preference.removeKey(macAddress + "nearbeacon").toString()
    }

    fun addveryFarBeacon(macAddress: String) {
        return tag8Preference.setString(macAddress + "vfarbeacon", VERY_FAR_BEACONS)
    }

    fun getveryFarBeacon(macAddress: String): String? {
        return tag8Preference.getString(macAddress + "vfarbeacon", "")
    }

    fun removeveryFarBeacon(macAddress: String): String? {
        return tag8Preference.removeKey(macAddress + "vfarbeacon").toString()
    }

    fun addPhoneAlertStatus(macAddress: String, status: String) {
        return tag8Preference.setString(macAddress + BEACON_CONNECTED, status)
    }

    fun getPhoneAlertStatus(macAddress: String): String? {
        return tag8Preference.getString(macAddress + BEACON_CONNECTED, "")
    }

    fun addtrackerOFF(macAddress: String) {
        return tag8Preference.setBoolean(macAddress + "trackeroff", true)
    }

    fun gettrackerOFF(macAddress: String): Boolean? {
        return tag8Preference.getBoolean(macAddress + "trackeroff", false)
    }

    fun removetrackerOFF(macAddress: String): String? {
        return tag8Preference.removeKey(macAddress + "trackeroff").toString()
    }

    fun addtrackerBattery(macAddress: String, batteryPercentage: String) {
        return tag8Preference.setString(macAddress + "battery", batteryPercentage)
    }

    fun gettrackerBattery(macAddress: String): String? {
        return tag8Preference.getString(macAddress + "battery", "0")
    }

    fun addSnoozeHours(macAddress: String, time: String) {
        tag8Preference.setString(BEACON_SNOOZE_TIME + macAddress, time)
    }

    fun getSnoozeHours(macAddress: String?): String? {
        return tag8Preference.getString(BEACON_SNOOZE_TIME + macAddress, "")
    }

    fun addAlertData(macAddress: String, alertType: String) {
        tag8Preference.setString(BEACON_RING_STATUS + macAddress, alertType)
    }

    fun getFirstTimeConnect(macAddress: String?): Boolean? {
        return tag8Preference.getBoolean(BEACON_RING_REMEMBER + macAddress, false)
    }

    fun addFirstTimeConnect(macAddress: String) {
        tag8Preference.setBoolean(BEACON_RING_REMEMBER + macAddress, true)
    }


    fun connectedTimeConnect(macAddress: String): String? {
        return tag8Preference.getString(CONNECTED_BEACON_TIME + macAddress, "")
    }

    fun connectedTimeConnect(macAddress: String, time: String) {
        tag8Preference.setString(CONNECTED_BEACON_TIME + macAddress, time)
    }

    fun getShowNotiStatus(macAddress: String?): Boolean? {
        return tag8Preference.getBoolean(BEACON_PACKAGE + macAddress, true)
    }

    fun addShowNotiStatus(macAddress: String, status: Boolean) {
        tag8Preference.setBoolean(BEACON_PACKAGE + macAddress, status)
    }

    fun getAlertData(macAddress: String?): String? {
        return tag8Preference.getString(BEACON_RING_STATUS + macAddress, "")
    }

    fun firstName(name: String) {
        tag8Preference.setString(USER_FIRST_NAME, name)
    }

    fun firstName(): String? {
        return tag8Preference.getString(USER_FIRST_NAME, "")
    }

    fun lastName(name: String) {
        tag8Preference.setString(USER_LAST_NAME, name)
    }

    fun lastName(): String? {
        return tag8Preference.getString(USER_LAST_NAME, "")
    }

    fun phoneNumber(name: String) {
        tag8Preference.setString(USER_PHONE_NUMBER, name)
    }

    fun phoneNumber(): String? {
        return tag8Preference.getString(USER_PHONE_NUMBER, "")
    }

    fun phoneNumberCountryCode(name: String) {
        tag8Preference.setString(USER_PHONE_NUMBER_C_C, name)
    }

    fun phoneNumberCountryCode(): String? {
        return tag8Preference.getString(USER_PHONE_NUMBER_C_C, "")
    }

    fun getEnableAlertBox(): Boolean {
        return tag8Preference.getBoolean(ENABLE_ALERT_BOX, false)
    }

    fun setEnableAlertBox(data: Boolean) {
        return tag8Preference.setBoolean(ENABLE_ALERT_BOX, data)
    }

    fun addLastAlertService(macAddress: String?, connectType: String?) {
        return tag8Preference.setString(LAST_ALERT_TYPE + macAddress, connectType.toString())
    }

    fun getLastAlertService(macAddress: String?): String? {
        return tag8Preference.getString(LAST_ALERT_TYPE + macAddress, "")
    }

    fun addRingtoneUri(ringtone: String) {
        return tag8Preference.setString(RINGTONE_URIS, ringtone)
    }

    fun getRingtoneUris(): String? {
        return tag8Preference.getString(RINGTONE_URIS, "")
    }

    fun getRingtoneDuration(): Int {
        return tag8Preference.getInt(RINGTONE_DURATION, 10000)
    }

    fun addRingtoneDuration(duration: Int) {
        return tag8Preference.setInt(RINGTONE_DURATION, duration)
    }

    fun sleepSwitch(): Boolean {
        return tag8Preference.getBoolean(SLEEP_SWITCH, false)
    }

    fun sleepSwitch(value: Boolean) {
        return tag8Preference.setBoolean(SLEEP_SWITCH, value)
    }

    fun isDisconnected(mac_address: String): Boolean {
        return tag8Preference.getBoolean(IS_DISCONNECTED + mac_address, true)
    }

    fun isDisconnected(value: Boolean, mac_address: String) {
        return tag8Preference.setBoolean(IS_DISCONNECTED + mac_address, value)
    }

    fun arrivalAlert(macAddress: String): Boolean {
        return tag8Preference.getBoolean(ARRIVAL_ALERT + macAddress, false)
    }

    fun arrivalAlert(value: Boolean, mac_address: String) {
        return tag8Preference.setBoolean(ARRIVAL_ALERT + mac_address, value)
    }


}