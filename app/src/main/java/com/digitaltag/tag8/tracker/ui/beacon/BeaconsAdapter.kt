package com.digitaltag.tag8.tracker.ui.beacon

import android.content.Context
import android.content.Intent
import android.location.Geocoder
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.baseui.BluetoothConnection.Companion.TAG
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.gettrackerOFF
import com.digitaltag.tag8.tracker.model.BeaconDevice
import com.digitaltag.tag8.tracker.ui.arrival.BeaconMacCheck
import com.digitaltag.tag8.tracker.ui.home.HomeFragment
import com.digitaltag.tag8.tracker.util.BLEType
import com.digitaltag.tag8.tracker.util.Utils
import com.digitaltag.tag8.tracker.util.setSafeOnClickListener
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_device.view.*
import okio.IOException
import java.util.*
import kotlin.collections.ArrayList


class BeaconsAdapter(
    val mContext: Context,
    val beaconAdapterCallback: BeaconAdapterCallback,
    val fragment: HomeFragment
) : RecyclerView.Adapter<BeaconsAdapter.ViewHolder>() {
    private val mDeviceList: ArrayList<BeaconDevice> = ArrayList()

    inner class ViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(beaconDevice: BeaconDevice) {
            containerView.deviceName.text = beaconDevice.bleDeviceName
            Log.d(TAG, "bind: beacon connected ${beaconDevice.isConnected()}")
            val disStatus: Boolean? = gettrackerOFF(beaconDevice.macAddress)
            containerView.mac_address.text = "MacAddress- ${beaconDevice.macAddress}"
            containerView.mac_address.visibility = View.INVISIBLE

            //  containerView.barCode.setImageBitmap(HomeFragment.barCodeButton(beaconDevice.macAddress, containerView.barCode))

            if (beaconDevice.bleType == BLEType.SMART_TRACKER) {
                containerView.beaconimg.visibility = View.VISIBLE
                containerView.lockimg.visibility = View.GONE
            } else if (beaconDevice.bleType == BLEType.SMART_LOCK) {
                containerView.beaconimg.visibility = View.GONE
                containerView.lockimg.visibility = View.VISIBLE
            }

            if (beaconDevice.isConnected()) {
                Tag8Application.arrivalAlertConnectedList.add(BeaconMacCheck(beaconDevice.macAddress))
                containerView.deviceState.text = mContext.getString(R.string.connected)
                containerView.deviceState.setTextColor(
                    connectedColor
                )
                containerView.beaconimg.setImageDrawable(
                    iconConnect
                )
                if (beaconDevice.bleType == BLEType.SMART_LOCK) {
                    containerView.lockimg.setColorFilter(
                        ContextCompat.getColor(
                            mContext,
                            R.color.connected_color
                        )
                    )
                }
                containerView.deviceLastTime.visibility = View.GONE
                containerView.devicemaplocation.visibility = View.GONE
            } else {
                if (beaconDevice.bleType == BLEType.SMART_LOCK) {
                    containerView.lockimg.setColorFilter(
                        ContextCompat.getColor(
                            mContext,
                            R.color.disconnect_color
                        )
                    )
                }
                containerView.deviceState.text = mContext.getString(R.string.disconnected)
                containerView.deviceState.setTextColor(
                    disconnectColor
                )
                containerView.beaconimg.setImageDrawable(
                    disconnectIcon
                )
                containerView.deviceLastTime.text =
                    Utils.getDate(beaconDevice.lastUpdated, "dd/MM/yyyy hh:mm:ss")
                containerView.deviceLastTime.visibility = View.VISIBLE
                containerView.devicemaplocation.visibility = View.VISIBLE
                if (SharedPreferenceManager.getLostAs(beaconDevice.macAddress)!!.isNotEmpty()) {
                    containerView.deviceState.text = mContext.getString(R.string.markas_lost)
                } else if (disStatus!!) {
                    containerView.deviceState.text = mContext.getString(R.string.status_off)
                }
            }
            if (beaconDevice.bleDeviceName.isNotEmpty()) {
                containerView.deviceName.text = beaconDevice.bleDeviceName
            } else {
                containerView.deviceName.text = mContext.getString(R.string.key)
                beaconDevice.bleDeviceName = mContext.getString(R.string.key)
            }
            /*if(fragment.mBluetoothLeService != null){
                fragment.mBluetoothLeService!!.updateEnableBeaconDevice(beaconDevice)
            }*/
            val colorActive = ContextCompat.getColor(mContext, R.color.primary_color)
            val colorInActive = ContextCompat.getColor(mContext, android.R.color.darker_gray)

            if (beaconDevice?.isConnected()) {
                containerView.circleFirst.visibility = View.VISIBLE
                containerView.circleFirstLeft.visibility = View.VISIBLE
                containerView.circleSecond.visibility = View.VISIBLE
                containerView.circleSecondLeft.visibility = View.VISIBLE
                containerView.circleThird.visibility = View.VISIBLE
                containerView.circleThirdLeft.visibility = View.VISIBLE
                when {
                    (beaconDevice?.rssiResult ?: -100) >= -60 -> {
                        containerView.circleFirst.setColorFilter(colorActive)
                        containerView.circleFirstLeft.setColorFilter(colorActive)
                        containerView.circleSecond.setColorFilter(colorActive)
                        containerView.circleSecondLeft.setColorFilter(colorActive)
                        containerView.circleThird.setColorFilter(colorActive)
                        containerView.circleThirdLeft.setColorFilter(colorActive)
                    }
                    (beaconDevice?.rssiResult ?: -100) >= -80 -> {
                        containerView.circleFirst.setColorFilter(colorActive)
                        containerView.circleFirstLeft.setColorFilter(colorActive)
                        containerView.circleSecond.setColorFilter(colorActive)
                        containerView.circleSecondLeft.setColorFilter(colorActive)
                        containerView.circleThird.setColorFilter(colorInActive)
                        containerView.circleThirdLeft.setColorFilter(colorInActive)
                    }
                    else -> {
                        containerView.circleFirst.setColorFilter(colorActive)
                        containerView.circleFirstLeft.setColorFilter(colorActive)
                        containerView.circleSecond.setColorFilter(colorInActive)
                        containerView.circleSecondLeft.setColorFilter(colorInActive)
                        containerView.circleThird.setColorFilter(colorInActive)
                        containerView.circleThirdLeft.setColorFilter(colorInActive)
                    }
                }
            } else {
                containerView.circleFirst.visibility = View.GONE
                containerView.circleFirstLeft.visibility = View.GONE
                containerView.circleSecond.visibility = View.GONE
                containerView.circleSecondLeft.visibility = View.GONE

                containerView.circleThird.visibility = View.GONE
                containerView.circleThirdLeft.visibility = View.GONE
            }


            val batteryLevel =
                SharedPreferenceManager.gettrackerBattery(beaconDevice!!.macAddress)!!.toInt()
            var imageDrawable = Utils.getBatteryImage(batteryLevel)
            containerView.battery.setImageResource(imageDrawable)
            val geocoder = Geocoder(mContext, Locale.getDefault())
            if (beaconDevice.latitude != null && beaconDevice.longitude != null) {
                try {
                    val addressList = geocoder.getFromLocation(
                        beaconDevice.latitude!!.toDouble(),
                        beaconDevice.longitude!!.toDouble(),
                        1
                    )
                    if (addressList != null && addressList.size > 0) {
                        val address = addressList[0]
                        containerView.devicemaplocation.text =
                            Html.fromHtml("<a href=''>${address.getAddressLine(0)}</a>")
                        containerView.devicemaplocation.movementMethod =
                            LinkMovementMethod.getInstance()
                        Log.d("beaconAdapter", "address" + address.getAddressLine(0))
                        var beaconAddress = address.getAddressLine(0)
                    }

                } catch (nullpointer: KotlinNullPointerException) {
                    Log.e("Location Address Loader", "Unable connect get  Geocoder", nullpointer)
                } catch (e: IOException) {
                    Log.e("Location Address Loader", "Unable connect to Geocoder", e)
                }

            }
            Log.d(TAG, "bind: type " + beaconDevice.type)
            when (beaconDevice.type) {
                Constants.Beacon.Type.Key, "" -> {
                    containerView.deviceimg.setImageDrawable(
                        keyDrawable
                    )
                }
                Constants.Beacon.Type.Pet -> {
                    containerView.deviceimg.setImageDrawable(
                        pett
                    )
                }
                Constants.Beacon.Type.Laptop -> {
                    containerView.deviceimg.setImageDrawable(
                        laptop
                    )
                }
                Constants.Beacon.Type.Wallet -> {
                    containerView.deviceimg.setImageDrawable(
                        wallet
                    )
                }
                Constants.Beacon.Type.Car -> {
                    containerView.deviceimg.setImageDrawable(
                        car
                    )
                }
                Constants.Beacon.Type.Passport -> {
                    containerView.deviceimg.setImageDrawable(
                        passportVextor
                    )
                }
                Constants.Beacon.Type.Bag -> {
                    containerView.deviceimg.setImageDrawable(
                        iconBag
                    )
                }
                Constants.Beacon.Type.Remote -> {
                    containerView.deviceimg.setImageDrawable(
                        remot
                    )
                }
                Constants.Beacon.Type.CardHolder -> {
                    containerView.deviceimg.setImageDrawable(
                        cardHolder
                    )
                }
                Constants.Beacon.Type.Camera -> {
                    containerView.deviceimg.setImageDrawable(
                        iconCamera
                    )
                }
                Constants.Beacon.Type.Purse -> {
                    containerView.deviceimg.setImageDrawable(
                        purse
                    )
                }
                Constants.Beacon.Type.Toy -> {
                    containerView.deviceimg.setImageDrawable(
                        iconToy
                    )
                }
                Constants.Beacon.Type.Other -> {
                    containerView.deviceimg.setImageDrawable(
                        otherDrawable
                    )
                }
                else -> {
                    Glide.with(mContext).load(beaconDevice?.type).into(containerView.deviceimg)
                }
            }

            containerView.devicemaplocation.setSafeOnClickListener {
                val intent = Intent(mContext, MapActivity::class.java)
                intent.putExtra(Constants.BEACON_DEVICE, beaconDevice)
                mContext?.startActivity(intent)
            }
            containerView.setSafeOnClickListener {
                beaconAdapterCallback.onClick(beaconDevice)
            }

            if (beaconDevice.connectionState == Constants.Beacon.State.DISTANCE_OFF) {
                beaconAdapterCallback.connect(beaconDevice)
            }


        }
    }

    private val pett = ContextCompat.getDrawable(
        mContext,
        R.drawable.pett
    )
    private val laptop = ContextCompat.getDrawable(
        mContext,
        R.drawable.laptop
    )
    private val wallet = ContextCompat.getDrawable(
        mContext,
        R.drawable.wallet
    )
    private val car = ContextCompat.getDrawable(
        mContext,
        R.drawable.car
    )
    private val passportVextor = ContextCompat.getDrawable(
        mContext,
        R.drawable.passport_vextor
    )
    private val iconBag = ContextCompat.getDrawable(
        mContext,
        R.drawable.ic_bag
    )
    private val remot = ContextCompat.getDrawable(
        mContext,
        R.drawable.remot
    )
    private val cardHolder = ContextCompat.getDrawable(
        mContext,
        R.drawable.ic_card_holder
    )
    private val iconCamera = ContextCompat.getDrawable(
        mContext,
        R.drawable.ic_camer
    )
    private val purse = ContextCompat.getDrawable(
        mContext,
        R.drawable.purse
    )
    private val iconToy = ContextCompat.getDrawable(
        mContext,
        R.drawable.ic_toy
    )

    private val otherDrawable = ContextCompat.getDrawable(
        mContext,
        R.drawable.other
    )
    private val keyDrawable = ContextCompat.getDrawable(
        mContext,
        R.drawable.key
    )
    private val iconConnect = ContextCompat.getDrawable(
        mContext,
        R.drawable.ic_beacon_connect
    )
    private val connectedColor = ContextCompat.getColor(
        mContext,
        R.color.connected_color
    )
    private val disconnectColor = ContextCompat.getColor(
        mContext,
        R.color.disconnect_color
    )
    private val disconnectIcon = ContextCompat.getDrawable(
        mContext,
        R.drawable.ic_beacon_disconnect
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.item_device, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mDeviceList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val beaconDevice = mDeviceList[holder.adapterPosition]
        holder.bind(beaconDevice)
    }

    fun addDevices(deviceList: List<BeaconDevice>) {
        mDeviceList.addAll(deviceList)
        notifyDataSetChanged()
        fragment.updateView()
    }

    fun addDevice(beaconDevice: BeaconDevice) {
        if (!mDeviceList.contains(beaconDevice)) {
            mDeviceList.add(beaconDevice)
            notifyItemInserted(itemCount - 1)
            fragment.updateView()
        }
    }

    fun updateDevice(beaconDevice: BeaconDevice) {
        Log.d(TAG, "updateDevice: ")
        var isExist = false
        for (i in mDeviceList.indices) {
            val device = mDeviceList[i]
            if (beaconDevice.macAddress == device.macAddress) {
                device.rssiResult = beaconDevice.rssiResult
                device.bleDeviceName = beaconDevice.bleDeviceName
                device.connectionState = beaconDevice.connectionState
                device.type = beaconDevice.type
                isExist = true
                notifyItemChanged(i)
                break
            }
        }
        if (!isExist) {
            addDevice(beaconDevice)
        } else {
            fragment.updateView()
        }

    }

    fun updateConnectionFailed(specificMacAddress: String?) {
        for (i in mDeviceList.indices) {
            val device = mDeviceList[i]
            if (specificMacAddress == device.macAddress) {
                device.connectionState = Constants.Beacon.State.DISCONNECTED
            }
        }
    }

    fun removeBeacon(beaconDevice: BeaconDevice) {
        Log.d(TAG, "removeBeacon: removed")
        for (i in mDeviceList.indices) {
            val device = mDeviceList[i]
            if (beaconDevice.macAddress == device.macAddress) {
                mDeviceList.removeAt(i)
                notifyItemRemoved(i)
                break
            }
        }
        fragment.updateView()
    }

    fun updateAllDevices(deviceList: MutableCollection<BeaconDevice>) {
        for (device in deviceList) {
            for (existingDevice in mDeviceList) {
                if (existingDevice == device) {
                    existingDevice.connectionState = device.connectionState
                    existingDevice.bleDeviceName = device.bleDeviceName
                    existingDevice.type = device.type
                    existingDevice.rssiResult = device.rssiResult
                }
            }
        }
        notifyDataSetChanged()
        fragment.updateView()
    }

    interface BeaconAdapterCallback {
        fun onClick(beaconDevice: BeaconDevice)
        fun connect(beaconDevice: BeaconDevice)
    }

}