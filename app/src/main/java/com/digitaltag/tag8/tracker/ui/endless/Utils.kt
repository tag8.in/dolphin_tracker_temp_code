package com.digitaltag.tag8.tracker.ui.endless

import android.util.Log

fun log(msg: String) {
    Log.d("TAG8_TRACKER", msg)
}