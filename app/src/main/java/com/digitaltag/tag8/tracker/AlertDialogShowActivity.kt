package com.digitaltag.tag8.tracker

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.digitaltag.tag8.tracker.ui.endless.log
import com.digitaltag.tag8.tracker.util.Utils
import com.digitaltag.tag8.tracker.util.unRingListener
import com.soumya.meats4all.utils.DialogButtonsCallBack

class AlertDialogShowActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.activity_alert_dialog_show)

        window.addFlags(WindowManager.LayoutParams.FLAG_SECURE)

        val type = intent.getStringExtra("ring_alarm")
        val name = intent.getStringExtra("beacon_name")

        if (type == "ring") {
            Tag8Application.getInstance().getActivity().runOnUiThread {
                log("tag8 ${Tag8Application.getInstance().getActivity()}")
                Utils.createAlertDialog(
                    this,
                    resources.getString(R.string.app_name),
                    "'$name' is calling you.",
                    "STOP",
                    "",
                    object : DialogButtonsCallBack {
                        override fun setCallback(isPositive: Boolean) {
                            unRingListener.ring.stop()
                            finish()
                        }
                    })
            }
        }
    }
}