package com.digitaltag.tag8.tracker.ui.lock

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.ble.BluetoothLeService
import com.digitaltag.tag8.tracker.util.setSafeOnClickListener
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.activity_editdetail.*
import kotlinx.android.synthetic.main.activity_editdetail.backarrow
import kotlinx.android.synthetic.main.activity_lock.*

class LockActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lock)


        backarrow.setSafeOnClickListener {
            finish()
        }
        changeBtn.setOnClickListener {
            if(old_pw_txt.text.toString().length < 6){
                Toast.makeText(this,"Your old password is wrong", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if(new_pw_txt.text.toString().length<6){
                Toast.makeText(this,"Your new password should be more than 5 characters", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if(new_pw_cnf_txt.text.toString().length<6){
                Toast.makeText(this,"Your confirm password should be more than 5 characters", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if(new_pw_txt.text.toString()!=new_pw_cnf_txt.text.toString()) {
                Toast.makeText(this,"New password did not matched.", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            MaterialAlertDialogBuilder(this)
                .setTitle(resources.getString(R.string.confirm))
              //  .setMessage(resources.getString(R.string.confirm_lock_desc))
                .setPositiveButton(resources.getString(R.string.ok)) { dialog, which ->
                    //Tag8BluetoothGatt().startChangePassword(old_pw_txt.text.toString(),new_pw_cnf_txt.text.toString())
                }
                .show()

            BluetoothLeService().changeLockPassword(old_pw_txt.text.toString(), new_pw_cnf_txt.text.toString())
        }
    }
}