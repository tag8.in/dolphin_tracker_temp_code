package com.digitaltag.tag8.tracker.ui.arrival

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application.Companion.arrivalAlertConnectedList
import com.digitaltag.tag8.tracker.baseui.BluetoothConnection.Companion.TAG
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.gettrackerOFF
import com.digitaltag.tag8.tracker.model.BeaconDevice
import com.digitaltag.tag8.tracker.util.Utils
import com.digitaltag.tag8.tracker.util.Utils.calculateTimestamp
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.arrival_item_device.view.*
import kotlinx.android.synthetic.main.item_device.view.deviceName
import kotlinx.android.synthetic.main.item_device.view.deviceState
import kotlinx.android.synthetic.main.item_device.view.deviceimg


class ArrivalBeaconsAdapter(
    val mContext: Context,
    val beaconAdapterCallback: BeaconAdapterCallback,
    val fragment: ArrivalFragment
) : RecyclerView.Adapter<ArrivalBeaconsAdapter.ViewHolder>() {
    private val mDeviceList: ArrayList<BeaconDevice> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.arrival_item_device, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mDeviceList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val beaconDevice = mDeviceList[holder.adapterPosition]
        holder.bind(beaconDevice)
    }

    fun addDevices(deviceList: List<BeaconDevice>) {
        mDeviceList.addAll(deviceList)
        notifyDataSetChanged()
        fragment.updateView()
    }

    fun addDevice(beaconDevice: BeaconDevice) {
        if (!mDeviceList.contains(beaconDevice)) {
            mDeviceList.add(beaconDevice)
            notifyItemInserted(itemCount)
            fragment.updateView()
        }
    }

    fun updateDevice(beaconDevice: BeaconDevice) {
        Log.d(TAG, "updateDevice: ")
        var isExist = false
        for (i in mDeviceList.indices) {
            val device = mDeviceList[i]
            if (beaconDevice.macAddress == device.macAddress) {
                device.rssiResult = beaconDevice.rssiResult
                device.bleDeviceName = beaconDevice.bleDeviceName
                device.connectionState = beaconDevice.connectionState
                device.type = beaconDevice.type
                isExist = true
                notifyItemChanged(i)
                break
            }
        }
        if (!isExist) {
            addDevice(beaconDevice)
        } else {
            fragment.updateView()
        }

    }

    fun updateConnectionFailed(specificMacAddress: String?) {
        for (i in mDeviceList.indices) {
            val device = mDeviceList[i]
            if (specificMacAddress == device.macAddress) {
                device.connectionState = Constants.Beacon.State.DISCONNECTED
            }
        }
    }

    fun removeBeacon(beaconDevice: BeaconDevice) {
        Log.d(TAG, "removeBeacon: removed")
        for (i in mDeviceList.indices) {
            val device = mDeviceList[i]
            if (beaconDevice.macAddress == device.macAddress) {
                mDeviceList.removeAt(i)
                notifyItemRemoved(i)
                break
            }
        }
        fragment.updateView()
    }

    fun updateAllDevices(deviceList: MutableCollection<BeaconDevice>) {
        for (device in deviceList) {
            for (existingDevice in mDeviceList) {
                if (existingDevice == device) {
                    existingDevice.connectionState = device.connectionState
                    existingDevice.bleDeviceName = device.bleDeviceName
                    existingDevice.type = device.type
                    existingDevice.rssiResult = device.rssiResult
                }
            }
        }
        notifyDataSetChanged()
        fragment.updateView()
    }

    inner class ViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(beaconDevice: BeaconDevice) {
            containerView.deviceName.text = beaconDevice.bleDeviceName
            Log.d(TAG, "bind: beacon connected ${beaconDevice.isConnected()}")
            val disStatus: Boolean? = gettrackerOFF(beaconDevice.macAddress)

            containerView.mainCard.setOnClickListener {
                if (beaconDevice.isConnected()) {
                    ArrivalFragment.arrivalList.dialog(beaconDevice)
                } else {
                    Toast.makeText(
                        mContext,
                        "Look's like package is far from your device.",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            if (beaconDevice.isConnected()) {
                ArrivalFragment.arrivalList.connected(beaconDevice)
                if (!arrivalAlertConnectedList.contains(BeaconMacCheck(beaconDevice.macAddress))) {
                    arrivalAlertConnectedList.add(BeaconMacCheck(beaconDevice.macAddress))
                }
                containerView.expectedTime.visibility = View.VISIBLE
                containerView.deviceState.text = mContext.getString(R.string.arrived)
                containerView.deviceState.setTextColor(
                    ContextCompat.getColor(
                        mContext,
                        R.color.green
                    )
                )
            } else {
                arrivalAlertConnectedList.remove(BeaconMacCheck(beaconDevice.macAddress))
                containerView.expectedTime.visibility = View.GONE
                containerView.deviceState.text = mContext.getString(R.string.expected)
                containerView.deviceState.setTextColor(
                    ContextCompat.getColor(
                        mContext,
                        R.color.red
                    )
                )
            }

            if (beaconDevice.bleDeviceName.isNotEmpty()) {
                containerView.deviceName.text = beaconDevice.bleDeviceName
            } else {
                containerView.deviceName.text = mContext.getString(R.string.key)
                beaconDevice.bleDeviceName = mContext.getString(R.string.key)
            }
            when (beaconDevice.type) {
                Constants.Beacon.Type.Key, "" -> {
                    containerView.deviceimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            mContext,
                            R.drawable.key
                        )
                    )
                }
                Constants.Beacon.Type.Pet -> {
                    containerView.deviceimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            mContext,
                            R.drawable.pett
                        )
                    )
                }
                Constants.Beacon.Type.Laptop -> {
                    containerView.deviceimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            mContext,
                            R.drawable.laptop
                        )
                    )
                }
                Constants.Beacon.Type.Wallet -> {
                    containerView.deviceimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            mContext,
                            R.drawable.wallet
                        )
                    )
                }
                Constants.Beacon.Type.Car -> {
                    containerView.deviceimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            mContext,
                            R.drawable.car
                        )
                    )
                }
                Constants.Beacon.Type.Passport -> {
                    containerView.deviceimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            mContext,
                            R.drawable.passport_vextor
                        )
                    )
                }
                Constants.Beacon.Type.Bag -> {
                    containerView.deviceimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            mContext,
                            R.drawable.ic_bag
                        )
                    )
                }
                Constants.Beacon.Type.Remote -> {
                    containerView.deviceimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            mContext,
                            R.drawable.remot
                        )
                    )
                }
                Constants.Beacon.Type.CardHolder -> {
                    containerView.deviceimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            mContext,
                            R.drawable.ic_card_holder
                        )
                    )
                }
                Constants.Beacon.Type.Camera -> {
                    containerView.deviceimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            mContext,
                            R.drawable.ic_camer
                        )
                    )
                }
                Constants.Beacon.Type.Purse -> {
                    containerView.deviceimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            mContext,
                            R.drawable.purse
                        )
                    )
                }
                Constants.Beacon.Type.Toy -> {
                    containerView.deviceimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            mContext,
                            R.drawable.ic_toy
                        )
                    )
                }
                Constants.Beacon.Type.Other -> {
                    containerView.deviceimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            mContext,
                            R.drawable.other
                        )
                    )
                }
                else -> {
                    Glide.with(mContext).load(beaconDevice?.type).into(containerView.deviceimg)
                }
            }

            if (beaconDevice.connectionState == Constants.Beacon.State.DISTANCE_OFF) {
                beaconAdapterCallback.connect(beaconDevice)
            }

            try {
                val dateDate =
                    SharedPreferenceManager.connectedTimeConnect(beaconDevice.macAddress)?.toLong()

                containerView.deviceDate.text =
                    dateDate?.let { Utils.getDateTimestampMs(it, "E, dd MMM yyy") }
                containerView.timeTxt.text =
                    dateDate?.let { Utils.getDateTimestampMs(it, "hh:mm a") }


                containerView.expectedTime.text =
                    if (calculateTimestamp(dateDate.toString().trim()) != "0") {
                        calculateTimestamp(dateDate.toString()) + " MIN"
                    } else {
                        "JUST NOW"
                    }
            } catch (e: Exception) {
                Log.d(TAG, "bind: Error ${e.message}")
                e.message
            }
        }
    }

    interface BeaconAdapterCallback {
        fun onClick(beaconDevice: BeaconDevice)
        fun connect(beaconDevice: BeaconDevice)
    }

}