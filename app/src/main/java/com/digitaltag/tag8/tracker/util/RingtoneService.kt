package com.digitaltag.tag8.tracker.util

import android.app.Service
import android.content.Intent
import android.media.AudioAttributes
import android.media.AudioManager
import android.os.IBinder
import android.media.MediaPlayer

import android.media.RingtoneManager
import android.os.Handler
import android.os.Looper
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager

class RingtoneService : Service() {

    private val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
    private val handler = Handler(Looper.myLooper()!!)
    private lateinit var player: MediaPlayer

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        player = MediaPlayer.create(this, notification)
        player.isLooping = true
        player.start()

        handler.postDelayed({
            player.stop()
            this.stopSelf()
        }, SharedPreferenceManager.getRingtoneDuration().toLong())

    }

    override fun onDestroy() {
        super.onDestroy()
        player.stop()
    }
}