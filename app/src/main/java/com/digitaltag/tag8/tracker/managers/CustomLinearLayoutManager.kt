package com.digitaltag.tag8.tracker.managers

import android.content.Context
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class CustomLinearLayoutManager(context: Context) : LinearLayoutManager(context) {

    var canScroll = true

    override fun onLayoutChildren(recycler: RecyclerView.Recycler?, state: RecyclerView.State?) {
        try {
            super.onLayoutChildren(recycler, state)
        } catch (e: IndexOutOfBoundsException) {
            Log.e("CustomLinearLayout", "meet a IOOBE in RecyclerView")
        }
    }

    override fun canScrollHorizontally(): Boolean {
        return super.canScrollHorizontally() && canScroll
    }

    override fun canScrollVertically(): Boolean {
        return super.canScrollVertically() && canScroll
    }


}