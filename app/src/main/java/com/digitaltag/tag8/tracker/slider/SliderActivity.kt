package com.digitaltag.tag8.tracker.slider

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.digitaltag.tag8.tracker.BuildConfig
import com.digitaltag.tag8.tracker.NavigationActivity
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.firebase.FirebaseAuthManager
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.ui.signup.RegisterActivity
import com.digitaltag.tag8.tracker.util.ReleaseSHAKey
import com.digitaltag.tag8.tracker.util.Utils
import com.digitaltag.tag8.tracker.util.setSafeOnClickListener
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.security.ProviderInstaller
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_slider.*
import java.security.NoSuchAlgorithmException
import javax.net.ssl.SSLContext

class SliderActivity : AppCompatActivity() {
    var currentpage = 0
    private var firebaseUser: FirebaseUser? = null
    private lateinit var auth: FirebaseAuth
    var slideRunnable = Runnable {
        viewpager!!.currentItem = currentpage + 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_slider)
        Tag8Application.getInstance().setActivity(this)
        auth = Firebase.auth
        val uid = auth.currentUser?.uid

        initializeSSLContext(this)

        if (BuildConfig.DEBUG) {
//            if (SharedPreferenceManager.firstName().toString().isEmpty() ||
//                SharedPreferenceManager.lastName().toString().isEmpty() ||
//                SharedPreferenceManager.phoneNumber().toString().isEmpty()
//            ) {
//                startActivity(Intent(this, RegisterActivity::class.java))
//                finish()
//                return
//            }
            startActivity(Intent(this, NavigationActivity::class.java))
            finish()
        }

        if (uid != null) {
            if (uid.isNotEmpty()) {
//                if (SharedPreferenceManager.firstName().toString().isEmpty() ||
//                    SharedPreferenceManager.lastName().toString().isEmpty() ||
//                    SharedPreferenceManager.phoneNumber().toString().isEmpty()
//                ) {
//                    startActivity(Intent(this, RegisterActivity::class.java))
//                    finish()
//                    return
//                }
                startActivity(Intent(this, NavigationActivity::class.java))
                finish()
            }
        }
        ReleaseSHAKey.getSig(this, "SHA")
        val sliderAdapter = SliderAdapter(this)
        viewpager!!.adapter = sliderAdapter
        viewpager!!.addOnPageChangeListener(listener)
        viewpager!!.currentItem = 0
        val mainHandler = Handler(Looper.getMainLooper())
        mainHandler.post { mainHandler.postDelayed(slideRunnable, 10000) }
        firebaseUser = FirebaseAuthManager.getCurrentUser()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken("402836644144-v8fp3v5js12maa5orev6lmsficl0q1m0.apps.googleusercontent.com")
            .requestEmail()
            .build()
        val googleSignInClient = GoogleSignIn.getClient(this@SliderActivity, gso)

        cvGoogle?.setSafeOnClickListener {
            val signInIntent = googleSignInClient?.signInIntent
            startActivityForResult(signInIntent, Constants.RC_SIGN_IN)

            /*
                if (firebaseUser == null) {
                    val signInIntent = googleSignInClient?.signInIntent
                    startActivityForResult(signInIntent, Constants.RC_SIGN_IN)
                    if (signInIntent == null) {
                        Toast.makeText(this@SliderActivity, "Unable to login", Toast.LENGTH_SHORT).show()
                    } else {
                        startActivityForResult(signInIntent, Constants.RC_SIGN_IN)
                    }
                } else {
                    finish()
                    startActivity(Intent(this@SliderActivity, NavigationActivity::class.java))
                }*/
        }
    }

    override fun onResume() {
        super.onResume()
        Tag8Application.getInstance().setActivity(this)
    }

    private var listener: OnPageChangeListener = object : OnPageChangeListener {
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
        }

        override fun onPageSelected(position: Int) {
            currentpage = position
            cvGoogle.visibility = View.VISIBLE

        }

        override fun onPageScrollStateChanged(state: Int) {}
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            FirebaseAuthManager.firebaseAuthWithGoogle(this@SliderActivity, task)
        }
    }

    fun onGoogleLoginSuccess(user: FirebaseUser?) {
        firebaseUser = user
        if (firebaseUser == null) {
            Toast.makeText(this, "Unable to login", Toast.LENGTH_SHORT).show()
        } else {
            if (Utils.checkNetwork()) {
                Toast.makeText(
                    this,
                    "Welcome to tag8 Tracker " + firebaseUser?.displayName,
                    Toast.LENGTH_SHORT
                ).show()

                finish()
                startActivity(Intent(this, RegisterActivity::class.java))
                if (user != null) {
                    Log.d(
                        "SliderActivity",
                        "ResponseData: token uid " + SharedPreferenceManager.TAG8Preferences.prefs.getString(
                            "usertoken",
                            ""
                        ) + " " + user.uid
                    )
                }
            }

        }
    }

    fun newLogin() {
        Log.d("SliderActivity", "Inside new Login")
        var email = firebaseUser?.email
        var uid = firebaseUser?.uid

        Log.d("SliderActivity", "email : " + email + " uid " + uid)

        if (Utils.checkNetwork()) {
            Toast.makeText(
                this,
                "Welcome to tag8 Tracker " + firebaseUser?.displayName,
                Toast.LENGTH_SHORT
            ).show()

            finish()
            startActivity(Intent(this, NavigationActivity::class.java))
        }
    }

    fun onGoogleLoginFail(message: String) {
        Toast.makeText(this, "Unable to login", Toast.LENGTH_SHORT).show()
    }

    private fun initializeSSLContext(mContext: Context) {
        try {
            SSLContext.getInstance("TLSv1.2")
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }

        try {
            ProviderInstaller.installIfNeeded(mContext.applicationContext)
        } catch (e: GooglePlayServicesRepairableException) {
            e.printStackTrace()
        } catch (e: GooglePlayServicesNotAvailableException) {
            e.printStackTrace()
        }

    }

}