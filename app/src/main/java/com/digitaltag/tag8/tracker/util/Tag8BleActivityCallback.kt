package com.digitaltag.tag8.tracker.util

import com.digitaltag.tag8.tracker.ble.Tag8BluetoothGatt

interface Tag8BleActivityCallback {
    fun onActivityCallback(bledevice : Tag8BluetoothGatt)
}