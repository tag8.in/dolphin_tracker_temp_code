package com.digitaltag.tag8.tracker.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.apis.ApiLiveData
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.databinding.FragmentMainBeaconNewBinding
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.addLostAs
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.getLostAs
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.removeLostAs
import com.digitaltag.tag8.tracker.model.BeaconDevice
import com.digitaltag.tag8.tracker.ui.beacon.BeaconActivity
import com.digitaltag.tag8.tracker.ui.beacon.MapActivity
import com.digitaltag.tag8.tracker.ui.camera.CameraxActivity
import com.digitaltag.tag8.tracker.util.BLEType
import com.digitaltag.tag8.tracker.util.IUpdateBeaconFrag
import com.digitaltag.tag8.tracker.util.Utils
import com.digitaltag.tag8.tracker.util.setSafeOnClickListener
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_beacon.*


class MainBeaconFragment : Fragment(), IUpdateBeaconFrag {

    private var beaconDevice: BeaconDevice? = null
    private val auth = Firebase.auth
    private val uid = auth.currentUser?.uid
    private lateinit var mActivity: BeaconActivity
    private var latitude: String = ""
    private var longitude: String = ""
    private lateinit var binding: FragmentMainBeaconNewBinding


    companion object {
        fun newInstance(beaconDevice: BeaconDevice): MainBeaconFragment {
            return MainBeaconFragment().apply {
                val bundle = Bundle().apply {
                    putParcelable(Constants.BEACON_DEVICE, beaconDevice)
                }
                arguments = bundle
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main_beacon_new, container, false);
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMainBeaconNewBinding.bind(view)
        mActivity = activity as BeaconActivity

        mActivity.setUpdateCallback(this)
        beaconDevice = arguments?.getParcelable(Constants.BEACON_DEVICE)
        setProperty()

        binding.btnRing?.setSafeOnClickListener {
            if (activity is BeaconActivity) {
                if (beaconDevice != null) {
                    (activity as BeaconActivity).connectOrRing(beaconDevice)
                }
            }
        }

        if (beaconDevice?.bleType == BLEType.SMART_LOCK) {
            binding.firstCardImg.setImageResource(R.drawable.ic_light_led)
            binding.btnRingText.text = "Flash LED"
            binding.secondCardImage.setImageResource(R.drawable.ic_unlock_)
            binding.btnSelfieLostText.text = "Unlock"
        }

        binding.disconnectplacebutton.setSafeOnClickListener {
            val intent = Intent(getActivity(), MapActivity::class.java)
            intent.putExtra(Constants.BEACON_DEVICE, beaconDevice)
            activity?.startActivity(intent)
        }
        binding.btnSelfieLost.setSafeOnClickListener {
            if (binding.btnSelfieLostText.text == resources.getString(R.string.selfie) ||
                binding.btnSelfieLostText.text == "Unlock"
            ) {
                if (beaconDevice?.bleType == BLEType.SMART_LOCK) {
                    (activity as BeaconActivity).unlockBeacon(beaconDevice)
                    return@setSafeOnClickListener
                }
                openCamera()
            } else {
                if (Utils.checkNetwork()) {
                    if (getLostAs(beaconDevice!!.macAddress).equals("lost_beacons")) {
                        MaterialAlertDialogBuilder(requireActivity())
                            .setTitle(resources.getString(R.string.revokemarkaslost))
                            .setMessage(
                                "After you revoke '" + beaconDevice!!.bleDeviceName + "' lost declaration, you will not receive notification when "
                                        + "it is found by Tracking networking."
                            )
                            .setNegativeButton(resources.getString(R.string.cancel)) { dialog, which ->
                                dialog.dismiss()
                            }
                            .setPositiveButton(resources.getString(R.string.ok)) { dialog, which ->

                                binding.btnSelfieLostText.text = getString(R.string.markaslost)
                                removeLostAs(beaconDevice?.macAddress.toString())
                                mActivity.status.text =
                                    SharedPreferenceManager.getStatusText(beaconDevice!!.macAddress)

                            }
                            .show()
                    } else {
                        MaterialAlertDialogBuilder(requireActivity())
                            .setTitle(resources.getString(R.string.markaslost))
                            .setMessage(
                                "After you mark this '" + beaconDevice!!.bleDeviceName + "' as lost our network will help you search it."
                                        + "You will be notified as soon as we find it."
                            )
                            .setNegativeButton(resources.getString(R.string.cancel)) { dialog, which ->
                                dialog.dismiss()
                            }
                            .setPositiveButton(resources.getString(R.string.ok)) { dialog, which ->

                                binding.btnSelfieLostText.text =
                                    getString(R.string.revokemarkaslost)
                                addLostAs(beaconDevice?.macAddress.toString())
                                mActivity.status.text = getString(R.string.markas_lost)
                                ApiLiveData.reportLost(beaconDevice!!.macAddress)
                            }
                            .show()
                    }
                }

            }
        }
        setDeviceData()
    }

    fun openCamera() {
        startActivity(Intent(context, CameraxActivity::class.java).apply {
            putExtra(Constants.BEACON_DEVICE, beaconDevice)
        })
    }

    fun setProperty() {
        if (beaconDevice?.isConnected() == true) {
            binding.disconnectplacebutton?.visibility = View.GONE
            binding.btnRing?.visibility = View.VISIBLE
            binding.btnRingText?.text = resources.getString(R.string.ring)
            binding.btnSelfieLostText?.text = resources.getString(R.string.selfie)
            if (beaconDevice?.bleType == BLEType.SMART_LOCK) {
                binding.btnRingText.text = "Flash LED"
                binding.btnSelfieLostText?.text = "Unlock"
            }
        } else {
            binding.disconnectplacebutton?.visibility = View.VISIBLE
            binding.btnRing?.visibility = View.GONE
            //   btnRing?.text = resources.getString(R.string.connect)
            binding.btnSelfieLostText?.text = resources.getString(R.string.markaslost)
            mActivity.status.text = SharedPreferenceManager.getStatusText(beaconDevice!!.macAddress)
            if (getLostAs(beaconDevice!!.macAddress).equals("lost_beacons")) {
                binding.btnSelfieLostText.text = getString(R.string.revokemarkaslost)
                mActivity.status.text = getString(R.string.markas_lost)
            }
        }
    }

    fun updateBeaconDevice(beaconDevice: BeaconDevice?) {
        if (beaconDevice != null) {
            this.beaconDevice = beaconDevice
            setDeviceData()
        }
    }

    private fun setDeviceData() {
        when (beaconDevice?.connectionState) {
            Constants.Beacon.State.CONNECTED -> {
                binding.disconnectplacebutton?.visibility = View.GONE
                if (beaconDevice?.isBeaconAlerting == true) {
                    binding.btnRingText?.text = resources.getString(R.string.unring)
                } else {
                    binding.btnRingText?.text = resources.getString(R.string.ring)
                    if (beaconDevice?.bleType == BLEType.SMART_LOCK) {
                        binding.btnRingText.text = "Flash LED"
                    }
                }
            }
            else -> {
                //  disconnectplacebutton?.visibility = View.VISIBLE
                //    btnRing?.text = resources.getString(R.string.connect)
            }
        }
    }

    override fun onUpdate(beaconDevice: BeaconDevice) {
        this.beaconDevice = beaconDevice
        setProperty()
    }

}