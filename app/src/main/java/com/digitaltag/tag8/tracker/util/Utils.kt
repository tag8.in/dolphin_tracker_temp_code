package com.digitaltag.tag8.tracker.util

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Build
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.ui.endless.log
import com.soumya.meats4all.utils.DialogButtonsCallBack
import kotlinx.android.synthetic.main.alert_view.view.*
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.*
import kotlin.experimental.and


object Utils {
    var updateType: String = ""
    fun bytesToHexString(src: ByteArray?): String? {
        val stringBuilder = StringBuilder()
        if (src == null || src.size <= 0) {
            return null
        }
        for (i in src.indices) {
            val v: Byte = src[i] and 0xFF.toByte()
            val hv = Integer.toHexString(v.toInt())
            if (hv.length < 2) {
                stringBuilder.append(0)
            }
            stringBuilder.append(hv)
        }
        return stringBuilder.toString()
    }

    fun getDate(milliSeconds: Long, dateFormat: String): String? {
        return try {
            val formatter = SimpleDateFormat(dateFormat)
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = milliSeconds
            formatter.format(calendar.time);
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    fun getCurrentSsid(context: Context): String? {
        var ssid: String? = null
        val connManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
        if (networkInfo != null && networkInfo.isConnected) {
            val wifiInfo =
                context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
            val connectionInfo = wifiInfo.connectionInfo
            connectionInfo.ssid
            ssid = connectionInfo.ssid
        }
        return ssid
    }

    fun getBatteryImage(batteryLevel: Int): Int {
        var batteryImage: Int? = null
        when {
            batteryLevel >= 90 -> {
                batteryImage = R.drawable.full
            }
            batteryLevel in 90 downTo 70 -> {
                batteryImage = R.drawable.semiful
            }
            batteryLevel in 69 downTo 40 -> {
                batteryImage = R.drawable.half
            }
            batteryLevel in 39 downTo 10 -> {
                batteryImage = R.drawable.low
            }
            else -> {
                batteryImage = R.drawable.empty
            }
        }
        return batteryImage
    }

    fun createAlertDialog(
        activity: Activity,
        title: String,
        message: String,
        positiveButtonText: String,
        negativeButtonText: String,
        callback: DialogButtonsCallBack?
    ) {
        var dialog = AlertDialog.Builder(activity)
        var inflater = LayoutInflater.from(Tag8Application.getInstance().applicationContext)

        var dialogView = inflater.inflate(R.layout.alert_view, null)
        dialog.setView(dialogView)
        if (title.isNotEmpty()) {
            dialogView.alert_tile.visibility = View.VISIBLE
            dialogView.alert_tile.text = title
        } else {
            dialogView.alert_tile.visibility = View.GONE
        }

        dialogView.alert_message.text = message
        if (positiveButtonText.isEmpty()) {
            dialogView.positive_btn.visibility = View.GONE
        }
        if (negativeButtonText.isEmpty()) {
            dialogView.negative_btn.visibility = View.GONE
        }
        dialogView.positive_btn.text = positiveButtonText
        dialogView.negative_btn.text = negativeButtonText
        var builder = dialog.create()
        builder.setCancelable(false)
        dialogView.positive_btn.setOnClickListener {
            callback?.setCallback(true)
            builder.dismiss()
        }
        dialogView.negative_btn.setOnClickListener {
            callback?.setCallback(false)
            builder.dismiss()
        }
        builder.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        log("tagcreate8 ${Tag8Application.getInstance().getActivity()}")
        builder.show()
    }

    fun checkNetwork(): Boolean {
        var isConnected = false
        if (ConnectivityHelper.isConnectedToNetwork(Tag8Application.getInstance().applicationContext)) {
            isConnected = true
        } else {
            Toast.makeText(
                Tag8Application.getInstance().applicationContext,
                Tag8Application.getInstance().applicationContext.getString(R.string.network_connection_lost),
                Toast.LENGTH_LONG
            ).show()

        }
        log("is internet connection $isConnected")
        return isConnected
    }

    fun isNetworkAvailable(): Boolean {
        val connectivityManager = Tag8Application.getInstance()
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        val activeNetworkInfo = connectivityManager!!.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }


    fun getDateTimestampMs(time: Long, format: String): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = time
        val sdf = SimpleDateFormat(format, Locale.ENGLISH)
        return sdf.format(calendar.time)
    }

    private const val formatYear = "yyyy"
    private const val formatMonths = "M"
    private const val formatDay = "d"
    private const val formatHour = "H"
    private const val formatMin = "m"

    fun calculateTimestamp(timestamp: String): String {
        val currentTimestamp = Calendar.getInstance()

        try {
            val oldTimeYear = getDate(timestamp.toLong(), formatYear)?.toInt()
            val oldTimeMonth = getDate(timestamp.toLong(), formatMonths)?.toInt()
            val oldTimeDay = getDate(timestamp.toLong(), formatDay)?.toInt()
            val oldTimeHour = getDate(timestamp.toLong(), formatHour)?.toInt()
            val oldTimeMinutes = getDate(timestamp.toLong(), formatMin)?.toInt()

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val fromDateTime = LocalDateTime.of(
                    oldTimeYear!!,
                    oldTimeMonth!!,
                    oldTimeDay!!,
                    oldTimeHour!!,
                    oldTimeMinutes!!
                )
                val toDateTime = LocalDateTime.of(
                    currentTimestamp.get(Calendar.YEAR),
                    currentTimestamp.get(Calendar.MONTH) + 1,
                    currentTimestamp.get(Calendar.DATE),
                    currentTimestamp.get(Calendar.HOUR_OF_DAY),
                    currentTimestamp.get(Calendar.MINUTE)
                )
                val tempDateTime = LocalDateTime.from(fromDateTime)
                return "${tempDateTime.until(toDateTime, ChronoUnit.MINUTES)}"
            }
        } catch (e: Exception) {
            return "0"
        }
        return "0"
    }

    fun openMiPermission(context: Context) {
        val c = Class.forName("android.os.SystemProperties")
        val get = c.getMethod("get", String::class.java)
        val miui = get.invoke(c, "ro.miui.ui.version.name") as String
        if (miui != null && miui.contains("11")) {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.fromParts("package", context.packageName, null)
            intent.data = uri
            context.startActivity(intent)
        }
    }
}