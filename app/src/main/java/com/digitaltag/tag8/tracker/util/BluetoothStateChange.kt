package com.digitaltag.tag8.tracker.util

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_MUTABLE
import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.ble.Tag8BluetoothGatt
import com.digitaltag.tag8.tracker.constants.Constants.NotificationChannelBeacon.BEACON_NOTIFICATION_CHANNEL_ID
import com.digitaltag.tag8.tracker.constants.Constants.NotificationChannelBeacon.BEACON_NOTIFICATION_CHANNEL_NAME
import com.digitaltag.tag8.tracker.slider.SliderActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder


class BluetoothStateChange : BroadcastReceiver() {
    var type_ble: String = ""
    var type_no: Int = 0
    override fun onReceive(context: Context?, intent: Intent?) {

        if (intent?.action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
            when (intent?.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)) {
                BluetoothAdapter.STATE_OFF -> {
                    type_ble = "state_off"
                    if (!Tag8BluetoothGatt.isAppIsInBackground(context!!)) {
                        showOffDialog(context)
                    } else {
                        createANotification(context)
                    }
                }
                BluetoothAdapter.STATE_TURNING_OFF -> {
                    type_ble = "state_off"
                    if (!Tag8BluetoothGatt.isAppIsInBackground(context!!)) {
                        showOffDialog(context)
                    } else {
                        createANotification(context)
                    }
                }
                BluetoothAdapter.STATE_ON -> {
                    type_ble = "state_on"
                    type_no = 0
                    Log.d("TAG", "onReceive: Turned On ")
                }
                BluetoothAdapter.STATE_TURNING_ON -> {
                    type_no = 0
                    type_ble = "state_on"
                    Log.d("TAG", "onReceive: Turned On")
                }
            }
        }
    }

    private fun createANotification(context: Context?) {
        if (type_no == 1) {
            return
        }
        type_no = 1

        context?.let { context ->
            val intent = Intent(context, SliderActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            val pendingIntent: PendingIntent = PendingIntent.getActivity(context, 0, intent, FLAG_MUTABLE)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val notificationChannel = NotificationChannel(
                    BEACON_NOTIFICATION_CHANNEL_ID,
                    BEACON_NOTIFICATION_CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_HIGH
                )
                notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
                val manager =
                    context.applicationContext.getSystemService(NotificationManager::class.java)
                manager.createNotificationChannel(notificationChannel)
            }

            val notification: Notification =
                NotificationCompat.Builder(context, BEACON_NOTIFICATION_CHANNEL_ID)
                    .setContentTitle(context.resources.getString(R.string.bluetooth_is_off))
                    .setContentText(context.resources.getString(R.string.bluetooth_is_off_desc))
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.mipmap.appicon)
                    .setAutoCancel(true)
                    .build()
            with(NotificationManagerCompat.from(context)) {
                notify(1524, notification)
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun showOffDialog(context: Context?) {
        if (type_no == 1) {
            return
        }
        type_no = 1
        context?.let {
            try {
                MaterialAlertDialogBuilder(it)
                    .setTitle(context.resources.getString(R.string.bluetooth_is_off))
                    .setMessage(context.resources.getString(R.string.bluetooth_is_off_desc))
                    .setNeutralButton(context.resources.getString(R.string.not_now)) { dialog, _ ->
                        dialog.dismiss()
                    }
                    .setPositiveButton(context.resources.getString(R.string.enable)) { dialog, _ ->
                        val adapter = BluetoothAdapter.getDefaultAdapter()
                        if (!adapter.isEnabled) {
                            adapter.enable()
                        }
                        dialog.dismiss()
                    }
                    .show()
            } catch (e: Exception) {
                e.message
            }
        }
    }
}