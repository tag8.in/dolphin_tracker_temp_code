package com.digitaltag.tag8.tracker.ui.beacon

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.util.setSafeOnClickListener
import kotlinx.android.synthetic.main.activity_tandc.*

class AboutActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tandc)
        Tag8Application.getInstance().setActivity(this)
        webview.loadUrl("https://tag8.in/page/terms-and-conditions")
        backarrow.setSafeOnClickListener { onBackPressed() }
    }

    override fun onResume() {
        super.onResume()
        Tag8Application.getInstance().setActivity(this)
    }

}