package com.digitaltag.tag8.tracker.modules

class Beacon(
        val uid: String?,
        val mac_address: String?,
        val type: String?,
        val name: String?,
        val beacon_connectionstatus: String?,
        val latitude: String?,
        val longitude: String?,
        val timestamp: String?,
        val modifiedOn: String?

)