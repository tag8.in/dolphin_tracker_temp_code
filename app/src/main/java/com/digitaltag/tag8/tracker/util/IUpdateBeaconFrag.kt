package com.digitaltag.tag8.tracker.util

import com.digitaltag.tag8.tracker.model.BeaconDevice

interface IUpdateBeaconFrag {
    fun onUpdate(beaconDevice: BeaconDevice)
}