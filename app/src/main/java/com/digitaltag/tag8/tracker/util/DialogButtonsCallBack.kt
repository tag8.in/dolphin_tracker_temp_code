package com.soumya.meats4all.utils

interface DialogButtonsCallBack {
    fun setCallback(isPositive: Boolean)
}