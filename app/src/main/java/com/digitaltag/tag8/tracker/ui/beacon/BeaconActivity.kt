package com.digitaltag.tag8.tracker.ui.beacon

import android.app.Dialog
import android.bluetooth.BluetoothDevice
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.baseui.BaseActivity
import com.digitaltag.tag8.tracker.baseui.BluetoothConnection
import com.digitaltag.tag8.tracker.baseui.SectionPagerAdapter
import com.digitaltag.tag8.tracker.ble.BluetoothLeService
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.databinding.ActivityBeaconBinding
import com.digitaltag.tag8.tracker.fragment.MainBeaconFragment
import com.digitaltag.tag8.tracker.fragment.SecondaryBeaconFragment
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.addStatusText
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.gettrackerBattery
import com.digitaltag.tag8.tracker.model.BeaconDevice
import com.digitaltag.tag8.tracker.util.*
import kotlinx.android.synthetic.main.activity_beacon.*
import kotlinx.android.synthetic.main.help_layout.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

class BeaconActivity : BaseActivity(), BluetoothConnection.Callback, BluetoothLeService.Callback {

    var beaconDevice: BeaconDevice? = null
    private var bluetoothConnection: BluetoothConnection? = null
    var mBluetoothLeService: BluetoothLeService? = null
    private var mServiceConnection: ServiceConnection? = null
    private var mainBeaconFragment: MainBeaconFragment? = null
    private var secondaryBeaconFragment: SecondaryBeaconFragment? = null
    private var mode = Constants.Beacon.Mode.BEACON
    private var updateCallback: IUpdateBeaconFrag? = null
    var handler: Handler = Handler(Looper.getMainLooper())
    var runnable: Runnable? = null
    var delay = 5000L
    var isScanStart = false
    var dialog: Dialog? = null
    var onceDisconnectCalled = false
    var scheduler: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()
    private lateinit var binding: ActivityBeaconBinding


    fun setUpdateCallback(updateCalbkv: IUpdateBeaconFrag) {
        updateCallback = updateCalbkv
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBeaconBinding.inflate(layoutInflater)
        setContentView(binding.root)
        beaconDevice = intent?.getParcelableExtra(Constants.BEACON_DEVICE)
        Tag8Application.getInstance().setActivity(this)
        if (gettrackerBattery(beaconDevice!!.macAddress).isNullOrEmpty()) {
            try {
                val batteryLevel = gettrackerBattery(beaconDevice!!.macAddress)!!.toInt()
                setBatteryLevel(batteryLevel = batteryLevel)
            } catch (e: RuntimeException) {
                batteryimg.setImageResource(R.drawable.ic_battery)
            }

        } else {
            batteryimg.setImageResource(R.drawable.ic_battery)
        }


        backarrow.setSafeOnClickListener {
            finish()
        }
        createHelpDialog()
        help.setSafeOnClickListener {
            if (beaconDevice != null) {
                if (!beaconDevice!!.enablePhoneRing && !beaconDevice!!.enableBeaconRing) {
                    dialog!!.alertstatus.text = getString(R.string.alertisoff)
                    dialog!!.phonealert2.visibility = View.VISIBLE
                    dialog!!.devicealert2.visibility = View.VISIBLE
                    dialog!!.motionalert1.visibility = View.VISIBLE
                    dialog!!.phonealert1.text = getString(R.string.phonealert1)
                    dialog!!.devicealert1.text = getString(R.string.devicealert1)
                } else {
                    dialog!!.alertstatus.text = getString(R.string.alertison)
                    dialog!!.motionalert1.visibility = View.GONE
                    if (beaconDevice!!.enablePhoneRing && beaconDevice!!.enableBeaconRing) {
                        dialog!!.phonealert2.visibility = View.GONE
                        dialog!!.devicealert2.visibility = View.GONE
                        dialog!!.phonealert1.text = getString(R.string.phonealerton)
                        dialog!!.devicealert1.text = getString(R.string.devicealerton)
                    } else if (beaconDevice!!.enablePhoneRing) {
                        dialog!!.phonealert2.visibility = View.GONE
                        dialog!!.phonealert1.text = getString(R.string.phonealerton)
                    } else if (beaconDevice!!.enableBeaconRing) {
                        dialog!!.devicealert2.visibility = View.GONE
                        dialog!!.devicealert1.text = getString(R.string.devicealerton)
                    }
                }
            }
            dialog!!.show()
        }

        container?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

                Log.d("BeaconActivity", "onPageScrollStateChanged: " + position)
                if (position == 0) {
                    firstclicle.background = ContextCompat.getDrawable(
                        this@BeaconActivity,
                        R.drawable.circle_fragment_live
                    )
                    secondclicle.background = ContextCompat.getDrawable(
                        this@BeaconActivity,
                        R.drawable.circle_fragment_blank
                    )
                } else {
                    secondclicle.background = ContextCompat.getDrawable(
                        this@BeaconActivity,
                        R.drawable.circle_fragment_live
                    )
                    firstclicle.background = ContextCompat.getDrawable(
                        this@BeaconActivity,
                        R.drawable.circle_fragment_blank
                    )
                }
            }

            override fun onPageSelected(position: Int) {
            }

        })

        setDeviceData()

        mServiceConnection = object : ServiceConnection {
            override fun onServiceDisconnected(name: ComponentName?) {
                bluetoothConnection?.stopScan()
            }

            override fun onServiceConnected(name: ComponentName?, iBinder: IBinder?) {
                if (iBinder is BluetoothLeService.LocalBinder) {
                    mBluetoothLeService = iBinder.service
                    mBluetoothLeService?.addCallback(this@BeaconActivity)
                    setupViewPager()
                }
            }
        }
        startTask()
        bindService(
            Intent(this, BluetoothLeService::class.java),
            mServiceConnection!!,
            Context.BIND_AUTO_CREATE
        )

    }

    fun getBatteryLevel() {
        val batteryLevel = gettrackerBattery(beaconDevice!!.macAddress)!!.toInt()
        setBatteryLevel(batteryLevel = batteryLevel)
    }

    private fun setBatteryLevel(batteryLevel: Int) {
        batteryimg.setImageResource(Utils.getBatteryImage(batteryLevel))
    }

    private fun createHelpDialog() {
        dialog = Dialog(this)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(false)
        dialog!!.setContentView(R.layout.help_layout)
        val okBtn = dialog!!.findViewById(R.id.ok) as AppCompatTextView
        okBtn.setSafeOnClickListener {
            dialog!!.dismiss()
        }
    }

    override fun onResume() {
        Tag8Application.getInstance().setActivity(this)
        scheduler = Executors.newSingleThreadScheduledExecutor()
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        scheduler?.shutdown()
        Log.d("TAG", "onPause: sefilay on pause ${scheduler.isShutdown}")
    }


    private fun setDeviceData() {
        val colorActive = ContextCompat.getColor(this, R.color.primary_color)
        val colorInActive = ContextCompat.getColor(this, android.R.color.darker_gray)

        val connected: String = getString(R.string.connected)
        val disconnected: String = getString(R.string.disconnected)
        beconofftxt.visibility = View.GONE
        if (beaconDevice?.isConnected() == true) {
            status.text = connected
            addStatusText(beaconDevice!!.macAddress, connected)
            status.setTextColor(ContextCompat.getColor(this, R.color.primary_color))
            circleDisconnected.visibility = View.GONE
            when {
                (beaconDevice?.rssiResult ?: -100) >= -60 -> {
                    beaconRange.text = "Tracker is very close"
                    if (beaconDevice?.bleType == BLEType.SMART_LOCK) {
                        beaconRange.text = "Lock is very close"
                    }
                    binding.circleFirst.setColorFilter(colorActive)
                    binding.circleFirstLeft.setColorFilter(colorActive)
                    binding.circleSecond.setColorFilter(colorActive)
                    binding.circleSecondLeft.setColorFilter(colorActive)
                    binding.circleThird.setColorFilter(colorActive)
                    binding.circleThirdLeft.setColorFilter(colorActive)
                }
                (beaconDevice?.rssiResult ?: -100) >= -80 -> {
                    beaconRange.text = "Tracker is close"
                    if (beaconDevice?.bleType == BLEType.SMART_LOCK) {
                        beaconRange.text = "Lock is close"
                    }
                    binding.circleFirst.setColorFilter(colorActive)
                    binding.circleFirstLeft.setColorFilter(colorActive)
                    binding.circleSecond.setColorFilter(colorActive)
                    binding.circleSecondLeft.setColorFilter(colorActive)
                    binding.circleThird.setColorFilter(colorInActive)
                    binding.circleThirdLeft.setColorFilter(colorInActive)
                }
                else -> {
                    beaconRange.text = "Tracker is Far"
                    if (beaconDevice?.bleType == BLEType.SMART_LOCK) {
                        beaconRange.text = "Lock is Far"
                    }
                    binding.circleFirst.setColorFilter(colorActive)
                    binding.circleFirstLeft.setColorFilter(colorActive)
                    binding.circleSecond.setColorFilter(colorInActive)
                    binding.circleSecondLeft.setColorFilter(colorInActive)
                    binding.circleThird.setColorFilter(colorInActive)
                    binding.circleThirdLeft.setColorFilter(colorInActive)
                }
            }
        } else {
            binding.circleFirst.setColorFilter(colorInActive)
            binding.circleFirstLeft.setColorFilter(colorInActive)

            binding.circleSecond.setColorFilter(colorInActive)
            binding.circleSecondLeft.setColorFilter(colorInActive)

            binding.circleThird.setColorFilter(colorInActive)
            binding.circleThirdLeft.setColorFilter(colorInActive)


            addStatusText(beaconDevice!!.macAddress, disconnected)
            val date = Utils.getDate(
                beaconDevice?.lastUpdated
                    ?: System.currentTimeMillis(), "dd/MM/yyyy hh:mm:ss"
            )
            beaconRange.text = "Lost time $date"
            val disStatus: Boolean? =
                SharedPreferenceManager.gettrackerOFF(beaconDevice?.macAddress!!)
            if (disStatus!!) {
                status.text = getString(R.string.status_off)
                addStatusText(beaconDevice!!.macAddress, getString(R.string.status_off))
                beconofftxt.visibility = View.VISIBLE
                beaconRange.text = "Closed time $date"
            }
            status.setTextColor(ContextCompat.getColor(this, R.color.disconnect_color))
            circleDisconnected.visibility = View.VISIBLE
        }

        val type = beaconDevice?.type

        devicename?.text = beaconDevice?.bleDeviceName

        when {
            type.equals(Constants.Beacon.Type.Key) -> {
                typeimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.key))
            }
            type.equals("") -> {
                typeimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.key))
            }
            type.equals(Constants.Beacon.Type.Pet) -> {
                typeimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.pett))
            }
            type.equals(Constants.Beacon.Type.Wallet) -> {
                typeimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.wallet))
            }
            type.equals(Constants.Beacon.Type.Laptop) -> {
                typeimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.laptop))
            }
            type.equals(Constants.Beacon.Type.Passport) -> {
                typeimg.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.passport_vextor
                    )
                )
            }
            type.equals(Constants.Beacon.Type.Bag) -> {
                typeimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_bag))
            }
            type.equals(Constants.Beacon.Type.CardHolder) -> {
                typeimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_card_holder))
            }
            type.equals(Constants.Beacon.Type.Toy) -> {
                typeimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_toy))
            }
            type.equals(Constants.Beacon.Type.Camera) -> {
                typeimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_camer))
            }
            type.equals(Constants.Beacon.Type.Remote) -> {
                typeimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.remot))
            }
            type.equals(Constants.Beacon.Type.Car) -> {
                typeimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.car))
            }
            type.equals(Constants.Beacon.Type.Other) -> {
                typeimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.other))
            }
            type.equals(Constants.Beacon.Type.Purse) -> {
                typeimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.purse))
            }
            else -> {
                Glide.with(applicationContext).load(beaconDevice?.type).into(typeimg)
            }
        }

        typeimg.postDelayed({
            val bitmap = BlurBuilder().blur(typeimg)
            bigImage.setImageBitmap(bitmap)
        }, 100)
        mainBeaconFragment?.updateBeaconDevice(beaconDevice)
        secondaryBeaconFragment?.updateBeaconDevice(beaconDevice)
    }

    override fun onDestroy() {
        super.onDestroy()
        mBluetoothLeService?.removeCallback(this@BeaconActivity)
        if (mServiceConnection != null) {
            unbindService(mServiceConnection!!)
        }
        bluetoothConnection?.stopScan()
        scheduler?.shutdown()

    }

    private fun setupViewPager() {
        val adapter = SectionPagerAdapter(supportFragmentManager)
        if (beaconDevice != null) {
            mainBeaconFragment = MainBeaconFragment.newInstance(beaconDevice!!)
            adapter.addFragment(mainBeaconFragment!!)
            secondaryBeaconFragment = SecondaryBeaconFragment.newInstance(beaconDevice!!)
            adapter.addFragment(secondaryBeaconFragment!!)
            container.offscreenPageLimit = 2
            container.adapter = adapter
        }
    }

    override fun handleScanResult(device: BluetoothDevice, rssi: Int) {
        val trackName = device.name ?: return
        var bleType = BLEType.NONE
        if (trackName == "iLck3") {
            bleType = BLEType.SMART_LOCK
            return
        }
        if (!ConnectionActivity.beaconsNameList.toString().contains(trackName)) {
            bleType = BLEType.SMART_TRACKER
            return
        }

        val tag8BluetoothGatt =
            mBluetoothLeService?.getTag8BluetoothGattByMacAddress(device.address)
        if (tag8BluetoothGatt != null) {
            if (tag8BluetoothGatt.beaconDevice != null) {
                if (!tag8BluetoothGatt.isConnected()) {
                    tag8BluetoothGatt.connect()
                }
            }
        } else {
            DebugLogger.d(
                ConnectionActivity.TAG,
                "Scan success, found new Track " + device.address + device.name + rssi
            )
            bluetoothConnection?.stopScan()
        }
        mBluetoothLeService?.addOrUpdateDevice(device, rssi, bleType)
        // mainBeaconFragment?.updateFinding(false)
    }


    override fun onScanFailed(errorCode: Int, specificMacAddress: String?) {
        bluetoothConnection?.stopScan()
        Toast.makeText(
            this@BeaconActivity,
            "Make sure you are near to beacon and beacon is on",
            Toast.LENGTH_SHORT
        ).show()
        // mainBeaconFragment?.updateFinding(false)
    }

    override fun onDeviceAdded(beaconDevice: BeaconDevice) {
        runOnUiThread {
            if (beaconDevice.macAddress.equals(this.beaconDevice?.macAddress)) {
                this.beaconDevice = beaconDevice
                setDeviceData()
            }
        }
    }

    override fun onDeviceUpdated(beaconDevice: BeaconDevice) {
        runOnUiThread {
            if (beaconDevice.macAddress == this.beaconDevice?.macAddress) {
                this.beaconDevice = beaconDevice
                if (updateCallback != null) {
                    updateCallback!!.onUpdate(beaconDevice)
                }
                //mBluetoothLeService?.phoneAlert(beaconDevice!!.macAddress)
                setDeviceData()
            }
        }
    }

    override fun onDeviceRemoved(beaconDevice: BeaconDevice) {
        finish()
    }

    fun connectOrRing(beaconDevice: BeaconDevice?) {
        if (beaconDevice != null) {
            if (beaconDevice.connectionState.equals(Constants.Beacon.State.CONNECTED)) {
                ringBeacon(beaconDevice)
            } else {
                connectBeacon(beaconDevice)
            }
        }
    }

    fun unlockBeacon(beaconDevice: BeaconDevice?) {
        if (beaconDevice != null) {
            if (beaconDevice.connectionState.equals(Constants.Beacon.State.CONNECTED)) {
                mBluetoothLeService?.unlockLock(beaconDevice)
            } else {
                connectBeacon(beaconDevice)
            }
        }
    }

    fun updateAlertOptions(beaconDevice: BeaconDevice?) {
        if (beaconDevice != null) {
            mBluetoothLeService?.updateEnableBeaconDevice(beaconDevice, "")
        }
    }

    private fun ringBeacon(beaconDevice: BeaconDevice) {
        mBluetoothLeService?.ringBeacon(beaconDevice)
    }

    private fun connectBeacon(beaconDevice: BeaconDevice) {
        if (bluetoothConnection == null) {
            bluetoothConnection = BluetoothConnection(this@BeaconActivity, this@BeaconActivity)
        }
        bluetoothConnection?.startScan(beaconDevice)
        // mainBeaconFragment?.updateFinding(true)
    }

    fun disconnect(beaconDevice: BeaconDevice?) {
        if (beaconDevice != null) {
            mBluetoothLeService?.disconnect(beaconDevice)
        }
    }

    fun deleteBeacon(beaconDevice: BeaconDevice?) {
        if (beaconDevice != null) {
            bluetoothConnection?.stopScan()
            mBluetoothLeService?.deleteBeacon(beaconDevice)
        }
    }

    fun startTask() {
        scheduler.scheduleAtFixedRate(Runnable {
            if (!scheduler?.isShutdown) {
                val offStatus: Boolean? =
                    SharedPreferenceManager.gettrackerOFF(beaconDevice?.macAddress!!)
                if (offStatus!!) {
                    connectBeacon(beaconDevice!!)
                } else {
                    var isDisconnected = !beaconDevice!!.isConnected()
                    if (isDisconnected) {
                        mBluetoothLeService?.let { it -> it.connectBeacon(beaconDevice!!) }
                    }
                }
            }

        }, 0, 4, TimeUnit.SECONDS)
    }

}