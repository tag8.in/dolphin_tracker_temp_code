package com.digitaltag.tag8.tracker.model

import android.os.Parcelable
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.util.BLEType
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BeaconDevice(
    @SerializedName("ble_device_name")
    var bleDeviceName: String,
    @SerializedName("ble_mac_address")
    val macAddress: String,
    @SerializedName("ble_type")
    var type: String,
    @Transient
    var rssiResult: Int,
    @SerializedName("ble_state")
    var connectionState: String = Constants.Beacon.State.DISCONNECTED,
    @Transient
    var startConnectTick: Long = 0,
    @Transient
    var findDeviceTick: Long = 0,
    @Transient
    var lastUpdateTick: Long = 0,
    @Transient
    var isPhoneAlerting: Boolean = false,
    @Transient
    var isBeaconAlerting: Boolean = false,
    @SerializedName("ble_enable_phone_ring")
    var enablePhoneRing: Boolean = true,
    @SerializedName("ble_enable_beacon_ring")
    var enableBeaconRing: Boolean = true,
    @SerializedName("latitude")
    var latitude: String? = null,
    @SerializedName("longitude")
    var longitude: String? = null,
    var location: String? = null,
    @SerializedName("last_updated")
    var lastUpdated: Long = System.currentTimeMillis(),
    @Transient
    var bleType: BLEType? = BLEType.NONE,
    @Transient
    var isDeviceLocked: Boolean = false
) : Parcelable {

    override fun equals(other: Any?): Boolean {
        return if (other is BeaconDevice) {
            other.macAddress == macAddress
        } else {
            super.equals(other)
        }
    }

    fun isConnected(): Boolean {
        return connectionState == Constants.Beacon.State.CONNECTED
    }

}