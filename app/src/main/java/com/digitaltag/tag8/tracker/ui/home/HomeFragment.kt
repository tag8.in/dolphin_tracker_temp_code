package com.digitaltag.tag8.tracker.ui.home

import android.bluetooth.BluetoothDevice
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.digitaltag.tag8.tracker.NavigationActivity
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.baseui.BluetoothConnection
import com.digitaltag.tag8.tracker.ble.BluetoothLeService
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.managers.CustomLinearLayoutManager
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.model.BeaconDevice
import com.digitaltag.tag8.tracker.ui.beacon.BeaconActivity
import com.digitaltag.tag8.tracker.ui.beacon.BeaconsAdapter
import com.digitaltag.tag8.tracker.ui.endless.log
import com.digitaltag.tag8.tracker.util.setSafeOnClickListener
import com.digitaltag.tag8.tracker.util.toast
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import kotlinx.android.synthetic.main.activity_connection.*
import kotlinx.android.synthetic.main.activity_navigation.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.beaconnotice
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit


class HomeFragment : Fragment(), BluetoothLeService.Callback {

    var mBluetoothLeService: BluetoothLeService? = null
    private var mServiceConnection: ServiceConnection? = null
    private val mApplication: Tag8Application = Tag8Application.getInstance()
    var handler: Handler = Handler(Looper.getMainLooper())
    var runnable: Runnable? = null
    var delay = 5000L
    var isScanStart = false
    lateinit var mActivity: NavigationActivity
    var scheduler: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()

    companion object {
        fun barCodeButton(text: String, imageView: ImageView): Bitmap? {
            val multiFormatWriter = MultiFormatWriter()
            try {
                val bitMatrix = multiFormatWriter.encode(
                    text,
                    BarcodeFormat.CODE_128,
                    imageView.getWidth(),
                    imageView.getHeight()
                )
                val bitmap = Bitmap.createBitmap(
                    imageView.getWidth(),
                    imageView.getHeight(),
                    Bitmap.Config.RGB_565
                )
                for (i in 0 until imageView.width) {
                    for (j in 0 until imageView.height) {
                        bitmap.setPixel(i, j, if (bitMatrix.get(i, j)) Color.BLACK else Color.WHITE)
                    }
                }
                return bitmap
                //imageView.setImageBitmap(bitmap)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return null
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mActivity = requireActivity() as NavigationActivity
        try {
            mActivity.progresslayout.visibility = View.VISIBLE
        } catch (e: Exception) {
            e.message
        }
        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (activity?.packageManager?.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE) != true) {
            requireActivity().toast("Ble not supported")
            requireActivity().finish()
        }
        /* Permissions.permissionList(activity)
         Permissions.locationSetting(activity)*/
        bindService()
        ivAddDevice?.setSafeOnClickListener {
            findNavController().navigate(R.id.action_navigation_home_to_addNewItemFragment)
        }
    }

    override fun onResume() {
        Tag8Application.getInstance().setActivity(mActivity)
        var beconList = mApplication.deviceHashMap
        log("device size ${beconList.size}")

        var listDevice: MutableList<BeaconDevice> = ArrayList()
        if (beconList?.isNotEmpty()) {
            for (device in beconList.values) {
                listDevice.add(device.beaconDevice!!)

            }
            setAdapter(listDevice)
        } else {
            setAdapter()
        }
        scheduler = Executors.newSingleThreadScheduledExecutor()
        /* runnable = Runnable {
             if(isScanStart){
                 if (deviceList?.isNotEmpty() == true) {
                     var count = 0
                     for (device in deviceList!!.values) {
                         var dtrackeroff = SharedPreferenceManager.gettrackerOFF(device.macAddress)
                         var needScan = mApplication.deviceHashMap[device.macAddress]
                         if(needScan != null){
                             if (dtrackeroff!!.isNotEmpty()) {
                                 startScan(device)
                                 count++
                             }
                         }
                         var beaconDisconnected = !device!!.isConnected()
                         Log.d("TAG", "onResume: isbeacon connected $beaconDisconnected")
                         if(beaconDisconnected){
                             mBluetoothLeService!!.connectBeacon(device)
                             count++
                         }
                     }
                     if(count == 0){
                         isScanStart = false
                         handler.removeCallbacks(runnable!!)
                     }
                 }
             }

         }
         isScanStart = true
         handler.postDelayed(runnable!!, delay)
         handler.post(runnable!!)*/
     //   startTask()

        super.onResume()
    }

    override fun onPause() {
        scheduler?.shutdown()
        super.onPause()
    }

    private fun bindService() {
        mServiceConnection = object : ServiceConnection {
            override fun onServiceDisconnected(name: ComponentName?) {
                mBluetoothLeService = null
            }

            override fun onServiceConnected(name: ComponentName?, iBinder: IBinder?) {
                if (iBinder is BluetoothLeService.LocalBinder) {
                    mBluetoothLeService = iBinder.service
                    mBluetoothLeService?.addCallback(this@HomeFragment)
                    rcv?.adapter?.notifyDataSetChanged()
                    var deviceList = SharedPreferenceManager.getDevices()
                    var beconList = mApplication.deviceHashMap
                    log("device size ${beconList.size}")
                    if (deviceList != null && deviceList?.isNotEmpty()!!) {
                        var listDevice = ArrayList<BeaconDevice>()
                        for (device in beconList.values) {
                            if (device.beaconDevice!!.bleDeviceName.isNotEmpty()) {
                                listDevice.add(device.beaconDevice!!)
                            } else {
                                SharedPreferenceManager.removeBeacon(device.beaconDevice!!)
                                beconList.remove(device.beaconDevice!!.macAddress)
                            }
                        }
                        (rcv?.adapter as BeaconsAdapter).updateAllDevices(listDevice)
                    }

                }
            }
        }
        if (activity != null) {
            activity?.bindService(
                Intent(activity, BluetoothLeService::class.java),
                mServiceConnection!!,
                Context.BIND_AUTO_CREATE
            )
        }
    }

    private fun setAdapter(deviceList: List<BeaconDevice>? = null) {
        val adapter = activity?.let {
            BeaconsAdapter(it, object : BeaconsAdapter.BeaconAdapterCallback {
                override fun onClick(beaconDevice: BeaconDevice) {
                    val intent = Intent(activity, BeaconActivity::class.java)
                    intent.putExtra(Constants.BEACON_DEVICE, beaconDevice)
                    startActivity(intent)
                }

                override fun connect(beaconDevice: BeaconDevice) {
                    startScan(beaconDevice)
                }

            }, this)
        }
        rcv?.layoutManager = context?.let { CustomLinearLayoutManager(it) }
        rcv?.adapter = adapter
        rcv?.itemAnimator?.changeDuration=0
        if (deviceList?.isNotEmpty() == true) {
            adapter?.addDevices(deviceList)
        }
        if (adapter != null) {
            adapterNotice(adapter)
        }

    }

    private fun adapterNotice(adapter: BeaconsAdapter) {
        if (adapter.itemCount < 1) {
            beaconnotice.visibility = View.VISIBLE
            mActivity.progresslayout.visibility = View.GONE
        }
    }

    fun startScan(beaconDevice: BeaconDevice) {
        val bluetoothConnection = context?.let { it1 ->
            BluetoothConnection(it1, object : BluetoothConnection.Callback {
                override fun handleScanResult(device: BluetoothDevice, rssi: Int) {
                    Log.d("Deba", "handleScanResult: ${device.address}")
                    mBluetoothLeService?.addOrUpdateDevice(device, rssi, beaconDevice.bleType!!)
                }

                override fun onScanFailed(errorCode: Int, specificMacAddress: String?) {
                    if (specificMacAddress != null) {
                        if (rcv?.adapter is BeaconsAdapter) {
                            (rcv?.adapter as BeaconsAdapter).updateConnectionFailed(
                                specificMacAddress
                            )
                        }
                    }
                }
            })
        }
        bluetoothConnection?.startScan(beaconDevice)
    }

    override fun onDestroy() {
        super.onDestroy()
        mBluetoothLeService?.removeCallback(this)
        if (mServiceConnection != null) {
            activity?.unbindService(mServiceConnection!!)
        }
    }

    fun updateView() {
        if (rcv?.adapter!!.itemCount > 0) {
            beaconnotice.visibility = View.GONE
        } else {
            beaconnotice.visibility = View.VISIBLE
        }
        mActivity.progresslayout.visibility = View.GONE
    }

    override fun onDeviceAdded(beaconDevice: BeaconDevice) {
        activity?.runOnUiThread {
            if (mServiceConnection == null) {
                bindService()
            } else {

                // mBluetoothLeService?.updateEnableBeaconDevice(beaconDevice!!)
                if (rcv?.adapter is BeaconsAdapter) {
                    (rcv?.adapter as BeaconsAdapter).addDevice(beaconDevice)
                }
            }
        }
    }

    override fun onDeviceUpdated(beaconDevice: BeaconDevice) {
        Log.d("TAG", "onDeviceUpdated: ")
        activity?.runOnUiThread {
            if (mServiceConnection == null) {
                bindService()
            } else {
                Log.d("TAG", "onDeviceUpdated before device name :${beaconDevice.bleDeviceName} ")
                if (beaconDevice.bleDeviceName.isEmpty()) {
                    var deviceList = SharedPreferenceManager.getDevices()
                    for (device in deviceList!!) {
                        if (beaconDevice.macAddress == device.value.macAddress) {
                            beaconDevice.bleDeviceName = device.value.bleDeviceName
                        }
                    }
                }
                Log.d("TAG", "onDeviceUpdated after device name :${beaconDevice.bleDeviceName} ")
                if (rcv?.adapter is BeaconsAdapter) {
                    (rcv?.adapter as BeaconsAdapter).updateDevice(beaconDevice)
                }

            }
            //mBluetoothLeService?.phoneAlert(beaconDevice!!.macAddress)
        }
    }

    override fun onDeviceRemoved(beaconDevice: BeaconDevice) {
        if (rcv?.adapter is BeaconsAdapter) {
            (rcv?.adapter as BeaconsAdapter).removeBeacon(beaconDevice)
        }
    }

    private fun startTask() {
        scheduler.scheduleAtFixedRate({
            if (!scheduler.isShutdown) {
                val deviceList = SharedPreferenceManager.getDevices()
                if (deviceList != null && deviceList.size > 0) {
                    for (device in deviceList.values) {
                        val offStatus: Boolean? =
                            SharedPreferenceManager.gettrackerOFF(device!!.macAddress)
                        if (offStatus!!) {
                            startScan(device)
                        } else {
                            var isDisconnected = !device.isConnected()
                            if (device.connectionState != Constants.Beacon.State.BLUETOOTH_OFF) {
                                if (isDisconnected) {
                                    mBluetoothLeService?.let { it -> it.connectBeacon(device!!) }
                                }
                            }
                        }
                    }
                }

            }
        }, 0, 2, TimeUnit.SECONDS)
    }

}