package com.digitaltag.tag8.tracker.util

enum class BLEType {
    SMART_TRACKER,
    SMART_LOCK,
    NONE
}