package com.digitaltag.tag8.tracker.baseui

import androidx.appcompat.app.AppCompatActivity
import com.digitaltag.tag8.tracker.ui.login.LoginCallBack
import com.google.firebase.auth.FirebaseUser

abstract class BaseActivity : AppCompatActivity(), LoginCallBack {

    override fun onGoogleLoginSuccess(user: FirebaseUser?) {}

    override fun onGoogleLoginFail(message: String) {}

}