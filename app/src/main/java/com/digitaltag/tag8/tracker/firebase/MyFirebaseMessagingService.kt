package com.digitaltag.tag8.tracker.firebase

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.util.Log
import com.digitaltag.tag8.tracker.ui.ringtone.RingtoneActivity

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class MyFirebaseMessagingService : FirebaseMessagingService()
{
     lateinit var sharedPreferences: SharedPreferences

    override fun onNewToken(p0: String)
    {
        super.onNewToken(p0)

        Log.e("NEW_TOKEN", p0)
        sharedPreferences = getSharedPreferences("myprefs", MODE_PRIVATE)
        sharedPreferences.edit().putString("FCM_TOKEN", p0).apply()

        //check if device_id and bearer token are available
     /*   if (!sharedPreferences.getString("device_id", "")!!.isEmpty()
                && !sharedPreferences.getString("bearer_token", "")!!.isEmpty())

        //check if asset exists
        {
            val client = OkHttpClient()

            val request: Request = Request.Builder()
                    //.url(sharedPreferences.getString("url", "") + "assetdetails/" + sharedPreferences.getString("device_id", ""))
                    .url(sharedPreferences.getString("url", "") + "getbeacons/" + sharedPreferences.getString("device_id", ""))
                    .get()
                    .addHeader("Authorization", "Bearer " + sharedPreferences.getString("bearer_token", ""))
                    .addHeader("Cache-Control", "no-cache")
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .build()

            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    call.cancel()
                }

                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    val myResponse = response.body!!.string()
                    val handler = Handler(Looper.getMainLooper())
                    handler.post {
                        try {
                            val json = JSONObject(myResponse)
                            if (json.getString("data") == "Error") {
                            } else {
                                //update fcm token
                                val updateclient = OkHttpClient()
                                val updateformBody: RequestBody = FormBody.Builder()
                                        .add("fcm_token", sharedPreferences.getString("FCM_TOKEN", "")!!)
                                        .add("device_id", sharedPreferences.getString("device_id", "")!!)
                                        .build()
                                val updaterequest: Request = Request.Builder() //.url(sharedPreferences.getString("url", "") + "updatefcmtoken")
                                        .url(sharedPreferences.getString("url", "") + "updatedevicefcmtoken")
                                        .post(updateformBody)
                                        .addHeader("Authorization", "Bearer " + sharedPreferences.getString("bearer_token", ""))
                                        .addHeader("Content-Type", "application/x-www-form-urlencoded")
                                        .build()
                                updateclient.newCall(updaterequest).enqueue(object : Callback {
                                    override fun onFailure(call: Call, e: IOException) {
                                        call.cancel()
                                    }

                                    override fun onResponse(call: Call, response: Response) {}
                                })
                                //end
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }
            })

        }
        */



    }

    override fun onMessageReceived(remoteMessage: RemoteMessage)
    {
        super.onMessageReceived(remoteMessage)

        Log.d("TAG", "From : ${remoteMessage.from}")

        remoteMessage.notification?.let{
            Log.d("TAG", "Message is Here : ${it.body}")
        }

        remoteMessage.data.let {
            val title = remoteMessage.data["title"]
            val body = remoteMessage.data["body"]
            val url = remoteMessage.data["url"]

            if (title == "Beacon Located")
            {

                if (isRingtoneActive())
                {
                    applicationContext.stopService(Intent(applicationContext, RingtoneActivity::class.java))
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                {
                    applicationContext.startForegroundService(Intent(applicationContext,
                        RingtoneActivity::class.java))
                } else
                {
                    applicationContext.startService(Intent(applicationContext, RingtoneActivity::class.java))
                }

            //Log.d("TAG", "Message : " + remoteMessage.data)
            } else if (title == "Stop Ringing")
            {
                if (isRingtoneActive()) applicationContext.stopService(Intent(applicationContext, RingtoneActivity::class.java))
                else{}
            }
            //else{}
            else if (title == "Lost Mode Activated")
            {
                //activateLostMode()
            } else if (title == "Lost Mode Deactivated")
            {
                //DeactivateLostMode(body)
            }else{}
            Log.d("TAG", "Message : " + remoteMessage.data)
        }

    }

    //To check whether ringtone is active
    fun isRingtoneActive(): Boolean
    {
        return if (applicationContext != null) {
            isMyServiceRunning(RingtoneActivity::class.java)
        } else {
            false
        }
    }

    fun isMyServiceRunning(serviceClass: Class<*>): Boolean
    {
        val manager =
                applicationContext.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }
}