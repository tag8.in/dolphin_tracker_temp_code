package com.digitaltag.tag8.tracker.ui.endless

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import com.digitaltag.tag8.tracker.Tag8Application

class NotiReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        var deviceList  = Tag8Application.getInstance().deviceHashMap
        var istag8call = false
        var macAddress = ""
        if(deviceList.isNotEmpty()){
            intent?.let {
                istag8call = it.getBooleanExtra("tag8call", false)
                macAddress = it.getStringExtra("macaddress").toString()
            }
            if(istag8call && macAddress.isNotEmpty()){
                var tag8ble = deviceList[macAddress]
                tag8ble?.unRingDevice()
            }
        }
    }
}
