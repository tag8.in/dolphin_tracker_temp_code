package com.digitaltag.tag8.tracker.ui.endless

enum class Actions {
    START,
    STOP
}