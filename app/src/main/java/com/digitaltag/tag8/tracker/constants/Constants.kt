package com.digitaltag.tag8.tracker.constants

import android.app.NotificationManager
import android.os.ParcelUuid
import java.util.*

object Constants {

    const val RC_SIGN_IN = 9001

    var Device_connection = ""
    const val MAC_ADDRESS = "mac_address"
    const val BEACON_DEVICE = "beacon_device"
    const val IS_NEW = "is_new"

    const val RSSI_RETRIEVAL_INTERVAL: Long = 5 * 1000

    object Beacon {
        val DEFAULT_PASSWORD =
            byteArrayOf(0xA1.toByte(), 0xA2.toByte(), 0xA3.toByte(), 0xA4.toByte())
        val DEFAULT_PASSWORD_NAMECHANGE_ =
            byteArrayOf(
                0x61.toByte(), 0x74.toByte(), 0x61.toByte(), 0x67.toByte(), 0x38.toByte()
            )
        val DEFAULT_PASSWORD_LOCK =
            byteArrayOf(0x30, 0x30.toByte(), 0x30.toByte(), 0x30.toByte(), 0x30.toByte())
        val MAX_CONNECTION_TIME_OUT = 30 * 1000
        val DEFAULT_ALERT_TIME: Long = 20 * 1000
        val TURNOFF_DEV_INVALID = 0
        val TURNOFF_DEV_START = 1
        val TURNOFF_DEV_TIMEOUT = 2
        val TURNOFF_DEV_SUCC = 3
        val KEY_PRESS_NOTIFY = 1
        val KEY_HOLD_NOTIFY = 2

        object Type {
            val Key = "Key"
            val Pet = "Pet"
            val Laptop = "Laptop"
            val Wallet = "Wallet"
            val Passport = "Passport"
            val Camera = "Camera"
            val Bag = "Bag"
            val Car = "Car"
            val Toy = "toy"
            val Purse = "Purse"
            val CardHolder = "Card Holder"
            val Remote = "Remote"
            val Other = "Other"
            val ImagePath = ""
        }

        object Signal {
            val STRONG = -60
            val WEAK = -80
            val DEAD_LINE = -90
        }

        object Mode {
            val BEACON = "beacon"
            val CAMERA = "camera"
        }

        object State {
            val CONNECTING = "connecting"
            val CONNECTED = "connected"
            val DISCONNECTING = "disconnecting"
            val DISCONNECTED = "disconnected"
            val BLUETOOTH_OFF = "bluetooth_off"
            val DISTANCE_OFF = "distance_off"
            val OFF = "off"
        }
    }

    object NotificationIds {
        const val BEACON_NOTIFICATION_ID = 111
    }

    object NotificationChannelBeacon {
        const val BEACON_NOTIFICATION_CHANNEL_ID =
            "com.digitaltag.tag8.digitaltag.ble.service.notification"
        const val BEACON_NOTIFICATION_CHANNEL_NAME = "Beacon Notification"
        const val BEACON_NOTIFICATION_CHANNEL_DESCRIPTION = "Tag8 tracker is running."
        const val BEACON_NOTIFICATION_CHANNEL_IMPORTANCE = NotificationManager.IMPORTANCE_DEFAULT
    }

    object RequestCodes {
        const val BEACON_NOTIFICATION_REQUEST_CODE = 1111
    }

    object UUIDs {
        val CHAR_DEV_CFG_UUID = UUID.fromString("0000fff6-0000-1000-8000-00805f9b34fb");
        val PARCEL_UUID_IMMEDIATE_ALERT_SERVICE =
            ParcelUuid.fromString("00001802-0000-1000-8000-00805f9b34fb")
        val CHAR_LOCK_NOTY_UUID = UUID.fromString("0000fff7-0000-1000-8000-00805f9b34fb")
        val CHAR_DEV_INFO_UUID = UUID.fromString("0000fff1-0000-1000-8000-00805f9b34fb");
        val IMMEDIATE_ALERT_SERVICE_UUID_STRING = "00001802-0000-1000-8000-00805f9b34fb"
        val IMMEDIATE_ALERT_SERVICE_UUID = UUID.fromString(IMMEDIATE_ALERT_SERVICE_UUID_STRING)
        val CUSTOM_SERVICE = UUID.fromString("0000fff0-0000-1000-8000-00805f9b34fb")
        val CHARACTERISTIC_CUSTOM_VERIFIED = UUID.fromString("0000fff4-0000-1000-8000-00805f9b34fb")
        val CHARACTERISTIC_KEY_PRESS_SRV_UUID =
            UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb")
        val CHARACTERISTIC_KEY_PRESS_UUID = UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb")
        val CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID =
            UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")
        val TI_KEYFOB_DISCONN_ALERT_SRV_UUID_STR = "00001803-0000-1000-8000-00805f9b34fb"
        val TI_KEYFOB_DISCONN_ALERT_SRV_UUID = UUID.fromString(TI_KEYFOB_DISCONN_ALERT_SRV_UUID_STR)
        val TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID = "00002a06-0000-1000-8000-00805f9b34fb"
        val TI_KEYFOB_PROXIMITY_ALERT_UUID = "00001802-0000-1000-8000-00805f9b34fb"
        val Battery_Service_UUID = "0000180F-0000-1000-8000-00805f9b34fb"
        val Battery_Level_UUID = "00002a19-0000-1000-8000-00805f9b34fb"
        val Custom_Service_uuid = "0000FFF0-0000-1000-8000-00805F9B34FB"
        val Custom_Service = "0000FE59-OOOO-1000-8000-00805F9B34FB"
    }
}