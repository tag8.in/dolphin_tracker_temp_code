package com.digitaltag.tag8.tracker.ble

import android.app.Service
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Binder
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.model.BeaconDevice
import com.digitaltag.tag8.tracker.ui.notification.NotificationBeacon
import com.digitaltag.tag8.tracker.util.BLEType
import com.digitaltag.tag8.tracker.util.DebugLogger


class BluetoothLeService : Service() {

    private var mBluetoothManager: BluetoothManager? = null
    private var mBluetoothAdapter: BluetoothAdapter? = null
    private var mCallbacks = ArrayList<Callback>()
    private val serviceManager = ServiceManager()

    @Transient
    val mApplication: Tag8Application = Tag8Application.getInstance()

    companion object {
        val TAG = DebugLogger.makeLogTag(BluetoothLeService::class.java.simpleName)
    }

    private val mBinder: IBinder = LocalBinder()

    override fun onCreate() {
        super.onCreate()
        serviceManager.startService()
        initialize()
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            turnAllBleDevicesOff()
            unregisterReceiver(mReceiver)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (mBluetoothAdapter == null || mBluetoothManager == null) {
            serviceManager.startService()
            initialize()
        }
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(intent: Intent?): IBinder? {
        return mBinder
    }

    inner class LocalBinder : Binder() {
        val service: BluetoothLeService get() = this@BluetoothLeService
    }

    fun addCallback(callback: Callback) {
        mCallbacks.add(callback)
    }

    fun removeCallback(callback: Callback) {
        mCallbacks.remove(callback)
    }


    private fun initialize(): Boolean {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.")
                return false
            }
        }
        mBluetoothAdapter = mBluetoothManager?.adapter
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.")
            return false
        }
        return true
    }

    fun getTag8BluetoothGattByMacAddress(address: String): Tag8BluetoothGatt? {
        return mApplication.deviceHashMap[address]
    }


    fun addOrUpdateDevice(bluetoothDevice: BluetoothDevice, rssi: Int, bletype: BLEType) {
        Log.d("Deb", "add or update 102")
        if (mApplication.deviceHashMap.containsKey(bluetoothDevice.address)) {
            Log.d("Deb", "add or update 104")
            val tag8BluetoothGatt = mApplication.deviceHashMap[bluetoothDevice.address]
            if (tag8BluetoothGatt != null) {
                tag8BluetoothGatt.updateDeviceRssi(rssi)
                for (callback in mCallbacks) {
                    if (tag8BluetoothGatt.beaconDevice != null) {
                        callback.onDeviceUpdated(tag8BluetoothGatt.beaconDevice!!)
                    }
                }
            }
        } else {
            val currTick = System.currentTimeMillis()
            val tag8BluetoothGatt =
                Tag8BluetoothGatt(this@BluetoothLeService, mBluetoothAdapter, mCallbacks)
            val beaconDevice = BeaconDevice(
                "", bluetoothDevice.address, "", rssi,
                Constants.Beacon.State.DISCONNECTED, 0, currTick, currTick,
                false, false, true, true,
                null, null, "", System.currentTimeMillis(), bletype, false
            )
            tag8BluetoothGatt.beaconDevice = beaconDevice
            mApplication.deviceHashMap[bluetoothDevice.address] = tag8BluetoothGatt
            Log.d("Deb", "addOrUpdateDevice: else 124 ")
            tag8BluetoothGatt.connect()
        }
    }


    fun updateNames(beaconDevice: BeaconDevice) {
        Log.d(TAG, "updateName: update runned here")
        val tag8BluetoothGatt = mApplication.deviceHashMap[beaconDevice.macAddress]
        tag8BluetoothGatt?.updateName()
    }

    fun updateBeaconDevice(beaconDevice: BeaconDevice) {
        Log.d(TAG, "updateBeaconDevice: ${beaconDevice.bleDeviceName}")
        val tag8BluetoothGatt = mApplication.deviceHashMap[beaconDevice.macAddress]
        if (tag8BluetoothGatt != null) {
            tag8BluetoothGatt.updateDevice(beaconDevice)
        } else {
            SharedPreferenceManager.addOrUpdateDevice(tag8BluetoothGatt!!)
            for (callback in mCallbacks) {
                callback.onDeviceUpdated(beaconDevice)
            }
        }
    }

    fun updateEnableBeaconDevice(beaconDevice: BeaconDevice, type: String) {
        val tag8BluetoothGatt = mApplication.deviceHashMap[beaconDevice.macAddress]
        Log.d(TAG, "updateEnableBeaconDevice: $type")
        val enablePhoneRing = beaconDevice.enablePhoneRing
        val enableBeaconRing = beaconDevice.enableBeaconRing
        /*  if (type == "TrackerUpdate") {
              Log.d(TAG, "updateEnableBeaconDevice: $type")
              tag8BluetoothGatt?.enableDistanceAlert()
          }*/
        if (tag8BluetoothGatt != null) {
            tag8BluetoothGatt.updateEnableBeaconDevice(enablePhoneRing, enableBeaconRing)
        } else {
            SharedPreferenceManager.addOrUpdateDevice(tag8BluetoothGatt!!)
            for (callback in mCallbacks) {
                callback.onDeviceUpdated(beaconDevice)
            }
        }
    }

    fun getAllDevices(): List<BeaconDevice> {
        val arrayList = ArrayList<BeaconDevice>()
        for (devices in mApplication.deviceHashMap.values.toList()) {
            if (devices.beaconDevice != null) {
                arrayList.add(devices.beaconDevice!!)
            }
        }
        return arrayList
    }

    fun ringBeacon(beaconDevice: BeaconDevice) {
        val device = mApplication.deviceHashMap[beaconDevice.macAddress]
        device?.makeTrackAlertOnOff()
    }

    fun unlockLock(beaconDevice: BeaconDevice) {
        val device = mApplication.deviceHashMap[beaconDevice.macAddress]
        device?.unlockLockDevice()
    }

    fun disconnect(beaconDevice: BeaconDevice) {
        Toast.makeText(mApplication, "disconnect $beaconDevice", Toast.LENGTH_LONG).show()
        val device = mApplication.deviceHashMap[beaconDevice.macAddress]
        if (device != null) {
            device!!.beaconDevice = beaconDevice
            SharedPreferenceManager.addOrUpdateDevice(beaconDevice)
            device?.disconnect(false)
        }

    }

    fun deleteBeacon(beaconDevice: BeaconDevice) {
        val device = mApplication.deviceHashMap[beaconDevice.macAddress]
        if (device != null) {
            SharedPreferenceManager.arrivalAlert(false, beaconDevice.macAddress)
            device?.delete()
            mApplication.deviceHashMap.remove(beaconDevice.macAddress)
            SharedPreferenceManager.removeBeacon(beaconDevice)
            for (callback in mCallbacks) {
                callback.onDeviceRemoved(beaconDevice)
            }
        }

    }


    fun changeLockPassword(oldPass: String, NewPass: String) {
        val tag8BluetoothGatt = Tag8BluetoothGatt(this@BluetoothLeService, mBluetoothAdapter, mCallbacks)
        tag8BluetoothGatt.startChangePassword(oldPass, NewPass)
    }


    fun isConnected(beaconDevice: BeaconDevice): Boolean {
        Log.d(TAG, "isConnected: 193 ${mApplication.deviceHashMap.size}")
        val device = mApplication.deviceHashMap[beaconDevice.macAddress]
        return device?.isConnected() ?: false
    }

    interface Callback {
        fun onDeviceAdded(beaconDevice: BeaconDevice)
        fun onDeviceUpdated(beaconDevice: BeaconDevice)
        fun onBeaconButtonPress(beaconDevice: BeaconDevice) {}
        fun onDeviceRemoved(beaconDevice: BeaconDevice)
    }

    inner class ServiceManager {
        fun startService() {

            val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
            registerReceiver(mReceiver, filter)
            ContextCompat.startForegroundService(
                this@BluetoothLeService,
                Intent(this@BluetoothLeService, BluetoothLeService::class.java)
            )
            val notificationBeacon = NotificationBeacon(this@BluetoothLeService)
            startForeground(notificationBeacon.notificationId, notificationBeacon.getNotification())
        }

        fun stopService() {
            try {
                unregisterMyBroadcastReceiver()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            stopForeground(true)
            stopSelf()
        }

        private fun unregisterMyBroadcastReceiver() {
            if (null != mReceiver) {
                unregisterReceiver(mReceiver)
                mReceiver = null
            }
        }
    }

    private var mReceiver: BroadcastReceiver? = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (action == BluetoothAdapter.ACTION_STATE_CHANGED) {
                val state = intent.getIntExtra(
                    BluetoothAdapter.EXTRA_STATE,
                    BluetoothAdapter.ERROR
                )
                when (state) {
                    BluetoothAdapter.STATE_OFF -> {
                        turnAllBleDevicesOff()
                    }
                    BluetoothAdapter.STATE_TURNING_OFF -> {
                    }
                    BluetoothAdapter.STATE_ON -> {
                        turnAllBleDevicesOn()
                    }
                    BluetoothAdapter.STATE_TURNING_ON -> {
                    }
                }
            }
        }
    }

    fun writePasswordToLock(address: String, passcode: String) {
        Log.d("Nirav", "writePasswordToLock address ${address} ")
        val tag8BluetoothGatt = mApplication.deviceHashMap[address]
        Log.d("Nirav", "writePasswordToLock address ${tag8BluetoothGatt} ")
        if (tag8BluetoothGatt != null) {
            tag8BluetoothGatt.startWritePassword2Lock(passcode)
        }
    }


    private fun turnAllBleDevicesOff() {
        Log.d(TAG, "turnAllBleDevicesOff: 256 ${mApplication.deviceHashMap.size}")
        mApplication.deviceHashMap.keys.forEach {
            mApplication.deviceHashMap[it]?.setLastLocation()
            mApplication.deviceHashMap[it]?.updateBluetoothOff()
        }
    }

    private fun turnAllBleDevicesOn() {
        Log.d(TAG, "turnAllBleDevicesOn: 263 ${mApplication.deviceHashMap.size}")
        mApplication.deviceHashMap.keys.forEach {
            mApplication.deviceHashMap[it]?.updateBluetoothOn()
        }
    }

    fun toggleMode(macAddress: String) {
        Log.d(TAG, "toggleMode: 270 ${mApplication.deviceHashMap.size}")
        mApplication.deviceHashMap.keys.forEach {
            if (macAddress.equals(it)) {
                mApplication.deviceHashMap[it]?.toggleMode(macAddress)
            }
        }
    }

    fun getBatterLevel(macAddress: String): Boolean {
        Log.d(TAG, "getBatterLevel: 279 ${mApplication.deviceHashMap.size}")
        var isGetBatteryInfo = false
        mApplication.deviceHashMap.keys.forEach {
            if (macAddress == it) {
                isGetBatteryInfo = mApplication.deviceHashMap[it]?.getBattery(macAddress)!!
            }
        }
        return isGetBatteryInfo
    }

    fun createAndAddBleIntoList(beaconDevice: BeaconDevice) {
        Log.d(TAG, "createAndAddBleIntoList: beacondevice $beaconDevice")
        val tag8BluetoothGatt =
            Tag8BluetoothGatt(this@BluetoothLeService, mBluetoothAdapter, mCallbacks)
        tag8BluetoothGatt.beaconDevice = beaconDevice
        if (mApplication!!.deviceHashMap[beaconDevice.macAddress] == null) {
            mApplication.deviceHashMap[beaconDevice.macAddress] = tag8BluetoothGatt
            Log.d(TAG, "createAndAddBleIntoList: added")
            tag8BluetoothGatt.reConnect()
        }
    }

    fun connectBeacon(beaconDevice: BeaconDevice) {
//        if (beaconDevice.enableBeaconRing) {
//            updateEnableBeaconDevice(beaconDevice, "TrackerUpdate")
//        }
        if (mApplication.deviceHashMap.size > 0) {
            val tag8BluetoothGatt = mApplication.deviceHashMap[beaconDevice.macAddress]
            tag8BluetoothGatt?.connect()
        } else {
            val tag8BluetoothGatt =
                Tag8BluetoothGatt(this@BluetoothLeService, mBluetoothAdapter, mCallbacks)
            tag8BluetoothGatt.beaconDevice = beaconDevice
            if (mApplication.deviceHashMap[beaconDevice.macAddress] == null) {
                mApplication.deviceHashMap[beaconDevice.macAddress] = tag8BluetoothGatt
                tag8BluetoothGatt.reConnect()

            }
        }

    }

}