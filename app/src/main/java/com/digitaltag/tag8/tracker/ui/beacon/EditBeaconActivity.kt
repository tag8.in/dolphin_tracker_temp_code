package com.digitaltag.tag8.tracker.ui.beacon

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.digitaltag.tag8.tracker.NavigationActivity
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.apis.ApiLiveData
import com.digitaltag.tag8.tracker.baseui.BaseActivity
import com.digitaltag.tag8.tracker.ble.BluetoothLeService
import com.digitaltag.tag8.tracker.ble.Tag8BluetoothGatt
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.model.BeaconDevice
import com.digitaltag.tag8.tracker.ui.camera.CropImageActivity
import com.digitaltag.tag8.tracker.ui.endless.log
import com.digitaltag.tag8.tracker.util.PermissionUtils
import com.digitaltag.tag8.tracker.util.Utils
import com.digitaltag.tag8.tracker.util.setSafeOnClickListener
import com.google.android.gms.location.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_edit_beacon.*
import kotlinx.android.synthetic.main.fragment_secondary_beacon.*
import okhttp3.*
import okio.IOException
import java.io.File
import java.util.*
import kotlin.collections.HashMap


class EditBeaconActivity : BaseActivity(), BluetoothLeService.Callback {

    private var mServiceConnection: ServiceConnection? = null
    private var mBluetoothLeService: BluetoothLeService? = null
    private var isNew = false
    private var isSubmitclicked = false
    private var beaconDevice: BeaconDevice? = null
    private val TAKE_PHOTO_REQUEST = 89
    private var currentPhotoPath: String = ""
    private lateinit var auth: FirebaseAuth
    private var latitude: String = ""
    private var longitude: String = ""
    var type = ""
    var mFusedLocationClient: FusedLocationProviderClient? = null
    var tagNameMap: HashMap<String, HashMap<String, String>>? = null

    private val PERMISSIONS_REQUEST_CODE = 10

    var mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent != null && intent.action != null && intent.action == "com.deb.imagecrop") {
                var path = intent.getStringExtra("imagepath")
                val mainfile = File(path)
                val uri = Uri.fromFile(mainfile)
                SharedPreferenceManager.addTypeImage(uri.toString(), beaconDevice!!.type)
                beaconDevice?.type = uri.toString()
                Glide.with(this@EditBeaconActivity).load(uri).into(mainimg)

            }
        }

    }

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 999
        var mCroppedImage: Bitmap? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_beacon)

        Tag8Application.getInstance().setActivity(this)
        var intentFilter = IntentFilter()
        intentFilter.addAction("com.deb.imagecrop")
        registerReceiver(mReceiver, intentFilter)
        auth = Firebase.auth
        Tag8Application.getInstance().setActivity(this)

        mServiceConnection = object : ServiceConnection {
            override fun onServiceDisconnected(name: ComponentName?) {}

            override fun onServiceConnected(name: ComponentName?, iBinder: IBinder?) {
                if (iBinder is BluetoothLeService.LocalBinder) {
                    mBluetoothLeService = iBinder.service
                    mBluetoothLeService?.addCallback(this@EditBeaconActivity)
                    mBluetoothLeService!!.getBatterLevel(beaconDevice!!.macAddress)
                }
            }

        }
        backarrow.setSafeOnClickListener {
            finish()
        }

        bindService(
            Intent(this, BluetoothLeService::class.java),
            mServiceConnection!!,
            Context.BIND_AUTO_CREATE
        )
        beaconDevice = intent.getParcelableExtra<BeaconDevice>(Constants.BEACON_DEVICE)
        isNew = intent?.getBooleanExtra(Constants.IS_NEW, false) ?: false
        if (beaconDevice != null) {
            tagNameMap = SharedPreferenceManager.getTagNameKey()
            if (tagNameMap == null) {
                tagNameMap = HashMap()
                var keyHashMap = HashMap<String, String>()
                keyHashMap[beaconDevice!!.macAddress] = resources.getString(R.string.key)
                tagNameMap!![Constants.Beacon.Type.Key] = keyHashMap

                var walletHashMap = HashMap<String, String>()
                walletHashMap[beaconDevice!!.macAddress] = resources.getString(R.string.wallet)
                tagNameMap!![Constants.Beacon.Type.Wallet] = walletHashMap

                var passportHashMap = HashMap<String, String>()
                passportHashMap[beaconDevice!!.macAddress] = resources.getString(R.string.passport)
                tagNameMap!![Constants.Beacon.Type.Passport] = passportHashMap

                var bagHashMap = HashMap<String, String>()
                bagHashMap[beaconDevice!!.macAddress] = resources.getString(R.string.bag)
                tagNameMap!![Constants.Beacon.Type.Bag] = bagHashMap

                var cardHolderHashMap = HashMap<String, String>()
                cardHolderHashMap[beaconDevice!!.macAddress] = resources.getString(R.string.card)
                tagNameMap!![Constants.Beacon.Type.CardHolder] = cardHolderHashMap

                var cameraHashMap = HashMap<String, String>()
                cameraHashMap[beaconDevice!!.macAddress] = resources.getString(R.string.camera)
                tagNameMap!![Constants.Beacon.Type.Camera] = cameraHashMap

                var petHashMap = HashMap<String, String>()
                petHashMap[beaconDevice!!.macAddress] = resources.getString(R.string.pet)
                tagNameMap!![Constants.Beacon.Type.Pet] = petHashMap

                var laptopHashMap = HashMap<String, String>()
                laptopHashMap[beaconDevice!!.macAddress] = resources.getString(R.string.laptop)
                tagNameMap!![Constants.Beacon.Type.Laptop] = laptopHashMap

                var purseHashMap = HashMap<String, String>()
                purseHashMap[beaconDevice!!.macAddress] = resources.getString(R.string.purse)
                tagNameMap!![Constants.Beacon.Type.Purse] = purseHashMap

                var toyHashMap = HashMap<String, String>()
                toyHashMap[beaconDevice!!.macAddress] = resources.getString(R.string.toy)
                tagNameMap!![Constants.Beacon.Type.Toy] = toyHashMap

                var remoteHashMap = HashMap<String, String>()
                remoteHashMap[beaconDevice!!.macAddress] = resources.getString(R.string.remote)
                tagNameMap!![Constants.Beacon.Type.Remote] = remoteHashMap

                var otherHashMap = HashMap<String, String>()
                otherHashMap[beaconDevice!!.macAddress] = resources.getString(R.string.other)
                tagNameMap!![Constants.Beacon.Type.Other] = otherHashMap
                SharedPreferenceManager.addTagNameKey(tagNameMap!!)
            } else {
                var keyMap = tagNameMap!![Constants.Beacon.Type.Key]
                if (keyMap != null) {
                    if (!keyMap.contains(beaconDevice!!.macAddress)) {
                        keyMap[beaconDevice!!.macAddress] = resources.getString(R.string.key)
                    }
                }

                var walletMap = tagNameMap!![Constants.Beacon.Type.Wallet]
                if (walletMap != null) {
                    if (!walletMap.contains(beaconDevice!!.macAddress)) {
                        walletMap[beaconDevice!!.macAddress] = resources.getString(R.string.wallet)
                    }
                }

                var passportMap = tagNameMap!![Constants.Beacon.Type.Passport]
                if (passportMap != null) {
                    if (!passportMap.contains(beaconDevice!!.macAddress)) {
                        passportMap[beaconDevice!!.macAddress] =
                            resources.getString(R.string.passport)
                    }
                }

                var bagMap = tagNameMap!![Constants.Beacon.Type.Bag]
                if (bagMap != null) {
                    if (!bagMap.contains(beaconDevice!!.macAddress)) {
                        bagMap[beaconDevice!!.macAddress] = resources.getString(R.string.bag)
                    }
                }

                var petMap = tagNameMap!![Constants.Beacon.Type.Pet]
                if (petMap != null) {
                    if (!petMap.contains(beaconDevice!!.macAddress)) {
                        petMap[beaconDevice!!.macAddress] = resources.getString(R.string.pet)
                    }
                }

                var laptopMap = tagNameMap!![Constants.Beacon.Type.Laptop]
                if (laptopMap != null) {
                    if (!laptopMap.contains(beaconDevice!!.macAddress)) {
                        laptopMap[beaconDevice!!.macAddress] = resources.getString(R.string.laptop)
                    }
                }

                var purseMap = tagNameMap!![Constants.Beacon.Type.Purse]
                if (purseMap != null) {
                    if (!purseMap.contains(beaconDevice!!.macAddress)) {
                        purseMap[beaconDevice!!.macAddress] = resources.getString(R.string.purse)
                    }
                }

                var cardHolderMap = tagNameMap!![Constants.Beacon.Type.CardHolder]
                if (cardHolderMap != null) {
                    if (!cardHolderMap.contains(beaconDevice!!.macAddress)) {
                        cardHolderMap[beaconDevice!!.macAddress] =
                            resources.getString(R.string.card)
                    }
                }

                var cameraHolderMap = tagNameMap!![Constants.Beacon.Type.Camera]
                if (cameraHolderMap != null) {
                    if (!cameraHolderMap.contains(beaconDevice!!.macAddress)) {
                        cameraHolderMap[beaconDevice!!.macAddress] =
                            resources.getString(R.string.camera)
                    }
                }

                var laptopHolderMap = tagNameMap!![Constants.Beacon.Type.Laptop]
                if (laptopHolderMap != null) {
                    if (!laptopHolderMap.contains(beaconDevice!!.macAddress)) {
                        laptopHolderMap[beaconDevice!!.macAddress] =
                            resources.getString(R.string.laptop)
                    }
                }


                var petHolderMap = tagNameMap!![Constants.Beacon.Type.Pet]
                if (petHolderMap != null) {
                    if (!petHolderMap.contains(beaconDevice!!.macAddress)) {
                        petHolderMap[beaconDevice!!.macAddress] = resources.getString(R.string.pet)
                    }
                }

                var toyHolderMap = tagNameMap!![Constants.Beacon.Type.Toy]
                if (toyHolderMap != null) {
                    if (!toyHolderMap.contains(beaconDevice!!.macAddress)) {
                        toyHolderMap[beaconDevice!!.macAddress] = resources.getString(R.string.toy)
                    }
                }

                var carMap = tagNameMap!![Constants.Beacon.Type.Car]
                if (carMap != null) {
                    if (!carMap.contains(beaconDevice!!.macAddress)) {
                        carMap[beaconDevice!!.macAddress] = resources.getString(R.string.car)
                    }
                }

                var remoteMap = tagNameMap!![Constants.Beacon.Type.Remote]
                if (remoteMap != null) {
                    if (!remoteMap.contains(beaconDevice!!.macAddress)) {
                        remoteMap[beaconDevice!!.macAddress] = resources.getString(R.string.remote)
                    }
                }

                var otherMap = tagNameMap!![Constants.Beacon.Type.Other]
                if (otherMap != null) {
                    if (!otherMap.contains(beaconDevice!!.macAddress)) {
                        otherMap[beaconDevice!!.macAddress] = resources.getString(R.string.other)
                    }
                }
                SharedPreferenceManager.addTagNameKey(tagNameMap!!)
            }
            type = beaconDevice!!.type
            imageSelected(type)
            when (type) {
                Constants.Beacon.Type.Key, "" -> {
                    mainimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            this@EditBeaconActivity,
                            R.drawable.key
                        )
                    )
                    beaconDevice?.type = Constants.Beacon.Type.Key
                    var name = getName(Constants.Beacon.Type.Key)
                    devicename.setText(name)
                    beaconDevice?.bleDeviceName = name
                }
                Constants.Beacon.Type.Wallet -> {
                    mainimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            this@EditBeaconActivity,
                            R.drawable.wallet
                        )
                    )
                    var name = getName(type)
                    devicename.setText(name)
                    beaconDevice?.bleDeviceName = name
                }
                Constants.Beacon.Type.Passport -> {
                    mainimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            this@EditBeaconActivity,
                            R.drawable.passport_vextor
                        )
                    )
                    var name = getName(type)
                    devicename.setText(name)
                    beaconDevice?.bleDeviceName = name
                }
                Constants.Beacon.Type.Bag -> {
                    mainimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            this@EditBeaconActivity,
                            R.drawable.ic_bag
                        )
                    )
                    var name = getName(type)
                    devicename.setText(name)
                    beaconDevice?.bleDeviceName = name
                }
                Constants.Beacon.Type.Purse -> {
                    mainimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            this@EditBeaconActivity,
                            R.drawable.purse
                        )
                    )
                    var name = getName(type)
                    devicename.setText(name)
                    beaconDevice?.bleDeviceName = name
                }
                Constants.Beacon.Type.CardHolder -> {
                    mainimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            this@EditBeaconActivity,
                            R.drawable.ic_card_holder
                        )
                    )
                    var name = getName(type)
                    devicename.setText(name)
                    beaconDevice?.bleDeviceName = name
                }
                Constants.Beacon.Type.Camera -> {
                    mainimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            this@EditBeaconActivity,
                            R.drawable.ic_camer
                        )
                    )
                    var name = getName(type)
                    devicename.setText(name)
                    beaconDevice?.bleDeviceName = name
                }
                Constants.Beacon.Type.Pet -> {
                    mainimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            this@EditBeaconActivity,
                            R.drawable.pett
                        )
                    )
                    var name = getName(type)
                    devicename.setText(name)
                    beaconDevice?.bleDeviceName = name
                }
                Constants.Beacon.Type.Laptop -> {
                    mainimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            this@EditBeaconActivity,
                            R.drawable.laptop
                        )
                    )
                    var name = getName(type)
                    devicename.setText(name)
                    beaconDevice?.bleDeviceName = name
                }
                Constants.Beacon.Type.Toy -> {
                    mainimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            this@EditBeaconActivity,
                            R.drawable.ic_toy
                        )
                    )
                    var name = getName(type)
                    devicename.setText(name)
                    beaconDevice?.bleDeviceName = name
                }
                Constants.Beacon.Type.Other -> {
                    mainimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            this@EditBeaconActivity,
                            R.drawable.other
                        )
                    )
                    var name = getName(type)
                    devicename.setText(name)
                    beaconDevice?.bleDeviceName = name
                }
                Constants.Beacon.Type.Remote -> {
                    mainimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            this@EditBeaconActivity,
                            R.drawable.remot
                        )
                    )
                    var name = getName(type)
                    devicename.setText(name)
                    beaconDevice?.bleDeviceName = name
                }
                Constants.Beacon.Type.Car -> {
                    mainimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            this@EditBeaconActivity,
                            R.drawable.car
                        )
                    )
                    var name = getName(type)
                    devicename.setText(name)
                    beaconDevice?.bleDeviceName = name
                }
                else -> {
                    Glide.with(this).load(beaconDevice?.type).into(mainimg)
                    val images = SharedPreferenceManager.getTypeImages()
                    if (images?.get(type) != null) {
                        images[type]?.let { devicename.setText(it) }
                    }
                }
            }
        }

        changeimg.setSafeOnClickListener {
            if (PermissionUtils.hasCameraPermissions(this)) {
                openCamera()
            } else {
                PermissionUtils.requestCameraPermission(this, PERMISSIONS_REQUEST_CODE)
            }
        }

        keylayout.setSafeOnClickListener { keyClick() }
        keyimgsel.setSafeOnClickListener { keyClick() }
        keyimg.setSafeOnClickListener { keyClick() }

        walletlayout.setSafeOnClickListener { walletClick() }
        walletimgsel.setSafeOnClickListener { walletClick() }
        walletimg.setSafeOnClickListener { walletClick() }

        purselayout.setSafeOnClickListener { purseClick() }
        purseimgsel.setSafeOnClickListener { purseClick() }
        purseimg.setSafeOnClickListener { purseClick() }

        cardHolderlayout.setSafeOnClickListener { cardHolderClick() }
        card.setSafeOnClickListener { cardHolderClick() }
        cardimg.setSafeOnClickListener { cardHolderClick() }

        camera.setSafeOnClickListener { cameraClick() }
        cameralayout.setSafeOnClickListener { cameraClick() }
        camerimg.setSafeOnClickListener { cameraClick() }


        laptoplayout.setSafeOnClickListener { laptopClick() }
        laptopimgsel.setSafeOnClickListener { laptopClick() }
        laptopimg.setSafeOnClickListener { laptopClick() }

        toy.setSafeOnClickListener { toyClick() }
        toyimg.setSafeOnClickListener { toyClick() }
        toylayout.setSafeOnClickListener { toyClick() }


        petlayout.setSafeOnClickListener { petClick() }
        petimgsel.setSafeOnClickListener { petClick() }
        petimg.setSafeOnClickListener { petClick() }

        bag.setSafeOnClickListener { bagClick() }
        bagimg.setSafeOnClickListener { bagClick() }
        baglayout.setSafeOnClickListener { bagClick() }

        passport.setSafeOnClickListener { passportClick() }
        passportimg.setSafeOnClickListener { passportClick() }
        passportlayout.setSafeOnClickListener { passportClick() }

        remotelayout.setSafeOnClickListener { remoteClick() }
        remoteimgsel.setSafeOnClickListener { remoteClick() }
        remoteimg.setSafeOnClickListener { remoteClick() }

        otherlayout.setSafeOnClickListener { otherClick() }
        otherimgsel.setSafeOnClickListener { otherClick() }
        otherimg.setSafeOnClickListener { otherClick() }
        submit.setSafeOnClickListener {
            submit()
        }
        if (beaconDevice?.enableBeaconRing!!) {
            if (beaconDevice != null) {
                beaconDevice?.enableBeaconRing = true
                BeaconActivity().updateAlertOptions(beaconDevice)
            }
        }
//
//        keyClick()
//        Handler(Looper.myLooper()!!).postDelayed({
//            submit()
//            //  mBluetoothLeService?.updateNames(beaconDevice!!)
//        }, 2000)

    }

    override fun onResume() {
        super.onResume()
        Tag8Application.getInstance().setActivity(this)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        submit()
    }

    fun submit() {
        if (Utils.checkNetwork()) {
            val uid = auth.currentUser?.uid
            isSubmitclicked = true
            var type1 = type
            if (type == "") {
                type1 = Constants.Beacon.Type.Key
            }
            var tagMap = tagNameMap!![type1]
            tagMap!![beaconDevice!!.macAddress] = devicename.text.toString()
            SharedPreferenceManager.addTagNameKey(tagNameMap!!)
            beaconDevice?.bleDeviceName = devicename.text.toString()
            if (Utils.updateType == getString(R.string.addnew)) {
                try {
                    Thread {
                        val geocoder = Geocoder(this, Locale.getDefault())
                        val location: FusedLocationProviderClient =
                            LocationServices.getFusedLocationProviderClient(this)
                        if (ActivityCompat.checkSelfPermission(
                                this,
                                Manifest.permission.ACCESS_FINE_LOCATION
                            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                                this,
                                Manifest.permission.ACCESS_COARSE_LOCATION
                            ) != PackageManager.PERMISSION_GRANTED
                        ) {
                            return@Thread
                        }
                        location.lastLocation.addOnSuccessListener {
                            try {
                                if (it == null) {
                                    return@addOnSuccessListener
                                }
                                val addressList =
                                    geocoder.getFromLocation(it.latitude, it.longitude, 1)

                                Log.d(
                                    "EditBeaconActivity",
                                    "addresslist 1 :" + addressList + " " + addressList.size
                                )
                                val address = addressList[0]
                                //containerView.devicemaplocation.text = Html.fromHtml("<a href=''>${address.getAddressLine(0)}</a>")
                                //containerView.devicemaplocation.movementMethod = LinkMovementMethod.getInstance()
                                Log.d(
                                    "beaconAdapter",
                                    "address" + address.getAddressLine(0) + " " + uid.toString()
                                )
                                var beaconAddress = address.getAddressLine(0)

                                Log.d("beaconAdapter", "address 1" + address.getAddressLine(0))
                                sendLocation(uid.toString(), beaconAddress)

                            } catch (nullpointer: KotlinNullPointerException) {
                                Log.e(
                                    "Location Address Loader",
                                    "Unable connect get  Geocoder",
                                    nullpointer
                                )
                                sendLocation(uid.toString(), "")
                            } catch (e: IOException) {
                                Log.e("Location Address Loader", "Unable connect to Geocoder", e)
                                sendLocation(uid.toString(), "")
                            }
                        }.addOnFailureListener {
                            sendLocation(uid.toString(), "")
                        }
                    }.start()

                } catch (e: IOException) {
                    Log.e(Tag8BluetoothGatt.TAG, "connect: ", e)
                }

            } else {
                ApiLiveData.updateCategory(beaconDevice!!.macAddress, beaconDevice!!.type)
                Log.d("editbeaconactivity", "else update")

            }
            if (beaconDevice != null) {
                //zx     mBluetoothLeService?.updateNames(beaconDevice!!)
                mBluetoothLeService?.updateBeaconDevice(beaconDevice!!)
                ApiLiveData.updateCategory(beaconDevice!!.macAddress, beaconDevice!!.type)
            }
        }

    }

    private fun sendLocation(uid: String, address: String) {

        Log.d("TAG", "sendLocation: " + address + " " + uid)

        return

    }


    @SuppressLint("WrongConstant")
    fun openCamera() {
        val values = ContentValues(1)
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg")
        val fileUri = contentResolver
            .insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                values
            )
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)


        currentPhotoPath = fileUri.toString()
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        // Launch the camera intent's
        // resolved app's default activity.
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Start the image capture intent to take photo
            startActivityForResult(intent, TAKE_PHOTO_REQUEST)
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        if (isNew && !isSubmitclicked) {
            mBluetoothLeService?.deleteBeacon(beaconDevice!!)
        }
        mBluetoothLeService?.removeCallback(this@EditBeaconActivity)
        if (mServiceConnection != null) {
            unbindService(mServiceConnection!!)
        }
        unregisterReceiver(mReceiver)
    }

    override fun onDeviceAdded(beaconDevice: BeaconDevice) {

    }

    override fun onDeviceUpdated(beaconDevice: BeaconDevice) {
        if (isNew && isSubmitclicked) {
            //isSubmitclicked = false
            ApiLiveData.newConnectBeacon(
                beaconDevice.macAddress.toString(),
                beaconDevice!!.type,
                beaconDevice!!.bleDeviceName
            )
            val intent = Intent(this@EditBeaconActivity, NavigationActivity::class.java)
            intent.putExtra(Constants.BEACON_DEVICE, beaconDevice)
            startActivity(intent)
            finish()
            Log.d("Deb", "onDeviceUpdated: ")
        } else {
            //runOnUiThread { Toast.makeText(applicationContext, resources.getString(R.string.device_updated), Toast.LENGTH_SHORT).show() }
            if (isSubmitclicked) {
                finish()
            }
            // finish()
        }
    }

    override fun onDeviceRemoved(beaconDevice: BeaconDevice) {

    }

    override fun onActivityResult(
        requestCode: Int, resultCode: Int,
        data: Intent?
    ) {
        log("result code $resultCode")
        if (resultCode == Activity.RESULT_OK
            && requestCode == TAKE_PHOTO_REQUEST
        ) {
            processCapturedPhoto()
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun processCapturedPhoto() {
        val cursor = contentResolver.query(
            Uri.parse(currentPhotoPath),
            Array(1) { MediaStore.Images.ImageColumns.DATA },
            null, null, null
        )
        cursor?.moveToFirst()
        val photoPath = cursor?.getString(0)
        cursor?.close()

        var intent = Intent(this, CropImageActivity::class.java)
        var bundle = Bundle()
        bundle.putString("imagepath", photoPath)
        intent.putExtras(bundle)
        startActivityForResult(intent, 2);

        /*val uri = Uri.fromFile(mainfile)
        SharedPreferenceManager.addTypeImage(uri.toString(), beaconDevice!!.type)
        beaconDevice?.type = uri.toString()
        Glide.with(this).load(uri).into(mainimg)*/
    }

    fun imageSelected(type: String) {
        return
        when (type) {
            Constants.Beacon.Type.Key, "" -> {
                keyimg.visibility = View.GONE
                keyimgsel.visibility = View.VISIBLE

                walletimg.visibility = View.VISIBLE
                walletimgsel.visibility = View.GONE

                purseimg.visibility = View.VISIBLE
                purseimgsel.visibility = View.GONE

                laptopimg.visibility = View.VISIBLE
                laptopimgsel.visibility = View.GONE

                petimg.visibility = View.VISIBLE
                petimgsel.visibility = View.GONE

                bagimg.visibility = View.VISIBLE

                remoteimg.visibility = View.VISIBLE
                remoteimgsel.visibility = View.GONE

                otherimg.visibility = View.VISIBLE
                otherimgsel.visibility = View.GONE

            }
            Constants.Beacon.Type.Wallet -> {
                keyimg.visibility = View.VISIBLE
                keyimgsel.visibility = View.GONE

                walletimg.visibility = View.GONE
                walletimgsel.visibility = View.VISIBLE

                purseimg.visibility = View.VISIBLE
                purseimgsel.visibility = View.GONE

                laptopimg.visibility = View.VISIBLE
                laptopimgsel.visibility = View.GONE

                petimg.visibility = View.VISIBLE
                petimgsel.visibility = View.GONE


                remoteimg.visibility = View.VISIBLE
                remoteimgsel.visibility = View.GONE

                otherimg.visibility = View.VISIBLE
                otherimgsel.visibility = View.GONE
            }
            Constants.Beacon.Type.Purse -> {
                keyimg.visibility = View.VISIBLE
                keyimgsel.visibility = View.GONE

                walletimg.visibility = View.VISIBLE
                walletimgsel.visibility = View.GONE

                purseimg.visibility = View.GONE
                purseimgsel.visibility = View.VISIBLE

                laptopimg.visibility = View.VISIBLE
                laptopimgsel.visibility = View.GONE

                petimg.visibility = View.VISIBLE
                petimgsel.visibility = View.GONE


                remoteimg.visibility = View.VISIBLE
                remoteimgsel.visibility = View.GONE

                otherimg.visibility = View.VISIBLE
                otherimgsel.visibility = View.GONE
            }
            Constants.Beacon.Type.Laptop -> {
                keyimg.visibility = View.VISIBLE
                keyimgsel.visibility = View.GONE

                walletimg.visibility = View.VISIBLE
                walletimgsel.visibility = View.GONE

                purseimg.visibility = View.VISIBLE
                purseimgsel.visibility = View.GONE

                laptopimg.visibility = View.GONE
                laptopimgsel.visibility = View.VISIBLE

                petimg.visibility = View.VISIBLE
                petimgsel.visibility = View.GONE


                remoteimg.visibility = View.VISIBLE
                remoteimgsel.visibility = View.GONE

                otherimg.visibility = View.VISIBLE
                otherimgsel.visibility = View.GONE
            }
            Constants.Beacon.Type.Pet -> {
                keyimg.visibility = View.VISIBLE
                keyimgsel.visibility = View.GONE

                walletimg.visibility = View.VISIBLE
                walletimgsel.visibility = View.GONE

                purseimg.visibility = View.VISIBLE
                purseimgsel.visibility = View.GONE

                laptopimg.visibility = View.VISIBLE
                laptopimgsel.visibility = View.GONE

                petimg.visibility = View.GONE
                petimgsel.visibility = View.VISIBLE

                remoteimg.visibility = View.VISIBLE
                remoteimgsel.visibility = View.GONE

                otherimg.visibility = View.VISIBLE
                otherimgsel.visibility = View.GONE
            }
            Constants.Beacon.Type.Car -> {
                keyimg.visibility = View.VISIBLE
                keyimgsel.visibility = View.GONE

                walletimg.visibility = View.VISIBLE
                walletimgsel.visibility = View.GONE

                purseimg.visibility = View.VISIBLE
                purseimgsel.visibility = View.GONE

                laptopimg.visibility = View.VISIBLE
                laptopimgsel.visibility = View.GONE

                petimg.visibility = View.VISIBLE
                petimgsel.visibility = View.GONE

                remoteimg.visibility = View.VISIBLE
                remoteimgsel.visibility = View.GONE

                otherimg.visibility = View.VISIBLE
                otherimgsel.visibility = View.GONE
            }
            Constants.Beacon.Type.Remote -> {
                keyimg.visibility = View.VISIBLE
                keyimgsel.visibility = View.GONE

                walletimg.visibility = View.VISIBLE
                walletimgsel.visibility = View.GONE

                purseimg.visibility = View.VISIBLE
                purseimgsel.visibility = View.GONE

                laptopimg.visibility = View.VISIBLE
                laptopimgsel.visibility = View.GONE

                petimg.visibility = View.VISIBLE
                petimgsel.visibility = View.GONE
                remoteimg.visibility = View.GONE
                remoteimgsel.visibility = View.VISIBLE

                otherimg.visibility = View.VISIBLE
                otherimgsel.visibility = View.GONE
            }
            Constants.Beacon.Type.Other -> {
                keyimg.visibility = View.VISIBLE
                keyimgsel.visibility = View.GONE

                walletimg.visibility = View.VISIBLE
                walletimgsel.visibility = View.GONE

                purseimg.visibility = View.VISIBLE
                purseimgsel.visibility = View.GONE

                laptopimg.visibility = View.VISIBLE
                laptopimgsel.visibility = View.GONE

                petimg.visibility = View.VISIBLE
                petimgsel.visibility = View.GONE

                remoteimg.visibility = View.VISIBLE
                remoteimgsel.visibility = View.GONE

                otherimg.visibility = View.GONE
                otherimgsel.visibility = View.VISIBLE
            }
            else -> {
                var images = SharedPreferenceManager.getTypeImages()
                if (images?.get(type) != null) {
                    images[type]?.let { imageSelected(it) }
                }
            }
        }
    }

    fun keyClick() {
        mainimg.setImageDrawable(ContextCompat.getDrawable(this@EditBeaconActivity, R.drawable.key))
        type = Constants.Beacon.Type.Key
        beaconDevice?.type = type
        var name = getName(type)
        devicename.setText(name)
        beaconDevice?.bleDeviceName = name
        imageSelected(type)
    }

    fun walletClick() {
        mainimg.setImageDrawable(
            ContextCompat.getDrawable(
                this@EditBeaconActivity,
                R.drawable.wallet
            )
        )
        type = Constants.Beacon.Type.Wallet
        beaconDevice?.type = type
        var name = getName(type)
        devicename.setText(name)
        beaconDevice?.bleDeviceName = name
        imageSelected(type)
    }

    fun purseClick() {
        mainimg.setImageDrawable(
            ContextCompat.getDrawable(
                this@EditBeaconActivity,
                R.drawable.purse
            )
        )
        type = Constants.Beacon.Type.Purse
        beaconDevice?.type = type
        var name = getName(type)
        devicename.setText(name)
        beaconDevice?.bleDeviceName = name
        imageSelected(type)
    }

    fun cardHolderClick() {
        mainimg.setImageDrawable(
            ContextCompat.getDrawable(
                this@EditBeaconActivity,
                R.drawable.ic_card_holder
            )
        )
        type = Constants.Beacon.Type.CardHolder
        beaconDevice?.type = type
        var name = getName(type)
        devicename.setText(name)
        beaconDevice?.bleDeviceName = name
        imageSelected(type)
    }

    fun cameraClick() {
        mainimg.setImageDrawable(
            ContextCompat.getDrawable(
                this@EditBeaconActivity,
                R.drawable.ic_card_holder
            )
        )
        type = Constants.Beacon.Type.Camera
        beaconDevice?.type = type
        var name = getName(type)
        devicename.setText(name)
        beaconDevice?.bleDeviceName = name
        imageSelected(type)
    }

    fun laptopClick() {
        mainimg.setImageDrawable(
            ContextCompat.getDrawable(
                this@EditBeaconActivity,
                R.drawable.laptop
            )
        )
        type = Constants.Beacon.Type.Laptop
        Log.d("TAG", "getName: Types $type")
        beaconDevice?.type = type
        var name = getName(type)
        devicename.setText(name)
        beaconDevice?.bleDeviceName = name
        imageSelected(type)
    }

    fun toyClick() {
        mainimg.setImageDrawable(
            ContextCompat.getDrawable(
                this@EditBeaconActivity,
                R.drawable.ic_toy
            )
        )
        type = Constants.Beacon.Type.Toy
        Log.d("TAG", "getName: Types $type")
        beaconDevice?.type = type
        var name = getName(type)
        devicename.setText(name)
        beaconDevice?.bleDeviceName = name
        imageSelected(type)
    }

    fun bagClick() {
        mainimg.setImageDrawable(
            ContextCompat.getDrawable(
                this@EditBeaconActivity,
                R.drawable.ic_bag
            )
        )
        type = Constants.Beacon.Type.Bag
        beaconDevice?.type = type
        var name = getName(type)
        devicename.setText(name)
        beaconDevice?.bleDeviceName = name
        imageSelected(type)
    }

    fun petClick() {
        mainimg.setImageDrawable(
            ContextCompat.getDrawable(
                this@EditBeaconActivity,
                R.drawable.pett
            )
        )
        type = Constants.Beacon.Type.Pet
        beaconDevice?.type = type
        var name = getName(type)
        devicename.setText(name)
        beaconDevice?.bleDeviceName = name
        imageSelected(type)
    }

    fun passportClick() {
        Log.d("TAG", "onCreate: Started Passports : started")
        mainimg.setImageDrawable(
            ContextCompat.getDrawable(
                this@EditBeaconActivity,
                R.drawable.passport_vextor
            )
        )
        type = Constants.Beacon.Type.Passport
        beaconDevice?.type = type
        var name = getName(type)
        devicename.setText(name)
        beaconDevice?.bleDeviceName = name
        imageSelected(type)
    }

    fun remoteClick() {
        mainimg.setImageDrawable(
            ContextCompat.getDrawable(
                this@EditBeaconActivity,
                R.drawable.remot
            )
        )
        type = Constants.Beacon.Type.Remote
        beaconDevice?.type = type
        var name = getName(type)
        devicename.setText(name)
        beaconDevice?.bleDeviceName = name
        imageSelected(type)
    }

    fun otherClick() {
        mainimg.setImageDrawable(
            ContextCompat.getDrawable(
                this@EditBeaconActivity,
                R.drawable.other
            )
        )
        type = Constants.Beacon.Type.Other
        beaconDevice?.type = type
        var name = getName(type)
        devicename.setText(name)
        beaconDevice?.bleDeviceName = name
        imageSelected(type)
    }

    @SuppressLint("MissingPermission")
    private fun setUpLocationListener() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        // for getting the current location update after every 2 seconds with high accuracy
        val locationRequest = LocationRequest().setInterval(10000).setFastestInterval(10000)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        mFusedLocationClient!!.requestLocationUpdates(
            locationRequest,
            object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult) {
                    super.onLocationResult(locationResult)
                    for (location in locationResult.locations) {
                        latitude = location.latitude.toString()
                        longitude = location.longitude.toString()
                    }
                    // Few more things we can do here:
                    // For example: Update the location of user on server
                    Log.d("EditBeaconActivity", "Location" + locationResult)
                }
            },
            Looper.myLooper()
        )
    }

    override fun onStart() {
        super.onStart()
        when {
            PermissionUtils.isAccessFineLocationGranted(this) -> {
                when {
                    PermissionUtils.isLocationEnabled(this) -> {
                        setUpLocationListener()
                    }
                    else -> {
                        PermissionUtils.showGPSNotEnabledDialog(this)
                    }
                }
            }
            else -> {
                PermissionUtils.requestAccessFineLocationPermission(
                    this,
                    LOCATION_PERMISSION_REQUEST_CODE
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    when {
                        PermissionUtils.isLocationEnabled(this) -> {
                            setUpLocationListener()
                        }
                        else -> {
                            PermissionUtils.showGPSNotEnabledDialog(this)
                        }
                    }
                } else {
                    Toast.makeText(
                        this,
                        "Permission not granted",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            PERMISSIONS_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openCamera()
                } else {
                    Toast.makeText(
                        this,
                        "Permission not granted",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    fun getName(type: String): String {
        var name = ""
        var tagMap = tagNameMap!![type]
        Log.d("TAG", "getName: Types $tagMap")
        if (tagMap!!.containsKey(beaconDevice!!.macAddress)) {
            name = tagMap!![beaconDevice!!.macAddress]!!.toString()
        }
        return name
    }


}