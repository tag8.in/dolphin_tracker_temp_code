package com.digitaltag.tag8.tracker.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.model.BeaconDevice

class SavedwifiFragment : Fragment() {

    private var beaconDevice: BeaconDevice? = null

    companion object {
        fun newInstance(beaconDevice: BeaconDevice): SavedwifiFragment {
            return SavedwifiFragment().apply {
                val bundle = Bundle().apply {
                    putParcelable(Constants.BEACON_DEVICE, beaconDevice)
                }
                arguments = bundle
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_saved_wifi, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }


}