package com.digitaltag.tag8.tracker.model.offlinebeacon

data class Data(val _id: String,
                val _v: Int,
                val beacon_connectionstatus: String,
                val device_id: String,
                val id: String,
                val latitude: String,
                val longitude: String,
                val location: String,
                val mac_address: String,
                val modifiedOn: String,
                val name: String,
                val timestamp: String,
                val type: String,
                val uid: String,
                val v: Any)
