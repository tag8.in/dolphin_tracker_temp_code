package com.digitaltag.tag8.tracker.firebase

import android.util.Log
import com.digitaltag.tag8.tracker.util.DebugLogger
import com.digitaltag.tag8.tracker.slider.SliderActivity
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

object FirebaseAuthManager {

    private val TAG = DebugLogger.makeLogTag(
        FirebaseAuthManager::class.java
    )
    private var auth: FirebaseAuth? = null

    init {
        auth = Firebase.auth
    }

    fun getCurrentUser(): FirebaseUser? {
        return auth?.currentUser
    }

    fun firebaseAuthWithGoogle(activity: SliderActivity, taskGsia: Task<GoogleSignInAccount>) {
        try {
            val account = taskGsia.getResult(ApiException::class.java)!!
            Log.d(TAG, "firebaseAuthWithGoogle:" + account.id)
            val credential = GoogleAuthProvider.getCredential(account.idToken, null)
            auth?.signInWithCredential(credential)
                    ?.addOnCompleteListener(activity) { task ->
                        if (task.isSuccessful) {
                            Log.d(TAG, "signInWithCredential:success")
                            activity.onGoogleLoginSuccess(auth?.currentUser)
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.exception)
                            activity.onGoogleLoginFail(task.exception?.localizedMessage
                                    ?: "Google Login Error")
                        }
                    }
        } catch (e: ApiException) {
            Log.w(TAG, "Google sign in failed", e)
        }
    }

    fun logout() {
        auth?.signOut()
    }

}