package com.digitaltag.tag8.tracker

import android.Manifest
import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.speech.tts.TextToSpeech
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.digitaltag.tag8.tracker.ble.BLEBackgroundScan
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.ui.endless.log
import com.digitaltag.tag8.tracker.util.*
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_navigation.*
import java.text.SimpleDateFormat
import java.util.*


class NavigationActivity : AppCompatActivity(), BluetoothStateListeners.BluetoothChange {

    private var firebaseUser: FirebaseUser? = null
    private lateinit var auth: FirebaseAuth

    companion object {
        val showBeacons = false
    }

    var t1: TextToSpeech? = null

    private lateinit var fusedLocationClient: FusedLocationProviderClient


    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        Tag8Application.getInstance().setActivity(this)
        startService(Intent(this, BLEBackgroundScan::class.java))
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_map, R.id.navigation_settings
            )
        )
        val currentTime: String = SimpleDateFormat("HH", Locale.getDefault()).format(Date())
        val type = intent.getStringExtra("stop_snooze")
        val mac_address = intent.getStringExtra("mac_address")
        if (type == "stop") {
            SharedPreferenceManager.addSnoozeHours(mac_address.toString(), currentTime)
        }
        BluetoothStateListeners().getBLEState(this)
        //  setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        log("Size of the map ${Tag8Application.getInstance().deviceHashMap.size}")
        var deviceList = Tag8Application.getInstance().deviceHashMap
        var istag8call = false
        var macAddress = ""
        if (deviceList.isNotEmpty()) {
            intent?.let {
                istag8call = it.getBooleanExtra("tag8call", false)
                macAddress = it.getStringExtra("macaddress").toString()
            }
            if (istag8call && macAddress.isNotEmpty()) {
                var tag8ble = deviceList[macAddress]
                tag8ble?.unRingDevice()
            }
        }

        val filter1 = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        registerReceiver(BluetoothStateChange(), filter1)

        auth = Firebase.auth
        val uid = auth.currentUser?.uid

        Handler(Looper.myLooper()!!).postDelayed({
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                openDialog.setOnClickListener {
                    val myIntent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION)
                    startActivity(myIntent)
                }
            } else {
                openDialog.visibility = View.GONE
            }
        }, 2000)
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled) {

            try {
                MaterialAlertDialogBuilder(this)
                    .setTitle(resources.getString(R.string.bluetooth_is_off))
                    .setMessage(resources.getString(R.string.bluetooth_is_off_desc))
                    .setNeutralButton(resources.getString(R.string.not_now)) { dialog, _ ->
                        dialog.dismiss()
                    }
                    .setPositiveButton(resources.getString(R.string.enable)) { dialog, _ ->
                        val adapter = BluetoothAdapter.getDefaultAdapter()
                        if (!adapter.isEnabled) {
                            adapter.enable()
                        }
                        dialog.dismiss()
                    }
                    .show()
            } catch (e: Exception) {
                e.message
            }
        }

    }

    override fun onStart() {
        super.onStart()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.canDrawOverlays(this)) {
                openDialog.visibility = View.GONE
            } else {
                openDialog.visibility = View.VISIBLE
            }
        }
    }

    override fun onResume() {
        super.onResume()
        Tag8Application.getInstance().setActivity(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun state(state: Int) {
        when (intent?.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)) {
            BluetoothAdapter.STATE_OFF -> {

            }
            BluetoothAdapter.STATE_TURNING_OFF -> {
            }
        }
    }



}