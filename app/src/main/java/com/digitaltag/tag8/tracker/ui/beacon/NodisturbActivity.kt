package com.digitaltag.tag8.tracker.ui.beacon

import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.getFromTimer
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.getToTimer
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.setFromTimer
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.setToTimer
import com.digitaltag.tag8.tracker.util.setSafeOnClickListener
import kotlinx.android.synthetic.main.activity_nodisturb.*

class NodisturbActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nodisturb)
        Tag8Application.getInstance().setActivity(this)
        fromtimertxt.text = getFromTimer()
        fromtotxt.text = getToTimer()
        var hour: Int
        var min: Int
        var timervar: String = getString(R.string.fromtimer)


        if (getToTimer().isNullOrBlank()) {
            fromtotxt.text = getToTimer()
        }
        if (getFromTimer().isNullOrBlank()) {
            fromtimertxt.text = getFromTimer()
        }
        timervar = getString(R.string.fromtimer)
        fromtimer.setBackgroundColor(ContextCompat.getColor(this, R.color.grey_color))
        setTimeIntoTimePicker(fromtimertxt.text.toString())
        fromtimer.setSafeOnClickListener {
            timervar = getString(R.string.fromtimer)
            fromtimer.setBackgroundColor(ContextCompat.getColor(this, R.color.grey_color))
            totimer.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white))
            setTimeIntoTimePicker(fromtimertxt.text.toString())

        }

        totimer.setSafeOnClickListener {
            timervar = getString(R.string.totimer)
            totimer.setBackgroundColor(ContextCompat.getColor(this, R.color.grey_color))
            fromtimer.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white))
            setTimeIntoTimePicker(fromtotxt.text.toString())
        }
        backarrow.setSafeOnClickListener { finish() }
        timePicker.setIs24HourView(false)
        timePicker.setOnTimeChangedListener { timepicker, arg1, arg2 ->
            if (Build.VERSION.SDK_INT >= 23 ){
                hour = timepicker.hour
                min = timepicker.minute
            }
            else{
                hour = timepicker.currentHour
                min = timepicker.currentMinute
            }
            val format: String;
            when {
                hour == 0 -> {
                    hour += 12
                    format = "AM"
                }
                hour == 12 -> {
                    format = "PM"
                }
                hour > 12 -> {
                    hour -= 12
                    format = "PM"
                }
                else -> {
                    format = "AM"
                }
            }
            val time: String = java.lang.String.format("%02d:%02d %s", hour, min, format)
            if (timervar == getString(R.string.fromtimer)) {
                setFromTimer(time)
            } else {
                setToTimer(time)
            }
            fromtimertxt.text = getFromTimer()
            fromtotxt.text = getToTimer()
        }

    }

    override fun onResume() {
        super.onResume()
        Tag8Application.getInstance().setActivity(this)
    }
    fun setTimeIntoTimePicker(timetext : String){
        var timerArray = timetext.split(" ")
        if(timerArray.size > 1){
            var timesecarray = timerArray[0].split(":")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                timePicker.hour = timesecarray[0].toInt()
                if(timerArray[1] == "PM"){
                    timePicker.hour = timesecarray[0].toInt() + 12
                }
                timePicker.minute = timesecarray[1].toInt()
            }

        }
    }
}