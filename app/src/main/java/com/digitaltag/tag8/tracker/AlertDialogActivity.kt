package com.digitaltag.tag8.tracker

import android.app.Activity
import android.content.DialogInterface
import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AlertDialog


class AlertDialogActivity: Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        displayAlert()
    }
    private fun displayAlert() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setMessage("Are you sure you want to exit?").setCancelable(
                false).setPositiveButton("Yes"
        ) { dialog, id ->
            dialog.cancel()
            finish()
        }
                .setNegativeButton("No"
        ) { dialog, id ->
                    dialog.cancel()
                    finish()
                }
        val alert: AlertDialog = builder.create()
        alert.show()
    }
}