package com.digitaltag.tag8.tracker.ui.endless

import android.app.*
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.Color
import android.os.Build
import android.os.IBinder
import android.os.PowerManager
import android.os.SystemClock
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.digitaltag.tag8.tracker.NavigationActivity
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.ble.BluetoothLeService
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.model.BeaconDevice
import com.digitaltag.tag8.tracker.ui.beacon.BeaconsAdapter
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class EndlessService : Service(), BluetoothLeService.Callback {
    var mBluetoothLeService: BluetoothLeService? = null
    private var mServiceConnection: ServiceConnection? = null
    private val  mApplication : Tag8Application = Tag8Application.getInstance()

    private fun bindService() {
        mServiceConnection = object : ServiceConnection {
            override fun onServiceDisconnected(name: ComponentName?) {
                mBluetoothLeService = null
            }
            override fun onServiceConnected(name: ComponentName?, iBinder: IBinder?) {
                if (iBinder is BluetoothLeService.LocalBinder) {
                    mBluetoothLeService = iBinder.service
                    mBluetoothLeService?.addCallback(this@EndlessService)
                    var deviceList = SharedPreferenceManager.getDevices()
                    if(deviceList != null && deviceList.isNotEmpty()){
                        for(device in deviceList!!.values){
                            var tag8ble = mApplication.deviceHashMap[device.macAddress]
                            if(tag8ble == null) {
                                log("onServiceConnected: Size ${deviceList?.size}")
                                mBluetoothLeService!!.createAndAddBleIntoList(device)
                            }
                        }
                    }

                }
            }
        }
        var intent = Intent()
        intent.component = ComponentName("com.digitaltag.tag8.tracker", "com.digitaltag.tag8.tracker.ble.BluetoothLeService")

        bindService(intent, mServiceConnection!!, Context.BIND_AUTO_CREATE)
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        startForeGround()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
       log( "onStartCommand: service started")
        bindService()
        return START_STICKY
    }
    fun startForeGround(){
        var channelId = ""
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelId = createNotifcationChannel("tag8", "service tag8")
        }
        var notiCompact = NotificationCompat.Builder(this, channelId)
        var notification = notiCompact.setOngoing(true).setSmallIcon(R.drawable.appicon)
                .setPriority(NotificationCompat.PRIORITY_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build()
        startForeground(102, notification)

    }
    @RequiresApi(Build.VERSION_CODES.O)
    fun createNotifcationChannel(channelId: String, channelName: String): String{
        var channel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_NONE)
        channel.lightColor = Color.RED
        channel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        var service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(channel)
        return channelId
    }

    override fun onDestroy() {
        super.onDestroy()
        log("Destroy")
        var intent = Intent()
        intent.action = "com.tag8.startservice"
        sendBroadcast(intent)

    }

    override fun onDeviceAdded(beaconDevice: BeaconDevice) {

    }

    override fun onDeviceUpdated(beaconDevice: BeaconDevice) {

    }

    override fun onDeviceRemoved(beaconDevice: BeaconDevice) {

    }


}