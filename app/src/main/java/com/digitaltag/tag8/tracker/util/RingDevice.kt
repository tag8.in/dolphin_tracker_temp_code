package com.digitaltag.tag8.tracker.util

import android.content.Context
import android.media.AudioManager
import android.media.Ringtone
import android.media.RingtoneManager
import android.net.Uri
import android.util.Log
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.getFindmyPhone

object RingDevice {

    private var mRingtone: Ringtone? = null

    private var mAudioManager: AudioManager? = null

    fun getRingtone(mContext: Context): Ringtone? {
        if (mRingtone == null) {
            mRingtone = RingtoneManager.getRingtone(mContext.applicationContext, if (SharedPreferenceManager.getRingtoneUri()?.isNotEmpty() == true) {
                Uri.parse(SharedPreferenceManager.getRingtoneUri()!!)
            } else {
                RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
            })
        }
        return mRingtone
    }

    fun getAudioManager(mContext: Context): AudioManager? {
        if (mAudioManager == null) {
            mAudioManager = mContext.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        }
        return mAudioManager
    }

}