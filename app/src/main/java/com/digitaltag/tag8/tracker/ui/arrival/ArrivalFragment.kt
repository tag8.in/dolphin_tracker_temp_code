package com.digitaltag.tag8.tracker.ui.arrival

import android.bluetooth.BluetoothDevice
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.speech.tts.TextToSpeech
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.baseui.BluetoothConnection
import com.digitaltag.tag8.tracker.ble.BluetoothLeService
import com.digitaltag.tag8.tracker.ble.Tag8BluetoothGatt
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.managers.CustomLinearLayoutManager
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.model.BeaconDevice
import com.digitaltag.tag8.tracker.ui.beacon.BeaconActivity
import com.digitaltag.tag8.tracker.ui.endless.log
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_navigation.*
import kotlinx.android.synthetic.main.fragment_arival.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.rcv
import kotlinx.android.synthetic.main.fragment_main_beacon.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class ArrivalFragment : Fragment(), BluetoothConnection.Callback, BluetoothLeService.Callback,
    ArrivalListener {

    private var mServiceConnection: ServiceConnection? = null
    var mBluetoothLeService: BluetoothLeService? = null
    private val mApplication: Tag8Application = Tag8Application.getInstance()
    var handler: Handler = Handler(Looper.getMainLooper())
    var runnable: Runnable? = null
    var delay = 5000L
    var isScanStart = false
    var scheduler: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()
    var t1: TextToSpeech? = null
    private var bottomSheetDialog: BottomSheetDialog? = null
    private val handlerDe = Handler(Looper.myLooper()!!)
    private var bluetoothConnection: BluetoothConnection? = null
    private var isActivity = true


    companion object {
        lateinit var arrivalList: ArrivalListener
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_arival, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arrivalList = this
        bottomSheetDialog = BottomSheetDialog(requireActivity())
        bindService()
    }

    override fun onDestroy() {
        super.onDestroy()
        isActivity = false
        mBluetoothLeService?.removeCallback(this)
        if (mServiceConnection != null) {
            activity?.unbindService(mServiceConnection!!)
        }
    }

    override fun onResume() {
        Tag8Application.getInstance().setActivity(requireActivity())
        var beconList = mApplication.deviceHashMap
        log("device size ${beconList.size}")

        var listDevice: MutableList<BeaconDevice> = ArrayList()
        if (beconList?.isNotEmpty()) {
            for (device in beconList.values) {
                listDevice.add(device.beaconDevice!!)

            }
            setAdapter(listDevice)
        } else {
            setAdapter()
        }
        scheduler = Executors.newSingleThreadScheduledExecutor()
        startTask()

        super.onResume()
    }

    override fun onPause() {
        scheduler?.shutdown()
        super.onPause()
    }

    private fun bindService() {
        mServiceConnection = object : ServiceConnection {
            override fun onServiceDisconnected(name: ComponentName?) {
                mBluetoothLeService = null
            }

            override fun onServiceConnected(name: ComponentName?, iBinder: IBinder?) {
                if (iBinder is BluetoothLeService.LocalBinder) {
                    mBluetoothLeService = iBinder.service
                    mBluetoothLeService?.addCallback(this@ArrivalFragment)
                    rcv?.adapter?.notifyDataSetChanged()
                    var deviceList = SharedPreferenceManager.getDevices()
                    var beconList = mApplication.deviceHashMap
                    log("device size ${beconList.size}")
                    if (deviceList != null && deviceList?.isNotEmpty()!!) {
                        var listDevice = ArrayList<BeaconDevice>()
                        for (device in beconList.values) {
                            if (device.beaconDevice!!.bleDeviceName.isNotEmpty()) {
                                listDevice.add(device.beaconDevice!!)
                            } else {
                                SharedPreferenceManager.removeBeacon(device.beaconDevice!!)
                                beconList.remove(device.beaconDevice!!.macAddress)
                            }
                        }
                        (rcv?.adapter as ArrivalBeaconsAdapter).updateAllDevices(listDevice)
                    }

                }
            }
        }
        if (activity != null) {
            activity?.bindService(
                Intent(activity, BluetoothLeService::class.java),
                mServiceConnection!!,
                Context.BIND_AUTO_CREATE
            )
        }
    }


    private fun setAdapter(deviceList: List<BeaconDevice>? = null) {
        val adapter = activity?.let {
            ArrivalBeaconsAdapter(it, object : ArrivalBeaconsAdapter.BeaconAdapterCallback {
                override fun onClick(beaconDevice: BeaconDevice) {
                    val intent = Intent(activity, BeaconActivity::class.java)
                    intent.putExtra(Constants.BEACON_DEVICE, beaconDevice)
                    startActivity(intent)
                }

                override fun connect(beaconDevice: BeaconDevice) {
                    //   startScan(beaconDevice)
                }

            }, this)
        }
        rcv?.layoutManager = context?.let { CustomLinearLayoutManager(it) }
        rcv?.adapter = adapter
        if (deviceList?.isNotEmpty() == true) {
            adapter?.addDevices(deviceList)
        }
        if (adapter != null) {
            adapterNotice(adapter)
        }
    }

    private fun adapterNotice(adapter: ArrivalBeaconsAdapter) {
        if (adapter.itemCount < 1) {

        }
    }


    fun updateView() {
        if (rcv?.adapter!!.itemCount > 0) {
            noFound.visibility = View.GONE
        } else {
            noFound.visibility = View.VISIBLE
        }
    }

    override fun onDeviceAdded(beaconDevice: BeaconDevice) {
        activity?.runOnUiThread {
            if (mServiceConnection == null) {
                bindService()
            } else {

                // mBluetoothLeService?.updateEnableBeaconDevice(beaconDevice!!)
                if (rcv?.adapter is ArrivalBeaconsAdapter) {
                    (rcv?.adapter as ArrivalBeaconsAdapter).addDevice(beaconDevice)
                }
            }
        }
    }

    override fun onDeviceUpdated(beaconDevice: BeaconDevice) {
        Log.d("TAG", "onDeviceUpdated: ")
        activity?.runOnUiThread {
            if (mServiceConnection == null) {
                bindService()
            } else {
                Log.d("TAG", "onDeviceUpdated before device name :${beaconDevice.bleDeviceName} ")
                if (beaconDevice.bleDeviceName.isEmpty()) {
                    var deviceList = SharedPreferenceManager.getDevices()
                    for (device in deviceList!!) {
                        if (beaconDevice.macAddress == device.value.macAddress) {
                            beaconDevice.bleDeviceName = device.value.bleDeviceName
                        }
                    }
                }
                Log.d("TAG", "onDeviceUpdated after device name :${beaconDevice.bleDeviceName} ")
                if (rcv?.adapter is ArrivalBeaconsAdapter) {
                    (rcv?.adapter as ArrivalBeaconsAdapter).updateDevice(beaconDevice)
                }

            }
            //mBluetoothLeService?.phoneAlert(beaconDevice!!.macAddress)
        }
    }

    override fun onDeviceRemoved(beaconDevice: BeaconDevice) {
        if (rcv?.adapter is ArrivalBeaconsAdapter) {
            (rcv?.adapter as ArrivalBeaconsAdapter).removeBeacon(beaconDevice)
        }
    }

    private fun startTask() {
        scheduler.scheduleAtFixedRate({
            if (!scheduler?.isShutdown) {
                val deviceList = SharedPreferenceManager.getDevices()
                if (deviceList != null && deviceList!!.size > 0) {
                    for (device in deviceList!!.values) {
                        val offStatus: Boolean? =
                            SharedPreferenceManager.gettrackerOFF(device!!.macAddress)
                        if (offStatus!!) {
                            //startScan(device)
                        } else {
                            var isDisconnected = !device!!.isConnected()
                            if (!device?.connectionState?.equals(Constants.Beacon.State.BLUETOOTH_OFF)) {
                                if (isDisconnected) {
                                    mBluetoothLeService?.let { it -> it.connectBeacon(device!!) }
                                }
                            }
                        }
                    }
                }

            }
        }, 0, 3, TimeUnit.SECONDS)
    }


    private fun connectBeacon(beaconDevice: BeaconDevice) {
        if (bluetoothConnection == null) {
            bluetoothConnection = BluetoothConnection(requireActivity(), this)
        }
        bluetoothConnection?.startScan(beaconDevice)
        // mainBeaconFragment?.updateFinding(true)
    }

    override fun connected(device: BeaconDevice) {

        if (!Tag8Application.arrivalAlertConnectedList.toString()
                .contains("macAddress=${device.macAddress}")
        ) {
            Log.d("TAG", "startArrivalVoice: started 1")
            startArrivalVoice(device)
            bottomSheetDialog?.dismiss()
            showBottomSheet(device)
        }
    }

    override fun dialog(mac: BeaconDevice) {
        bottomSheetDialog?.dismiss()
        showBottomSheet(mac)
    }

    override fun startScanning(device: BeaconDevice) {
        if (!isActivity) {
            return
        }
        connectBeacon(device)
    }


    private fun showBottomSheet(device: BeaconDevice) {
        if (bottomSheetDialog != null) {
            bottomSheetDialog!!.setContentView(R.layout.ring_alert_dialog)
            val btnRing: AppCompatButton? = bottomSheetDialog!!.findViewById(R.id.btnRing)

            bottomSheetDialog!!.setOnDismissListener {
                handlerDe.removeCallbacksAndMessages(null)
            }
            when (device.connectionState) {
                Constants.Beacon.State.CONNECTED -> {
                    if (device.isBeaconAlerting) {
                        btnRing?.text = resources.getString(R.string.unring)
                    } else {
                        btnRing?.text = resources.getString(R.string.ring)
                    }
                }
                else -> {
                    bottomSheetDialog!!.dismiss()
                    Toast.makeText(
                        context,
                        "Look's like package is far from your device.",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            handlerDe.postDelayed(object : Runnable {
                override fun run() {
                    when (device.connectionState) {
                        Constants.Beacon.State.CONNECTED -> {
                            if (device.isBeaconAlerting) {
                                btnRing?.text = resources.getString(R.string.unring)
                            } else {
                                btnRing?.text = resources.getString(R.string.ring)
                            }
                        }
                        else -> {
                            bottomSheetDialog!!.dismiss()
                            Toast.makeText(
                                context,
                                "Look's like package is far from your device.",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                    handlerDe.postDelayed(this, 1000)
                }
            }, 1000)


            val titleTxt: AppCompatTextView? = bottomSheetDialog!!.findViewById(R.id.titleTxt)
            titleTxt?.text = "${device.bleDeviceName} is Arrived"

            btnRing?.setOnClickListener {
                if (device.connectionState == Constants.Beacon.State.CONNECTED) {
                    mBluetoothLeService?.ringBeacon(device)
                    if (device.isBeaconAlerting) {
                        btnRing?.text = resources.getString(R.string.unring)
                    } else {
                        btnRing?.text = resources.getString(R.string.ring)
                    }
                } else {
                    bottomSheetDialog!!.dismiss()
                    Toast.makeText(
                        context,
                        "Look's like package is far from your device.",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            bottomSheetDialog!!.show()

        }
    }


    private fun startArrivalVoice(beaconDevice: BeaconDevice) {
        Log.d("TAG", "startArrivalVoice: started ")
        val notification =
            Uri.parse("android.resource://" + requireActivity().packageName.toString() + "/" + R.raw.arrivaltone)
        val r = RingtoneManager.getRingtone(
            requireContext(),
            notification
        )
        r.play()
        CoroutineScope(Dispatchers.Main).launch {
            delay(3000)
            Log.d(Tag8BluetoothGatt.TAG, "startArrivalVoice: Starte voicess")
            r.stop()
            try {
                t1 = TextToSpeech(requireContext()) { status ->
                    if (status != TextToSpeech.ERROR) {
                        t1?.language = Locale.US
                        t1?.speak(
                            "Your " + beaconDevice!!.bleDeviceName + " has arrived.",
                            TextToSpeech.QUEUE_ADD, null, null
                        )
                    }
                }
            } catch (e: Exception) {
                e.message
            }
        }
//        Handler(Looper.myLooper()!!).postDelayed({
//            Log.d(Tag8BluetoothGatt.TAG, "startArrivalVoice: Starte voicess")
//            r.stop()
//            t1 = TextToSpeech(requireContext()) { status ->
//                if (status != TextToSpeech.ERROR) {
//                    t1?.language = Locale.US
//                    Handler(Looper.myLooper()!!).postDelayed({
//                        t1?.speak(
//                            "Your " + beaconDevice!!.bleDeviceName + " has arrived.",
//                            TextToSpeech.QUEUE_ADD, null, null
//                        )
//                    }, 1000)
//                }
//            }
//        }, 3000)
    }

    override fun handleScanResult(device: BluetoothDevice, rssi: Int) {
        Log.d("TAG", "handleScanResult: $device")
    }

    override fun onScanFailed(errorCode: Int, specificMacAddress: String?) {
        Log.d("TAG", "handleScanResult: $errorCode")
    }

}