package com.digitaltag.tag8.tracker.util

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.digitaltag.tag8.tracker.ui.endless.log

fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
    itemView.setSafeOnClickListener {
        event.invoke(adapterPosition, itemViewType)
    }
    return this
}
// Extension function to show toast message
fun Activity.toast(message: String) {
    log("log $message")
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}
fun Activity.hideKeyboard(currentFocus: View){
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
    imm!!.hideSoftInputFromWindow(currentFocus.windowToken, 0);
}
fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
    val safeClickListener = SafeClickListener {
        onSafeClick(it)
    }
    setOnClickListener(safeClickListener)
}