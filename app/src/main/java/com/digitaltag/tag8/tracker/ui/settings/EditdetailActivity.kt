package com.digitaltag.tag8.tracker.ui.settings

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.apis.ApiLiveData
import com.digitaltag.tag8.tracker.baseui.BluetoothConnection
import com.digitaltag.tag8.tracker.ble.BluetoothLeService
import com.digitaltag.tag8.tracker.ble.Tag8BluetoothGatt
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.addFarBeacon
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.addFindmyPhone
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.addNearBeacon
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.addPhoneAlertStatus
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.addtrackerOFF
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.addveryFarBeacon
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.getFarBeacon
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.getFindmyPhone
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.getNearBeacon
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.getPhoneAlertStatus
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.getveryFarBeacon
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.removeFarBeacon
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.removeFindmyPhone
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.removeNearBeacon
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.removeveryFarBeacon
import com.digitaltag.tag8.tracker.model.BeaconDevice
import com.digitaltag.tag8.tracker.modules.URLS
import com.digitaltag.tag8.tracker.ui.beacon.EditBeaconActivity
import com.digitaltag.tag8.tracker.util.Utils
import com.digitaltag.tag8.tracker.util.setSafeOnClickListener
import com.digitaltag.tag8.tracker.util.toast
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.slider.LabelFormatter
import com.google.android.material.slider.Slider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_editdetail.*
import okhttp3.*
import okio.IOException
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class EditdetailActivity : AppCompatActivity(), BluetoothLeService.Callback {

    private var beaconDevice: BeaconDevice? = null
    private var bluetoothConnection: BluetoothConnection? = null
    private var mServiceConnection: ServiceConnection? = null
    private var mBluetoothLeService: BluetoothLeService? = null
    private var rangeValue: Float = 0.0f
    private var prangeValue: Float = 0.0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editdetail)
        Tag8Application.getInstance().setActivity(this)
        beaconDevice = intent.getParcelableExtra(Constants.BEACON_DEVICE)
        bindService()
        view2.setSafeOnClickListener {
            val intent = Intent(this, EditBeaconActivity::class.java)
            Utils.updateType = getString(R.string.updatedata)
            intent.putExtra(Constants.BEACON_DEVICE, beaconDevice)
            startActivity(intent)
        }

        backarrow.setSafeOnClickListener {
            finish()
        }
        if (beaconDevice?.isConnected() != true) {
            trackeroffimg.setColorFilter(resources.getColor(R.color.red))
        }

        trackeroff.setSafeOnClickListener {
            if (beaconDevice?.isConnected() == true) {
                MaterialAlertDialogBuilder(this)
                    .setTitle(resources.getString(R.string.turnofftracker))
                    .setMessage("If you turn off '" + beaconDevice!!.bleDeviceName + "' you will need to wake it up to use it again. Are you sure to turn it off?")
                    .setNegativeButton(resources.getString(R.string.cancel)) { dialog, which ->
                        dialog.dismiss()
                    }
                    .setPositiveButton(resources.getString(R.string.ok)) { dialog, which ->
                        beaconDevice?.connectionState = Constants.Beacon.State.OFF;
                        disconnect(beaconDevice)
                        addtrackerOFF(beaconDevice!!.macAddress)
                        trackeroffimg.setColorFilter(resources.getColor(R.color.red))
                    }.show()
            } else {
                MaterialAlertDialogBuilder(this)
                    .setTitle(resources.getString(R.string.turnofftracker))
                    .setMessage("Hold the button on '" + beaconDevice!!.bleDeviceName + "' to wake it up.")
                    .setNegativeButton(resources.getString(R.string.cancel)) { dialog, which ->
                        dialog.dismiss()
                    }
                    .setPositiveButton(resources.getString(R.string.ok)) { dialog, which ->
                        connectBeacon(beaconDevice)
                    }.show()
            }
        }

        arrivalSwitch.isChecked = SharedPreferenceManager.arrivalAlert(beaconDevice?.macAddress!!)
        arrivalSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            SharedPreferenceManager.arrivalAlert(
                isChecked,
                beaconDevice?.macAddress!!
            )
        }

        prangeValue = 10.0f
        if (getPhoneAlertStatus(beaconDevice?.macAddress!!) == "connected") {
            if (beaconDevice!!.enablePhoneRing) {
                prangeValue = 0.0f
                phonealertcheck.isChecked = true
            }
        } else if (getPhoneAlertStatus(beaconDevice?.macAddress!!) == "disconnected") {
            if (beaconDevice!!.enablePhoneRing) {
                prangeValue = 5.0f
                phonealertcheck.isChecked = true
            }
        } else if (getPhoneAlertStatus(beaconDevice?.macAddress!!) == "both") {
            if (beaconDevice!!.enablePhoneRing) {
                prangeValue = 10.0f
                phonealertcheck.isChecked = true
            }
        }
        phonerangeSlider.value = prangeValue
        phonerangeSlider.addOnChangeListener { slider, value, fromUser ->
            if (beaconDevice!!.enablePhoneRing) {
                prangeValue = value
                when (prangeValue) {
                    10.0f -> {
                        addPhoneAlertStatus(beaconDevice?.macAddress!!, "both")
                    }
                    5.0f -> {
                        addPhoneAlertStatus(beaconDevice?.macAddress!!, "disconnected")
                    }
                    else -> {
                        addPhoneAlertStatus(beaconDevice?.macAddress!!, "connected")
                    }
                }
            }

        }
        phonealertcheck.setOnCheckedChangeListener { buttonView, isChecked ->
            phonerangeSlider.isEnabled = isChecked
        }
        phonerangeSlider.isEnabled = phonealertcheck.isChecked


        rangeValue = 10.0f
        if (getFarBeacon(beaconDevice!!.macAddress)!!.isNotEmpty()) {
            if (beaconDevice!!.enableBeaconRing) {
                rangeValue = 5.0f
                beaconalertcheck.isChecked = true
            }

        }
        if (getveryFarBeacon(beaconDevice!!.macAddress)!!.isNotEmpty()) {
            if (beaconDevice!!.enableBeaconRing) {
                rangeValue = 10.0f
                beaconalertcheck.isChecked = true
            }

        }
        if (getNearBeacon(beaconDevice!!.macAddress)!!.isNotEmpty()) {
            if (beaconDevice!!.enableBeaconRing) {
                rangeValue = 0.0f
                beaconalertcheck.isChecked = true
            }

        }
        rangeSlider.value = rangeValue
        rangeSlider.addOnChangeListener { slider, value, fromUser ->
            if (beaconDevice!!.enableBeaconRing) {
                rangeValue = value
                when (rangeValue) {
                    10.0f -> {
                        addveryFarBeacon(beaconDevice?.macAddress!!)
                        removeFarBeacon(beaconDevice?.macAddress!!)
                        removeNearBeacon(beaconDevice?.macAddress!!)
                    }
                    5.0f -> {
                        addFarBeacon(beaconDevice?.macAddress!!)
                        removeveryFarBeacon(beaconDevice?.macAddress!!)
                        removeNearBeacon(beaconDevice?.macAddress!!)
                    }
                    else -> {
                        removeFarBeacon(beaconDevice?.macAddress!!)
                        removeveryFarBeacon(beaconDevice?.macAddress!!)
                        addNearBeacon(beaconDevice?.macAddress!!)
                    }
                }
            }

        }
        beaconalertcheck.setOnCheckedChangeListener { buttonView, isChecked ->
            rangeSlider.isEnabled = isChecked
        }
        rangeSlider.isEnabled = beaconalertcheck.isChecked


        findmyphone.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                addFindmyPhone(beaconDevice?.macAddress)
            } else {
                removeFindmyPhone(beaconDevice?.macAddress)
            }

        }



        findmyphone?.isChecked = getFindmyPhone(beaconDevice?.macAddress)!!

        findmyphone?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                toast(resources.getString(R.string.findmyphone_message))
                addFindmyPhone(beaconDevice?.macAddress)
            } else {
                removeFindmyPhone(beaconDevice?.macAddress)
            }

        }

        btnDisconnectDelete.setSafeOnClickListener {
            MaterialAlertDialogBuilder(this)
                .setTitle(resources.getString(R.string.deletetracker))
                .setMessage("Once you delete this '" + beaconDevice!!.bleDeviceName + "' item, it will be removed from your Tracker list, and you cannot use it unless you add it again.")
                .setNegativeButton(resources.getString(R.string.cancel)) { dialog, which ->
                    dialog.dismiss()
                }
                .setPositiveButton(resources.getString(R.string.ok)) { dialog, which ->
                    if (beaconDevice != null) {
                        ApiLiveData.deactivateQr(beaconDevice!!.macAddress)
                        ApiLiveData.deleteBeacon(beaconDevice!!.macAddress)
                        deleteBeacon(beaconDevice)
                        removeTagName()
                    } else {
                        Toast.makeText(this@EditdetailActivity, "Error", Toast.LENGTH_SHORT).show()
                    }
                }
                .show()
        }
        if (!beaconDevice!!.enableBeaconRing) {
            rangeSlider.isEnabled = false
            beaconalertcheck.isEnabled = false
        } else {
            beaconalertcheck.isEnabled = true
        }
        if (!beaconDevice!!.enablePhoneRing) {
            phonerangeSlider.isEnabled = false
            phonealertcheck.isEnabled = false
        } else {
            phonealertcheck.isEnabled = true
        }

        val getData = SharedPreferenceManager.getAlertData(beaconDevice?.macAddress)
        val sliders: Slider = findViewById(R.id.sliders)

        val enableType: AppCompatTextView = findViewById(R.id.enableType)

        sliders.setLabelFormatter(object : LabelFormatter {
            override fun getFormattedValue(value: Float): String {
                return when (value) {
                    0.0F -> {
                        "Very Near"
                    }
                    50.0F -> {
                        "Near"
                    }
                    else -> {
                        "Far"
                    }
                }
                return String.format(Locale.US, "%.0F", value)
            }
        })

        when (getData) {
            "Very Near" -> {
                sliders.value = 0.0F
                enableType.text = "Alert enable if beacon is Near, Far or disconnected"
            }
            "Near" -> {
                sliders.value = 50.0F
                enableType.text = "Alert enable if beacon is Far or disconnected"
            }
            else -> {
                sliders.value = 100.0F
                enableType.text = "Alert enable if beacon is disconnected"
            }
        }
        sliders.addOnChangeListener { _, value, _ ->
            when (value) {
                0.0F -> {
                    SharedPreferenceManager.addAlertData(
                        beaconDevice!!.macAddress,
                        "Very Near"
                    )
                    enableType.text = "Alert enable if beacon is Near, Far or disconnected"
                }
                50.0F -> {
                    SharedPreferenceManager.addAlertData(beaconDevice!!.macAddress, "Near")
                    enableType.text = "Alert enable if beacon is Far or disconnected"
                }
                else -> {
                    SharedPreferenceManager.addAlertData(beaconDevice!!.macAddress, "Far")
                    enableType.text = "Alert enable if beacon is disconnected"
                }
            }
        }

        val checkboxAlerts: AppCompatCheckBox = findViewById(R.id.checkboxAlerts)
        val alertCheck = SharedPreferenceManager.getEnableAlertBox()
        checkboxAlerts.isChecked = alertCheck
        if (alertCheck) {
            sliders.visibility = View.VISIBLE
            enableType.visibility = View.VISIBLE
            materialCardView2.visibility = View.VISIBLE
            nearCard.visibility = View.VISIBLE
            farCard.visibility = View.VISIBLE
        } else {
            sliders.visibility = View.GONE
            enableType.visibility = View.GONE
            materialCardView2.visibility = View.GONE
            nearCard.visibility = View.GONE
            farCard.visibility = View.GONE
        }

        checkboxAlerts.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                sliders.visibility = View.VISIBLE
                enableType.visibility = View.VISIBLE
                materialCardView2.visibility = View.VISIBLE
                nearCard.visibility = View.VISIBLE
                farCard.visibility = View.VISIBLE
            } else {
                sliders.visibility = View.GONE
                enableType.visibility = View.GONE
                materialCardView2.visibility = View.GONE
                nearCard.visibility = View.GONE
                farCard.visibility = View.GONE
            }
        }

        checkboxAlerts.setOnClickListener {
            if (checkboxAlerts.isChecked) {
                SharedPreferenceManager.addAlertData(beaconDevice!!.macAddress, "Far")
                SharedPreferenceManager.setEnableAlertBox(true)
            } else {
                SharedPreferenceManager.setEnableAlertBox(false)
                SharedPreferenceManager.addAlertData(beaconDevice!!.macAddress, "No Alert")
            }
        }
        // temp
        /* rangeSlider.isEnabled = false
         beaconalertcheck.isEnabled = false*/

    }

    fun delete() {
    }


    private fun bindService() {
        mServiceConnection = object : ServiceConnection {
            override fun onServiceDisconnected(name: ComponentName?) {
                mBluetoothLeService = null
            }

            override fun onServiceConnected(name: ComponentName?, iBinder: IBinder?) {
                if (iBinder is BluetoothLeService.LocalBinder) {
                    mBluetoothLeService = iBinder.service
                    mBluetoothLeService?.addCallback(this@EditdetailActivity)
                }
            }
        }
        bindService(
            Intent(this, BluetoothLeService::class.java),
            mServiceConnection!!,
            Context.BIND_AUTO_CREATE
        )
    }

    override fun onResume() {
        super.onResume()
        Tag8Application.getInstance().setActivity(this)
    }

    fun disconnect(beaconDevice: BeaconDevice?) {
        if (beaconDevice != null) {
            mBluetoothLeService?.disconnect(beaconDevice)
        }
    }

    private fun connectBeacon(beaconDevice: BeaconDevice?) {
        bluetoothConnection?.startScan(beaconDevice)
    }


    override fun onStart() {
        super.onStart()
        updateDevice(beaconDevice!!)

    }

    private fun deleteBeacon(beaconDevice: BeaconDevice?) {
        if (beaconDevice != null) {
            mBluetoothLeService?.deleteBeacon(beaconDevice)
            bluetoothConnection?.stopScan()
            mBluetoothLeService?.deleteBeacon(beaconDevice)

        } else {
            toast("Error")
        }
    }

    private fun updateDevice(beaconDevice: BeaconDevice) {
        runOnUiThread {
            if (beaconDevice?.isConnected()) {
                turnofftrackertxt?.text = getString(R.string.turnofftracker)
            } else {
                turnofftrackertxt?.text = getString(R.string.turnontracker)
            }

            val type = beaconDevice?.type

            when (type) {
                Constants.Beacon.Type.Key, "" -> {
                    categoryimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.key))
                }
                Constants.Beacon.Type.Wallet -> {
                    categoryimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.wallet))
                }
                Constants.Beacon.Type.Passport -> {
                    categoryimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            this,
                            R.drawable.passport_vextor
                        )
                    )
                }
                Constants.Beacon.Type.Bag -> {
                    categoryimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_bag))
                }
                Constants.Beacon.Type.Pet -> {
                    categoryimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.pett))
                }
                Constants.Beacon.Type.Laptop -> {
                    categoryimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.laptop))
                }
                Constants.Beacon.Type.CardHolder -> {
                    categoryimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            this,
                            R.drawable.ic_card_holder
                        )
                    )
                }
                Constants.Beacon.Type.Camera -> {
                    categoryimg.setImageDrawable(
                        ContextCompat.getDrawable(
                            this,
                            R.drawable.ic_camer
                        )
                    )
                }
                Constants.Beacon.Type.Remote -> {
                    categoryimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.remot))
                }
                Constants.Beacon.Type.Toy -> {
                    categoryimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_toy))
                }
                Constants.Beacon.Type.Car -> {
                    categoryimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.car))
                }
                Constants.Beacon.Type.Purse -> {
                    categoryimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.purse))
                }
                Constants.Beacon.Type.Other -> {
                    categoryimg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.other))
                }
                else -> {
                    Glide.with(applicationContext).load(beaconDevice?.type).into(categoryimg)

                }
            }
        }

    }

    override fun onDeviceAdded(beaconDevice: BeaconDevice) {

    }

    override fun onDeviceUpdated(beaconDevice: BeaconDevice) {
        if (this.beaconDevice!!.macAddress == beaconDevice.macAddress) {
            this.beaconDevice = beaconDevice
            updateDevice(beaconDevice)
        }

    }

    override fun onDeviceRemoved(beaconDevice: BeaconDevice) {
        toast("Delete success!!")
        finish()

    }

    fun removeTagName() {
        var tagHashMap = SharedPreferenceManager.getTagNameKey()
        if (tagHashMap != null) {
            try {
                var keyMap = tagHashMap[Constants.Beacon.Type.Key]
                if (keyMap!!.containsKey(beaconDevice!!.macAddress)) {
                    keyMap!!.remove(beaconDevice!!.macAddress)
                }

                var walletMap = tagHashMap[Constants.Beacon.Type.Wallet]
                if (walletMap!!.containsKey(beaconDevice!!.macAddress)) {
                    walletMap!!.remove(beaconDevice!!.macAddress)
                }
                var passportMap = tagHashMap[Constants.Beacon.Type.Passport]
                if (passportMap!!.containsKey(beaconDevice!!.macAddress)) {
                    passportMap!!.remove(beaconDevice!!.macAddress)
                }
                var bagMap = tagHashMap[Constants.Beacon.Type.Bag]
                if (bagMap!!.containsKey(beaconDevice!!.macAddress)) {
                    bagMap!!.remove(beaconDevice!!.macAddress)
                }

                var petMap = tagHashMap[Constants.Beacon.Type.Pet]
                if (petMap!!.containsKey(beaconDevice!!.macAddress)) {
                    petMap!!.remove(beaconDevice!!.macAddress)
                }

                var laptopMap = tagHashMap[Constants.Beacon.Type.Laptop]
                if (laptopMap!!.containsKey(beaconDevice!!.macAddress)) {
                    laptopMap!!.remove(beaconDevice!!.macAddress)
                }

                var purseMap = tagHashMap[Constants.Beacon.Type.Purse]
                if (purseMap!!.containsKey(beaconDevice!!.macAddress)) {
                    purseMap!!.remove(beaconDevice!!.macAddress)
                }

                var cardHolderMap = tagHashMap[Constants.Beacon.Type.CardHolder]
                if (cardHolderMap!!.containsKey(beaconDevice!!.macAddress)) {
                    cardHolderMap!!.remove(beaconDevice!!.macAddress)
                }

                var toyMap = tagHashMap[Constants.Beacon.Type.Toy]
                if (toyMap!!.containsKey(beaconDevice!!.macAddress)) {
                    toyMap!!.remove(beaconDevice!!.macAddress)
                }

                var cameraMap = tagHashMap[Constants.Beacon.Type.Camera]
                if (cameraMap!!.containsKey(beaconDevice!!.macAddress)) {
                    cameraMap!!.remove(beaconDevice!!.macAddress)
                }
                var carMap = tagHashMap[Constants.Beacon.Type.Car]
                if (carMap!!.containsKey(beaconDevice!!.macAddress)) {
                    carMap!!.remove(beaconDevice!!.macAddress)
                }


                var remoteMap = tagHashMap[Constants.Beacon.Type.Remote]
                if (remoteMap!!.containsKey(beaconDevice!!.macAddress)) {
                    remoteMap!!.remove(beaconDevice!!.macAddress)
                }

                var otherMap = tagHashMap[Constants.Beacon.Type.Other]
                if (otherMap!!.containsKey(beaconDevice!!.macAddress)) {
                    otherMap!!.remove(beaconDevice!!.macAddress)
                }
                SharedPreferenceManager.addTagNameKey(tagHashMap)

            } catch (e: Exception) {

            }
        }
    }
}