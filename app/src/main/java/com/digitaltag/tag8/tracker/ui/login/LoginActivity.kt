package com.digitaltag.tag8.tracker.ui.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.digitaltag.tag8.tracker.NavigationActivity
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.baseui.BaseActivity
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.firebase.FirebaseAuthManager
import com.digitaltag.tag8.tracker.util.setSafeOnClickListener
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {

    private var firebaseUser: FirebaseUser? = null
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        auth = Firebase.auth
        val uid = auth.currentUser?.uid
        if (uid != null) {
            if (uid.isNotEmpty()) {
                startActivity(Intent(this, NavigationActivity::class.java))
                finish()
            }
        }
        firebaseUser = FirebaseAuthManager.getCurrentUser()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        val googleSignInClient = GoogleSignIn.getClient(this, gso)
        cvGoogle?.setSafeOnClickListener {

            startActivity(Intent(this, NavigationActivity::class.java))
            finish()
            /*
            if (firebaseUser == null) {
                val signInIntent = googleSignInClient?.signInIntent
                if (signInIntent == null) {
                    Toast.makeText(this, "Unable to login", Toast.LENGTH_SHORT).show()
                } else {
                    startActivityForResult(signInIntent, Constants.RC_SIGN_IN)
                }
            } else {
                finish()
                startActivity(Intent(this, NavigationActivity::class.java))
            }*/
        }
    }

    override fun onResume() {
        super.onResume()
        Tag8Application.getInstance().setActivity(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.RC_SIGN_IN) {
            GoogleSignIn.getSignedInAccountFromIntent(data)
        }
    }

    override fun onGoogleLoginSuccess(user: FirebaseUser?) {
        firebaseUser = user
        if (firebaseUser == null) {
            Toast.makeText(this, "Unable to login", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(
                this,
                "Welcome to tag8 Tracker " + firebaseUser?.displayName,
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onGoogleLoginFail(message: String) {
        Toast.makeText(this, "Unable to login", Toast.LENGTH_SHORT).show()
    }
}