package com.digitaltag.tag8.tracker.util

import android.util.Log
import com.digitaltag.tag8.tracker.BuildConfig

/**
 * Created on 15/06/20.
 */
object DebugLogger {

    private const val LOG_PREFIX = "tag8_"
    private const val LOG_PREFIX_LENGTH = LOG_PREFIX.length
    private const val MAX_LOG_TAG_LENGTH = 30

    fun d(tag: String, log: String) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, log)
        }
    }

    fun e(tag: String, log: String) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, log)
        }
    }

    fun makeLogTag(cls: Class<*>): String {
        return makeLogTag(cls.simpleName)
    }

    fun makeLogTag(str: String): String {
        return if (str.length > MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH) {
            LOG_PREFIX + str.substring(0, MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH - 1)
        } else LOG_PREFIX + str

    }

}
