package com.digitaltag.tag8.tracker.ui.ringtone

class NotifyListener {

    companion object {
        lateinit var notify: Notify
    }
    interface Notify {
        fun notify(value: String)
    }

    fun getNotifyListener(notice: Notify) {
        notify = notice
    }
}