package com.digitaltag.tag8.tracker.ui.ringtone

import android.app.Service
import android.content.Intent
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.RingtoneManager
import android.os.IBinder
import androidx.core.net.toUri
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.util.RingDevice

class RingtoneService : Service() {
    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        var ringtone = RingDevice.getRingtone(this)
        val audioManager = RingDevice.getAudioManager(this)
        if (audioManager != null && ringtone != null) {
            val volume = audioManager.getStreamMaxVolume(AudioManager.STREAM_RING)
            audioManager.setStreamVolume(
                AudioManager.STREAM_ALARM,
                volume,
                AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE
            )
            if (SharedPreferenceManager.getRingtoneUris()?.isNotEmpty() == true) {
                ringtone = RingtoneManager.getRingtone(
                    this, SharedPreferenceManager.getRingtoneUris()!!
                        .toUri()
                )
            }
            if (ringtone != null) {
                ringtone.audioAttributes = AudioAttributes.Builder()
                    .setFlags(AudioAttributes.FLAG_AUDIBILITY_ENFORCED)
                    .setLegacyStreamType(AudioManager.STREAM_ALARM)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build()
            }
            ringtone?.play()
        }
        return START_STICKY
    }
}