package com.digitaltag.tag8.tracker.util

class BluetoothStateListeners {

    companion object {
        lateinit var bluetoothChange: BluetoothChange
    }

    interface BluetoothChange {
        fun state(state: Int)
    }

    fun getBLEState(ble: BluetoothChange) {
        bluetoothChange = ble
    }
}