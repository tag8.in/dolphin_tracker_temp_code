package com.digitaltag.tag8.tracker.ui.map

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.digitaltag.tag8.tracker.NavigationActivity
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.ui.beacon.EditBeaconActivity
import com.digitaltag.tag8.tracker.util.PermissionUtils
import com.digitaltag.tag8.tracker.util.Utils
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapFragment : Fragment(), OnMapReadyCallback {

    private var fusedLocationClient: FusedLocationProviderClient? = null
    var googleMap: GoogleMap? = null
    var mActivity: NavigationActivity? = null
    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 999
    }
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mActivity = requireActivity() as NavigationActivity
        Tag8Application.getInstance().setActivity(mActivity!!)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map)
        if (mapFragment is SupportMapFragment) {
            mapFragment.getMapAsync(this)
        }

    }

    override fun onResume() {
        super.onResume()
        Tag8Application.getInstance().setActivity(mActivity!!)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap
        val devices = SharedPreferenceManager.getDevices()
        if (devices != null) {
            for (beaconDevice in devices.values) {
                googleMap?.apply {
                    val location = LatLng((beaconDevice!!.latitude
                            ?: "0.0").toDouble(), (beaconDevice.longitude
                            ?: "0.0").toDouble())
                    val title = if (beaconDevice.isConnected()) {
                        beaconDevice.bleDeviceName
                    } else {
                        "Last ${beaconDevice.bleDeviceName} Location."
                    }
                    addMarker(MarkerOptions()
                            .position(location)
                            .title(title))
                    moveCamera(CameraUpdateFactory.newLatLngZoom(location, 18f))
                }
            }
        } else {
            setCurrentLocation(googleMap)
        }
    }

    private fun setCurrentLocation(googleMap: GoogleMap?) {
        if (activity != null && isAdded) {
            when {
                PermissionUtils.isAccessFineLocationGranted(requireContext()) -> {
                    when {
                        PermissionUtils.isLocationEnabled(requireContext()) -> {
                            setUpLocationListener(googleMap)
                        }
                        else -> {
                            PermissionUtils.showGPSNotEnabledDialog(requireContext())
                        }
                    }
                }
                else -> {
                    PermissionUtils.requestAccessFineLocationPermission(
                            requireActivity(),
                            LOCATION_PERMISSION_REQUEST_CODE
                    )
                }
            }
        }
    }
    @SuppressLint("MissingPermission")
    private fun setUpLocationListener(googleMap: GoogleMap?) {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        // for getting the current location update after every 2 seconds with high accuracy
        val locationRequest = LocationRequest().setInterval(10000).setFastestInterval(10000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        fusedLocationClient!!.requestLocationUpdates(
                locationRequest,
                object : LocationCallback() {
                    override fun onLocationResult(locationResult: LocationResult) {
                        super.onLocationResult(locationResult)
                        for (location in locationResult.locations) {
                            val latLan = LatLng(location?.latitude ?: 0.0, location?.longitude ?: 0.0)
                            googleMap?.apply {
                                addMarker(MarkerOptions()
                                        .position(latLan)
                                        .title("Your location"))
                                moveCamera(CameraUpdateFactory.newLatLngZoom(latLan, 18f))
                            }
                        }
                        // Few more things we can do here:
                        // For example: Update the location of user on server
                    }
                },
                Looper.myLooper()
        )
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    when {
                        PermissionUtils.isLocationEnabled(requireContext()) -> {
                            setUpLocationListener(googleMap)
                        }
                        else -> {
                            PermissionUtils.showGPSNotEnabledDialog(requireContext())
                        }
                    }
                } else {
                    Toast.makeText(
                            context,
                            "Permission not granted",
                            Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

}