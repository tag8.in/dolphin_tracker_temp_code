package com.digitaltag.tag8.tracker.ui.signup

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.digitaltag.tag8.tracker.NavigationActivity
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.databinding.ActivityRegisterBinding
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.util.toast

class RegisterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val type = intent.getStringExtra("isSettings")

        if (type == "true") {
            binding.skipBtn.text = "Back"
        }

        binding.register.setOnClickListener {
            if (binding.firstName.text.toString().isEmpty()) {
                toast(resources.getString(R.string.enter_first_name))
                return@setOnClickListener
            }
            if (binding.lastName.text.toString().isEmpty()) {
                toast(resources.getString(R.string.enter_last_name))
                return@setOnClickListener
            }
            if (binding.phoneNumber.text.toString().isEmpty()) {
                toast(resources.getString(R.string.enter_phone_number))
                return@setOnClickListener
            }

            SharedPreferenceManager.firstName(binding.firstName.text.toString())
            SharedPreferenceManager.lastName(binding.lastName.text.toString())
            SharedPreferenceManager.phoneNumber(binding.phoneNumber.text.toString())
            SharedPreferenceManager.phoneNumberCountryCode("+" + binding.ccp.selectedCountryCode)
            startActivity(Intent(this, NavigationActivity::class.java))
            finish()
        }

        binding.firstName.setText(SharedPreferenceManager.firstName().toString())
        binding.lastName.setText(SharedPreferenceManager.lastName().toString())
        binding.phoneNumber.setText(SharedPreferenceManager.phoneNumber().toString())
        binding.ccp.setDefaultCountryUsingNameCode(
            SharedPreferenceManager.phoneNumberCountryCode().toString()
        )


        binding.skipBtn.setOnClickListener {
            if (type == "true") {
                finish()
                return@setOnClickListener
            }
            startActivity(Intent(this, NavigationActivity::class.java))
            finish()
        }
    }
}