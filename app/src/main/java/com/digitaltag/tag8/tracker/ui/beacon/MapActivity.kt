package com.digitaltag.tag8.tracker.ui.beacon

import android.os.Bundle
import android.widget.Toast
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.baseui.BaseActivity
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.model.BeaconDevice
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

@Suppress("UNREACHABLE_CODE")
class MapActivity : BaseActivity(), OnMapReadyCallback {

    private var beaconDevice: BeaconDevice? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        Tag8Application.getInstance().setActivity(this)
        beaconDevice = intent?.getParcelableExtra(Constants.BEACON_DEVICE)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map)
        if (mapFragment is SupportMapFragment) {
            mapFragment.getMapAsync(this)
        }
    }

    override fun onResume() {
        super.onResume()
        Tag8Application.getInstance().setActivity(this)
    }
    override fun onMapReady(googleMap: GoogleMap?) {
        googleMap?.apply {
            val location = LatLng((beaconDevice?.latitude
                    ?: "0.0").toDouble(), (beaconDevice?.longitude ?: "0.0").toDouble())
            addMarker(MarkerOptions()
                    .position(location)
                    .title("Last ${beaconDevice?.bleDeviceName} location."))
                    .showInfoWindow()
            moveCamera(CameraUpdateFactory.newLatLngZoom(location, 18f));
        }
    }
}