package com.digitaltag.tag8.tracker.util

class unRingListener {
    companion object{
        lateinit var ring : UnRing
    }

    interface UnRing{
        fun stop()
    }

    fun ringCallback(listener: UnRing){
        ring = listener
    }
}