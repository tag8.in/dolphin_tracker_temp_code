package com.digitaltag.tag8.tracker.ble

import android.app.Notification
import android.app.NotificationManager
import android.app.Service
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.apis.ApiLiveData


class BLEBackgroundScan : Service(), BluetoothAdapter.LeScanCallback {

    private val TAG: String = BLEBackgroundScan::class.java.simpleName

    private var btGatt: BluetoothGatt? = null
    private var mBluetoothAdapter: BluetoothAdapter? = null


    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "onCreate: Ble Scanning")
        getBTService()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onDestroy() {
        stopBLEscan()
        if (btGatt != null) {
            btGatt!!.disconnect()
            btGatt!!.close()
            btGatt = null
        }
    }


    override fun onLeScan(device: BluetoothDevice?, rssi: Int, scanRecord: ByteArray?) {
        try {
            if (device?.name != null) {
                Log.d(TAG, "onLeScan: Device in range ${device.name} $rssi")
                if (device.name.contains("iTrack") || device.name.contains("iLck3")) {
                    Log.d(TAG, "onLeScan: Device in range 1 ${device.name} $rssi")
                }
                stopBLEscan()
            }
        } catch (e: Exception) {
            e.message
        }
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent!!.action
            when {
                BluetoothAdapter.ACTION_DISCOVERY_STARTED == action -> {
                }
                BluetoothAdapter.ACTION_DISCOVERY_FINISHED == action -> {
                    mBluetoothAdapter?.startDiscovery()
                }
                BluetoothDevice.ACTION_FOUND == action -> {
                    val device =
                        intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE) as BluetoothDevice?
                    if (device != null) {
                        try {
                            if (device.name.contains("iTrack")) {

//                                val builder =
//                                    NotificationCompat.Builder(
//                                        this@BLEBackgroundScan,
//                                        this@BLEBackgroundScan.getString(R.string.channel_name)
//                                    )
//                                builder.setContentTitle(this@BLEBackgroundScan.getString(R.string.app_name))
//                                builder.setContentText("'" + device!!.name + " ${device.address}' is found.")
//                                builder.setSmallIcon(R.mipmap.appicon)
//                                builder.setAutoCancel(true)
//                                val notificationManager =
//                                    this@BLEBackgroundScan.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//                                builder.build().flags =
//                                    builder.build().flags or Notification.FLAG_AUTO_CANCEL
//                                notificationManager.notify(11222, builder.build())

                                ApiLiveData.communityTrace(device.address)
                            }
                        } catch (e: Exception) {
                            e.message
                        }
                    }
                }
            }
        }

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        if (isBluetoothSupported()) {
//            if (mBluetoothAdapter != null) {
//                if (mBluetoothAdapter!!.isEnabled) {
//                    val filter = IntentFilter()
//
//                    filter.addAction(BluetoothDevice.ACTION_FOUND)
//                    filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED)
//                    filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
//
//                    registerReceiver(mReceiver, filter)
//                    mBluetoothAdapter!!.startDiscovery()
//                }
//                startBLEscan()
//            } else {
//                stopSelf()
//            }
        } else {
            Log.d(TAG, "onStartCommand: Ble Not supported")
            stopSelf()
        }

        return START_STICKY
    }

    private fun getBTService(): BluetoothAdapter? {
        val btManager = this.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        mBluetoothAdapter = btManager.adapter as BluetoothAdapter
        return mBluetoothAdapter
    }

    private fun isBluetoothSupported(): Boolean {
        return this.packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)
    }

    private fun startBLEscan() {
        mBluetoothAdapter!!.startLeScan(this)
    }

    private fun stopBLEscan() {
        mBluetoothAdapter!!.stopLeScan(this)
    }
}