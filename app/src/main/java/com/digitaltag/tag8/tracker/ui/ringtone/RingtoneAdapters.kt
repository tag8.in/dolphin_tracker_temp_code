package com.digitaltag.tag8.tracker.ui.ringtone

import android.content.Context
import android.media.RingtoneManager
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatRadioButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.model.AlertRingtones
import com.google.android.material.card.MaterialCardView


class RingtoneAdapters(
    private val context: Context,
    private val arrayList: ArrayList<AlertRingtones>
) : RecyclerView.Adapter<RingtoneAdapters.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val toneName: AppCompatTextView = itemView.findViewById(R.id.toneName)
        val card: MaterialCardView = itemView.findViewById(R.id.card)
        val radioBtn: AppCompatRadioButton = itemView.findViewById(R.id.radioBtn)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.select_ringtone_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val tone = arrayList[position]
        holder.toneName.text = tone.title

        holder.radioBtn.isChecked = SharedPreferenceManager.getRingtoneUris() == tone.path

        holder.radioBtn.setOnClickListener {
            NotifyListener.notify.notify("changed")

            SharedPreferenceManager.setRingtoneName(tone.title)
            SharedPreferenceManager.addRingtoneUri(tone.path)

            val ringtone: Uri = tone.path.toUri()
            val r = RingtoneManager.getRingtone(context, ringtone)
            r.play()

            Handler(Looper.myLooper()!!).postDelayed({
                r.stop()
            }, 3000)

        }

        holder.card.setOnClickListener {
            NotifyListener.notify.notify("changed")
            SharedPreferenceManager.setRingtoneName(tone.title)
            SharedPreferenceManager.addRingtoneUri(tone.path)
            val ringtone: Uri = tone.path.toUri()
            val r = RingtoneManager.getRingtone(context, ringtone)
            r.play()

            Handler(Looper.myLooper()!!).postDelayed({
                r.stop()
            }, 3000)

        }
    }

    override fun getItemCount(): Int = arrayList.size
}