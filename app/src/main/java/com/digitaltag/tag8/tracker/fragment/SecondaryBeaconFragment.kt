package com.digitaltag.tag8.tracker.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.model.BeaconDevice
import com.digitaltag.tag8.tracker.ui.beacon.BeaconActivity
import com.digitaltag.tag8.tracker.ui.settings.EditdetailActivity
import com.digitaltag.tag8.tracker.util.BLEType
import com.digitaltag.tag8.tracker.util.setSafeOnClickListener
import com.digitaltag.tag8.tracker.util.toast
import kotlinx.android.synthetic.main.fragment_secondary_beacon.*

class SecondaryBeaconFragment : Fragment() {

    private var beaconDevice: BeaconDevice? = null
    private var isBatterLevelCalled = false

    companion object {
        fun newInstance(beaconDevice: BeaconDevice): SecondaryBeaconFragment {
            return SecondaryBeaconFragment().apply {
                val bundle = Bundle().apply {
                    putParcelable(Constants.BEACON_DEVICE, beaconDevice)
                }
                arguments = bundle
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_secondary_beacon, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        beaconDevice = arguments?.getParcelable(Constants.BEACON_DEVICE)

        editbeaconlayout.setSafeOnClickListener {
            val intent = Intent(context, EditdetailActivity::class.java)
            intent.putExtra(Constants.BEACON_DEVICE, beaconDevice)
            startActivity(intent)

        }
        phonealertswitch1.isChecked = beaconDevice?.enablePhoneRing ?: false
        if (beaconDevice?.enableBeaconRing!!) {
            beaconalertswitch1.isChecked = true
            updateAlertOptions()
        }
        phonealertswitch1?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                requireActivity().toast(resources.getString(R.string.phonealert_message))
            }
            beaconDevice?.enablePhoneRing = isChecked
            updateAlertOptions()
        }

        editLockPasswrd.visibility = View.GONE
        if (beaconDevice?.bleType == BLEType.SMART_LOCK) {
            beaconalerttx.visibility = View.GONE
            beaconalertswitch1.visibility = View.GONE
            view2.visibility = View.GONE
            editLockPasswrd.visibility = View.VISIBLE
        }

        beaconalertswitch1?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (beaconDevice!!.isConnected()) {
                if (isChecked) {
                    requireActivity().toast(resources.getString(R.string.trackalert_message))
                }
                beaconDevice?.enableBeaconRing = isChecked
                updateAlertOptions()
            } else {
                requireActivity().toast(resources.getString(R.string.trackalert_fail))
                beaconalertswitch1.isChecked = !isChecked
            }

        }

        btnDisconnectDelete?.visibility = View.GONE
        btnDisconnectDelete?.setSafeOnClickListener {
            if (activity is BeaconActivity) {
                if (beaconDevice != null) {
                    if (beaconDevice?.isConnected() == true) {
                        (activity as BeaconActivity).disconnect(beaconDevice)
                    } else {
                        (activity as BeaconActivity).deleteBeacon(beaconDevice)
                    }
                }

            }
        }

    }


    private fun updateAlertOptions() {
        if (activity is BeaconActivity) {
            if (beaconDevice != null) {
                (activity as BeaconActivity).updateAlertOptions(beaconDevice)
            }
        }
    }

    fun updateBeaconDevice(beaconDevice: BeaconDevice?) {
        if (beaconDevice != null) {
            this.beaconDevice = beaconDevice
            if (!isBatterLevelCalled) {
                var isBatteryInfo =
                    (activity as BeaconActivity).mBluetoothLeService?.getBatterLevel(beaconDevice!!.macAddress)
                (activity as BeaconActivity).getBatteryLevel()
                if (isBatteryInfo!!) {
                    isBatterLevelCalled = true
                }
            }
            setDeviceData()
        }
    }

    private fun setDeviceData() {
        btnDisconnectDelete?.text = if (beaconDevice?.isConnected() == true) {
            resources.getString(R.string.disconnect)
        } else {
            resources.getString(R.string.deletebeacon)
        }
    }

}