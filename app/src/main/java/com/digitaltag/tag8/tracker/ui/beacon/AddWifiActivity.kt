package com.digitaltag.tag8.tracker.ui.beacon

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.addTrustedwifi
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.getTrustedwifi
import com.digitaltag.tag8.tracker.util.Utils
import com.digitaltag.tag8.tracker.util.setSafeOnClickListener
import kotlinx.android.synthetic.main.activity_add_wifi.*

class AddWifiActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_wifi)
        Tag8Application.getInstance().setActivity(this)
        val ssid = Utils.getCurrentSsid(this)

        backarrow.setSafeOnClickListener {
            finish()
        }
        Log.d("TAG", "onCreate: $ssid")
        wifiname.text = ssid

        val type: String?
        type = if (savedInstanceState == null) {
            val extras = intent.extras
            extras?.getString(getString(R.string.typebig))
        } else {
            savedInstanceState.getSerializable(getString(R.string.typebig)) as String?
        }

        val check = getTrustedwifi(type)

        if (check?.isNotEmpty()!!) {
            if (check == ssid)
                checkbox?.isChecked = true
        }
        checkbox?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                addTrustedwifi(ssid!!, type)
            } else {
                addTrustedwifi("", type)
            }
        }

    }

    override fun onResume() {
        super.onResume()
        Tag8Application.getInstance().setActivity(this)
    }
}