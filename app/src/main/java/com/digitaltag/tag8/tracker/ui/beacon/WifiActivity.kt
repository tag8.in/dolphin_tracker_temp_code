package com.digitaltag.tag8.tracker.ui.beacon

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.addTrustedwifiswitch
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.getTrustedwifiswitch
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.removeTrustedwifiswitch
import com.digitaltag.tag8.tracker.util.Utils
import com.digitaltag.tag8.tracker.util.setSafeOnClickListener
import kotlinx.android.synthetic.main.activity_wifi.*


class WifiActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wifi)

        val ssid = Utils.getCurrentSsid(this)
        trustedwifiswitch.isChecked = getTrustedwifiswitch()!!
        backarrow.setSafeOnClickListener{
            finish()
        }
        trustedwifiswitch?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                addTrustedwifiswitch()
            } else {
                removeTrustedwifiswitch()
            }
        }

        home.setSafeOnClickListener {
            val intent = Intent(this, AddWifiActivity::class.java)
            intent.putExtra(getString(R.string.typebig), getString(R.string.home))
            startActivity(intent)
        }

        office.setSafeOnClickListener {
            val intent = Intent(this, AddWifiActivity::class.java)
            intent.putExtra(getString(R.string.typebig), getString(R.string.office))
            startActivity(intent)
        }


        other.setSafeOnClickListener {
            val intent = Intent(this, AddWifiActivity::class.java)
            intent.putExtra(getString(R.string.typebig), getString(R.string.others))
            startActivity(intent)
        }


    }

    override fun onStart() {
        super.onStart()
        val checkhome = SharedPreferenceManager.getTrustedwifi(getString(R.string.home))
        if (checkhome?.isNotEmpty()!!) {
            homego.text = checkhome
        }else{
            homego.text = getString(R.string.clicktoaddwifi)
        }

        val checkoffice = SharedPreferenceManager.getTrustedwifi(getString(R.string.office))

        if (checkoffice?.isNotEmpty()!!) {
            officego.text = checkoffice
        }else{
            officego.text = getString(R.string.clicktoaddwifi)
        }

        val checkother = SharedPreferenceManager.getTrustedwifi(getString(R.string.others))
        if (checkother?.isNotEmpty()!!) {
            othergo.text = checkother
        }else{
            othergo.text = getString(R.string.clicktoaddwifi)
        }
    }

    override fun onResume() {
        super.onResume()
        Tag8Application.getInstance().setActivity(this)
    }

}