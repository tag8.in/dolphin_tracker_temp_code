package com.digitaltag.tag8.tracker.apis

import android.Manifest
import android.content.pm.PackageManager
import android.location.Geocoder
import android.os.Build
import android.util.Log
import androidx.core.app.ActivityCompat
import com.digitaltag.tag8.tracker.BuildConfig
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.google.android.gms.location.LocationServices
import com.google.firebase.auth.FirebaseAuth
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import java.io.IOException
import java.util.*


object ApiLiveData {

    private fun getCurrentUserEmail(): String {
        return FirebaseAuth.getInstance().currentUser?.email.toString()
    }

    private fun getCurrentUserName(): String {
        return FirebaseAuth.getInstance().currentUser?.displayName.toString()
    }

    private fun getCurrentUserUid(): String {
        return FirebaseAuth.getInstance().currentUser?.uid.toString()
    }

    private val geocoder = Geocoder(Tag8Application.getInstance(), Locale.getDefault())

    private val BASE_URL = "https://dolphin.tag8.in/api/dolphin/"
    val report_found = "reportfound"
    val update_location = "updatelocation"
    val report_lost = "reportlost"
    val community_trace = "communitytrace"
    val activate_qr = "activateqr"
    val reactivate_qr = "reactivateqr"
    val deactivate_qr = "deactivateqr"
    val delete_beacon = "deletebeacon"
    val connect = "connect"
    val disconnect = "disconnect"
    val update_category = "updatecategory"


    private val logging = HttpLoggingInterceptor().apply {
        setLevel(HttpLoggingInterceptor.Level.BASIC)
        setLevel(HttpLoggingInterceptor.Level.HEADERS)
        setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    var client = OkHttpClient().newBuilder()
        .addInterceptor(logging).build()

    fun getLocation() {
        val fusedLocationClient =
            LocationServices.getFusedLocationProviderClient(Tag8Application.getInstance())

        if (ActivityCompat.checkSelfPermission(
                Tag8Application.getInstance(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                Tag8Application.getInstance(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLocationClient.lastLocation
            .addOnSuccessListener {
                try {
                    updateLocation(it.latitude.toString(), it.longitude.toString())
                } catch (e: Exception) {
                    e.message
                }
            }
    }

    fun updateLocation(latitude: String, longitude: String) {
        return
        if (BuildConfig.DEBUG) {
            return
        }
        val formBody = FormBody.Builder()
            .add("email", getCurrentUserEmail())
            //   .add("uid", getCurrentUserName())
            .add("longitude", longitude)
            .add("latitude", latitude)
            .build()
        val request = Request.Builder()
            .url("$BASE_URL$update_location")
            .post(formBody)
            .addHeader("Content-Type", "application/x-www-form-urlencoded")
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.d("TAG", "Okhttp data onFailure: ${e.message}")
            }

            override fun onResponse(call: Call, response: Response) {
                val res = response.body?.string()
                Log.d("TAG", "Okhttp data onSucess: 1 ${res}")
            }
        })
    }

    fun reportLost(macAddress: String) {
        return
        if (BuildConfig.DEBUG) {
            return
        }
        getLocation()
        val formBody = FormBody.Builder()
            .add("email", getCurrentUserEmail())
            .add("mac_address", macAddress)
            .build()
        val request = Request.Builder()
            .url("$BASE_URL$report_lost")
            .post(formBody)
            .addHeader("Content-Type", "application/x-www-form-urlencoded")
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.d("TAG", "Okhttp data onFailure: ${e.message}")
            }

            override fun onResponse(call: Call, response: Response) {
                val res = response.body?.string()
                Log.d("TAG", "Okhttp data onSucess: 2 ${res}")
            }
        })
    }

    fun communityTrace(macAddress: String) {
        return
        if (BuildConfig.DEBUG) {
            return
        }
        val fusedLocationClient =
            LocationServices.getFusedLocationProviderClient(Tag8Application.getInstance())

        if (ActivityCompat.checkSelfPermission(
                Tag8Application.getInstance(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                Tag8Application.getInstance(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLocationClient.lastLocation
            .addOnSuccessListener {
                try {
                    val formBody = FormBody.Builder()
                        .add("latitude", it.latitude.toString())
                        .add("longitude", it.longitude.toString())
                        .add("mac_address", macAddress)
                        .build()
                    val request = Request.Builder()
                        .url("$BASE_URL$community_trace")
                        .post(formBody)
                        .addHeader("Content-Type", "application/x-www-form-urlencoded")
                        .build()

                    client.newCall(request).enqueue(object : Callback {
                        override fun onFailure(call: Call, e: IOException) {
                            Log.d("TAG", "Okhttp data onFailure: ${e.message}")
                        }

                        override fun onResponse(call: Call, response: Response) {
                            val res = response.body?.string()
                            Log.d("TAG", "Okhttp data onSucess: Tracker ${res}")
                        }
                    })
                } catch (e: Exception) {
                    e.message
                }
            }
    }

    fun newConnectBeacon(macAddress: String, type: String, bleDeviceName: String) {
        return
        if (BuildConfig.DEBUG) {
            return
        }
        getLocation()
        val fusedLocationClient =
            LocationServices.getFusedLocationProviderClient(Tag8Application.getInstance())

        if (ActivityCompat.checkSelfPermission(
                Tag8Application.getInstance(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                Tag8Application.getInstance(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLocationClient.lastLocation
            .addOnSuccessListener {
                val addressList =
                    geocoder.getFromLocation(it.latitude, it.longitude, 1)
                val address = addressList[0]
                val formBody = FormBody.Builder()
                    .add("email", getCurrentUserEmail())
                    .add("first_name", SharedPreferenceManager.firstName().toString())
                    .add("last_name", SharedPreferenceManager.lastName().toString())
                    .add("mac_address", macAddress)
                    .add("location", address.toString())
                    .add("latitude", it.latitude.toString())
                    .add("longitude", it.longitude.toString())
                    .add("product_type", type)
                    .add("device_id", "${Build.BRAND} ${Build.MODEL}")
                    .add("beacon_name", bleDeviceName)
                    .build()

                val request = Request.Builder()
                    .url("$BASE_URL$activate_qr")
                    .post(formBody)
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .build()

                client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        Log.d("TAG", "Okhttp data onFailure: ${e.message}")
                    }

                    override fun onResponse(call: Call, response: Response) {
                        val res = response.body?.string()
                        Log.d(
                            "TAG",
                            "Okhttp data onSucess newconnecte: 3 $res ${getCurrentUserEmail()}" +
                                    "${getCurrentUserName()} ${macAddress}"
                        )
                    }
                })
            }
    }

    fun deactivateQr(macAddress: String) {
        return
        if (BuildConfig.DEBUG) {
            return
        }
        getLocation()
        val formBody = FormBody.Builder()
            .add("email", getCurrentUserEmail())
            //  .add("uid", getCurrentUserName())
            .add("mac_address", macAddress)
            .build()
        val request = Request.Builder()
            .url("$BASE_URL$deactivate_qr")
            .post(formBody)
            .addHeader("Content-Type", "application/x-www-form-urlencoded")
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.d("TAG", "Okhttp data onFailure: ${e.message}")
            }

            override fun onResponse(call: Call, response: Response) {
                val res = response.body?.string()
                Log.d("TAG", "Okhttp data onSucess newconnecte:4 ${res}")
            }
        })
    }

    fun deleteBeacon(macAddress: String) {
        return
        if (BuildConfig.DEBUG) {
            return
        }
        getLocation()
        val formBody = FormBody.Builder()
            .add("email", getCurrentUserEmail())
            // .add("uid", getCurrentUserName())
            .add("mac_address", macAddress)
            .build()
        val request = Request.Builder()
            .url("$BASE_URL$delete_beacon")
            .post(formBody)
            .addHeader("Content-Type", "application/x-www-form-urlencoded")
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.d("TAG", "Okhttp data onFailure:5 ${e.message}")
            }

            override fun onResponse(call: Call, response: Response) {
                val res = response.body?.string()
                Log.d("TAG", "Okhttp data onSucess newconnecte:6 ${res}")
            }
        })
    }

    fun connect(macAddress: String) {
        return
        if (BuildConfig.DEBUG) {
            return
        }
        getLocation()
        val formBody = FormBody.Builder()
            .add("email", getCurrentUserEmail())
            //  .add("uid", getCurrentUserName())
            .add("mac_address", macAddress)
            .build()
        val request = Request.Builder()
            .url("$BASE_URL$connect")
            .post(formBody)
            .addHeader("Content-Type", "application/x-www-form-urlencoded")
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.d("TAG", "Okhttp data onFailure: ${e.message}")
            }

            override fun onResponse(call: Call, response: Response) {
                val res = response.body?.string()
                Log.d("TAG", "Okhttp data onSucess newconnecte: 7${res}")
            }
        })
    }

    fun disconnect(macAddress: String) {
        return
        if (BuildConfig.DEBUG) {
            return
        }
        getLocation()
        val formBody = FormBody.Builder()
            .add("email", getCurrentUserEmail())
            // .add("uid", getCurrentUserName())
            .add("mac_address", macAddress)
            .build()
        val request = Request.Builder()
            .url("$BASE_URL$disconnect")
            .post(formBody)
            .addHeader("Content-Type", "application/x-www-form-urlencoded")
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.d("TAG", "Okhttp data onFailure: ${e.message}")
            }

            override fun onResponse(call: Call, response: Response) {
                val res = response.body?.string()
                Log.d("TAG", "Okhttp data onSucess newconnecte: 8 ${res}")
            }
        })
    }

    fun updatephoto(macAddress: String) {
        if (BuildConfig.DEBUG) {
            return
        }
        getLocation()
        val formBody = FormBody.Builder()
            .add("email", getCurrentUserEmail())
            //  .add("uid", getCurrentUserName())
            .add("mac_address", macAddress)
            .add("url", "urlss")
            .build()
        val request = Request.Builder()
            .url("$BASE_URL$delete_beacon")
            .post(formBody)
            .addHeader("Content-Type", "application/x-www-form-urlencoded")
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.d("TAG", "Okhttp data onFailure: ${e.message}")
            }

            override fun onResponse(call: Call, response: Response) {
                val res = response.body?.string()
                Log.d("TAG", "Okhttp data onSucess newconnecte: ${res}")
            }
        })
    }

    fun updateCategory(macAddress: String, category: String) {
        if (BuildConfig.DEBUG) {
            return
        }
        val formBody = FormBody.Builder()
            .add("email", getCurrentUserEmail())
            //  .add("uid", getCurrentUserName())
            .add("mac_address", macAddress)
            .add("category", macAddress)
            .build()
        val request = Request.Builder()
            .url("$BASE_URL$update_category")
            .post(formBody)
            .addHeader("Content-Type", "application/x-www-form-urlencoded")
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.d("TAG", "Okhttp data onFailure: ${e.message}")
            }

            override fun onResponse(call: Call, response: Response) {
                val res = response.body?.string()
                Log.d("TAG", "Okhttp data onSucess newconnecte:9 ${res}")
            }
        })
    }

}