package com.digitaltag.tag8.tracker.util

class ConnectedCallback {
    companion object{
        lateinit var connectionCallback: ConnectionCallback
    }
    interface ConnectionCallback{
        fun data(macAddress:String,connection:String)
    }
    fun setConnectionCallback(
        conn: ConnectionCallback
        ){
        connectionCallback = conn
    }

}