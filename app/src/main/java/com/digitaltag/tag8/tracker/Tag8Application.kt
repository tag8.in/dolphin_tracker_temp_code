package com.digitaltag.tag8.tracker

import android.app.Activity
import android.app.Application
import com.digitaltag.tag8.tracker.ble.Tag8BluetoothGatt
import com.digitaltag.tag8.tracker.ui.arrival.BeaconMacCheck

class Tag8Application : Application() {
    var deviceHashMap = HashMap<String, Tag8BluetoothGatt>()
    private var mActivity: Activity? = null

    companion object {
        @Volatile
        private var tag8Application: Tag8Application? = null

        @Synchronized
        fun getInstance(): Tag8Application {
            return tag8Application!!
        }

        val arrivalAlertConnectedList = ArrayList<BeaconMacCheck>()
    }

    override fun onCreate() {
        super.onCreate()
        tag8Application = this
    }

    fun getActivity(): Activity {
        return mActivity!!
    }

    fun setActivity(activity: Activity) {
        mActivity = activity
    }

}