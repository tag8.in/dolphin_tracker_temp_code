package com.digitaltag.tag8.tracker.modules

object URLS {
    var mainurl: String = "https://digitalstage123xyz.tag8.co.in/api/"
    val gettoken: String = mainurl + "getuser/"
    val login: String = mainurl + "mobile/login"
    val deletebeacon: String = mainurl + "mobile/deleteBeacon"
    val sendbeacondetail: String = mainurl + "beacondetails"
    val getbeacons: String = mainurl + "getbeacons/"
    val updatebeacondata: String = mainurl + "mobile/updatebeacon"
    val beaconcommunitytrace: String = mainurl + "beaconcommunitytrace"
    val beaconconnection: String = mainurl + "updatebeaconconnection"
    val getbeaconlostdetails: String = mainurl + "mobile/getbeaconlostdetails"
    val beaconloststatuslocation: String = mainurl + "beaconloststatuslocation"

    val getbeacondetails: String = mainurl + "getbeacondetails/"
    val getbeaconlocationbyuid: String = mainurl + "getbeaconlocationbyuid/"
    val getbeaconloststatuslocation: String = mainurl + "getbeaconloststatuslocation/"
}