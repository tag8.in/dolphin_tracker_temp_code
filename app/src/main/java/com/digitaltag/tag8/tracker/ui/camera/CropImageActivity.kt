package com.digitaltag.tag8.tracker.ui.camera

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.ui.camera.fragments.CropDemoPreset
import com.digitaltag.tag8.tracker.ui.camera.fragments.CropFragment
import com.digitaltag.tag8.tracker.ui.camera.fragments.CropFragment.Companion.newInstance
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class CropImageActivity : AppCompatActivity() {
    private var mCurrentFragment: CropFragment? = null
    private var mCropImageUri: Uri? = null
    var mBitmap: Bitmap? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cropimages)
        Tag8Application.getInstance().setActivity(this)
        if (intent != null) {
            val imagepath = intent.getStringExtra("imagepath")
            mCropImageUri = Uri.fromFile(File(imagepath))
        }
        setMainFragmentByPreset(CropDemoPreset.CIRCULAR)
    }

    override fun onResume() {
        super.onResume()
        Tag8Application.getInstance().setActivity(this)
    }

    fun setCurrentFragment(fragment: CropFragment?) {
        mCurrentFragment = fragment
    }

    fun setImageUri() {
        mCurrentFragment!!.setImageUri(mCropImageUri)
    }

    fun setResult(bitmap: Bitmap) {
        mBitmap = bitmap
        val imagePath = storeImage(bitmap)
        val intent = Intent()
        intent.putExtra("imagepath", imagePath)
        intent.action = "com.deb.imagecrop"
        sendBroadcast(intent)
        finish()

        /*Intent intent = new Intent();
        intent.putExtra("BitmapImage", bitmap);
        setResult(2, intent);*/
    }

    private fun storeImage(image: Bitmap): String {
        val pictureFile = outputMediaFile
        if (pictureFile == null) {
            Log.d(TAG,
                    "Error creating media file, check storage permissions: ") // e.getMessage());
            return ""
        }
        try {
            val fos = FileOutputStream(pictureFile)
            image.compress(Bitmap.CompressFormat.PNG, 90, fos)
            fos.close()
        } catch (e: FileNotFoundException) {
            Log.d(TAG, "File not found: " + e.message)
        } catch (e: IOException) {
            Log.d(TAG, "Error accessing file: " + e.message)
        }
        return pictureFile.absolutePath
    }// To be safe, you should check that the SDCard is mounted
    // using Environment.getExternalStorageState() before doing this.

    // This location works best if you want the created images to be shared
    // between applications and persist after your app has been uninstalled.

    // Create the storage directory if it does not exist
    /* Create a media file name */

    /** Create a File for saving an image or video  */
    private val outputMediaFile: File?
        private get() {
            // To be safe, you should check that the SDCard is mounted
            // using Environment.getExternalStorageState() before doing this.
            val mediaStorageDir = File(Environment.getExternalStorageDirectory()
                    .toString() + "/Android/data/"
                    + applicationContext.packageName
                    + "/Files")

            // This location works best if you want the created images to be shared
            // between applications and persist after your app has been uninstalled.

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null
                }
            }
            /* Create a media file name */
            val timeStamp = SimpleDateFormat("ddMMyyyy_HHmm").format(Date())
            val mediaFile: File
            val mImageName = "beacon_$timeStamp.jpg"
            mediaFile = File(mediaStorageDir.path + File.separator + mImageName)
            return mediaFile
        }

    private fun setMainFragmentByPreset(demoPreset: CropDemoPreset) {
        val fragmentManager = supportFragmentManager
        fragmentManager
                .beginTransaction()
                .replace(R.id.container, newInstance())
                .commit()
    }

    companion object {
        private const val TAG = "CropImageActivity"
    }
}