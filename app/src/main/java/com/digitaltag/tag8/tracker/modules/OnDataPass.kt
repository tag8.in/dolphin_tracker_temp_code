package com.digitaltag.tag8.tracker.modules

import android.view.View

interface OnDataPass {
    fun onDataPass(data: String)
}