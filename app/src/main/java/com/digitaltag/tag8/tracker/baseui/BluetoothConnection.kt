package com.digitaltag.tag8.tracker.baseui

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Context
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.model.BeaconDevice
import com.digitaltag.tag8.tracker.ui.beacon.ConnectionActivity
import com.digitaltag.tag8.tracker.util.DebugLogger
import java.util.*
import kotlin.collections.ArrayList

class BluetoothConnection(val context: Context, val callback: Callback) {

    private var mBluetoothManager: BluetoothManager? = null
    private var mBluetoothAdapter: BluetoothAdapter? = null
    private var mScanning: Boolean = false
    private var oldBluetoothScan: OldBluetoothScan? = null
    private var newBluetoothScan: NewBluetoothScan? = null
    private var specificMacAddress: String? = null
    private var specificLatitude: String = ""
    private var specificLongitude: String = ""
    private val scanHandler = Handler(Looper.getMainLooper())
    private var scanRunnable: Runnable? = null
    private var beaconDevice: BeaconDevice? = null

    companion object {
        val TAG = DebugLogger.makeLogTag(BluetoothConnection::class.java)
    }

    init {
        if (mBluetoothManager == null) {
            mBluetoothManager =
                context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            if (mBluetoothManager == null) {
                DebugLogger.d(TAG, "Unable to initialize BluetoothManager.")
            }
        }
        mBluetoothAdapter = mBluetoothManager?.adapter
        if (mBluetoothAdapter == null) {
            DebugLogger.d(TAG, "Unable to obtain a BluetoothAdapter.")
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            oldBluetoothScan = OldBluetoothScan()
        } else {
            newBluetoothScan = NewBluetoothScan()
        }
        mBluetoothAdapter?.enable()
    }

    fun isEnable(): Boolean {
        return mBluetoothAdapter != null
    }

    fun startScan(beaconDevice: BeaconDevice? = null) {
        if (!isEnable()) {
            return
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            if (oldBluetoothScan == null) {
                Log.d(TAG, "startScan: type oldscan")
                oldBluetoothScan = OldBluetoothScan()
            }
        } else {
            if (newBluetoothScan == null) {
                Log.d(TAG, "startScan: type newscan")
                newBluetoothScan = NewBluetoothScan()
            }
        }
        this.specificMacAddress = beaconDevice?.macAddress
        //getLostBeaconDetails(specificMacAddress.toString())

        Log.d(ConnectionActivity.TAG, "scan: started scan 2")
        if (!mScanning) {
            mScanning = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val builder = ScanFilter.Builder()
                builder.setServiceUuid(Constants.UUIDs.PARCEL_UUID_IMMEDIATE_ALERT_SERVICE)
                val filters: ArrayList<ScanFilter> = ArrayList(2)
                filters.add(builder.build())
                val setsBuild = ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_BALANCED)
                val scanner = mBluetoothAdapter?.bluetoothLeScanner
                scanner?.flushPendingScanResults(newBluetoothScan)
                scanner?.startScan(filters, setsBuild.build(), newBluetoothScan)
                Log.d(ConnectionActivity.TAG, "scan: started scan 3")
                getLostBeaconDetails(specificMacAddress.toString())
                true
            } else {
                val trackUUID = arrayOf<UUID>(Constants.UUIDs.IMMEDIATE_ALERT_SERVICE_UUID)
                mBluetoothAdapter?.startLeScan(trackUUID, oldBluetoothScan)
                true
            }
        }
        if (scanRunnable != null) {
            scanHandler.removeCallbacks(scanRunnable!!)
        }
        scanRunnable = Runnable {
            stopScan()
            Log.d(TAG, "startScan: 96")
            callback.onScanFailed(0, specificMacAddress)
        }
        scanHandler.postDelayed(scanRunnable!!, 12000)
    }

    fun stopScan() {
        if (mScanning) {
            mScanning = false
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                mBluetoothAdapter?.stopLeScan(oldBluetoothScan)
            } else {
                val scanner = mBluetoothAdapter?.bluetoothLeScanner
                scanner?.stopScan(newBluetoothScan)
            }
        }
    }

    fun handleScanResult(device: BluetoothDevice, rssi: Int) {
        Log.d(TAG, "handleScanResult: inside rssi $rssi")
        callback.handleScanResult(device, rssi)
        if (scanRunnable != null) {
            scanHandler.removeCallbacks(scanRunnable!!)
        }
        Log.d(ConnectionActivity.TAG, "scan: started scan 5")
        stopScan()
    }

    private inner class OldBluetoothScan : BluetoothAdapter.LeScanCallback {
        override fun onLeScan(device: BluetoothDevice?, rssi: Int, scanRecord: ByteArray?) {
            if (device != null) {
                if (specificMacAddress?.isNotEmpty() == true) {
                    if (specificMacAddress?.equals(device.address) == true) {
                        handleScanResult(device, rssi)
                    }
                } else {
                    handleScanResult(device, rssi)
                }
            } else {
                Log.d(TAG, "startScan: 135")
                callback.onScanFailed(0, specificMacAddress)
            }
        }
    }

    private inner class NewBluetoothScan : ScanCallback() {
        override fun onScanFailed(errorCode: Int) {
            super.onScanFailed(errorCode)
            DebugLogger.d(ConnectionActivity.TAG, "onScanFailed $errorCode}")
            mScanning = false
        }

        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)
            Log.d(TAG, "onScanResult: $callbackType")
            Log.d(ConnectionActivity.TAG, "scan: started scan 4")
            if (callbackType == ScanSettings.CALLBACK_TYPE_MATCH_LOST) {
                Log.d(TAG, "onScanResult: inside $callbackType")
                callback.onScanFailed(0, specificMacAddress)
                return
            }
            val device = result?.device
            val rslt = result?.scanRecord
            if (device != null && rslt != null) {
                Log.d(TAG, "onScanResult: inside 156  $callbackType")
                if (specificMacAddress?.isNotEmpty() == true) {
                    if (specificMacAddress?.equals(device.address) == true) {
                        Log.d(TAG, "onScanResult: inside 159 $callbackType")
                        handleScanResult(device, result.rssi)
                    }
                } else {
                    Log.d(TAG, "onScanResult: inside 163 $callbackType")
                    handleScanResult(device, result.rssi)
                }
            } else {
                Log.d(TAG, "startScan: 171")
                callback.onScanFailed(0, specificMacAddress)
            }
        }

        override fun onBatchScanResults(results: List<ScanResult>?) {
            super.onBatchScanResults(results)
            if (results?.isNotEmpty() == true) {
                for (scanResult in results) {
                    onScanResult(10, scanResult)
                }
            } else {
                Log.d(TAG, "startScan: 183")
                callback.onScanFailed(0, specificMacAddress)
            }
        }
    }

    interface Callback {
        fun handleScanResult(device: BluetoothDevice, rssi: Int)
        fun onScanFailed(errorCode: Int, specificMacAddress: String? = null)
    }

    private fun getLostBeaconDetails(macAdd: String) {
        this.specificLatitude = beaconDevice?.latitude.toString()
        this.specificLongitude = beaconDevice?.longitude.toString()
    }

}