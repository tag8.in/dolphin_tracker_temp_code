package com.digitaltag.tag8.tracker.ui.beacon

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.app.Dialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.graphics.Point
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.baseui.BaseActivity
import com.digitaltag.tag8.tracker.baseui.BluetoothConnection
import com.digitaltag.tag8.tracker.ble.BluetoothLeService
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.model.BeaconDevice
import com.digitaltag.tag8.tracker.util.*
import kotlinx.android.synthetic.main.activity_connection.*
import java.util.*


class ConnectionActivity : BaseActivity(), BluetoothConnection.Callback,
    BluetoothLeService.Callback {

    companion object {
        val TAG = DebugLogger.makeLogTag(ConnectionActivity::class.java)
        val beaconsNameList = arrayListOf("iTrack", "tag8", "aaa")
    }

    private var mServiceConnection: ServiceConnection? = null
    private var bluetoothConnection: BluetoothConnection? = null
    private var mBluetoothLeService: BluetoothLeService? = null
    private var scanAuto = false
    private var lftToRgt: ObjectAnimator? = null
    private var rgtToLft: ObjectAnimator? = null
    private var halfW = 0f
    private var animatorSet: AnimatorSet? = null
    private var bletype: BLEType = BLEType.NONE


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_connection)
        bletype = getBLEType()

        Tag8Application.getInstance().setActivity(this)
        val display = windowManager.defaultDisplay
        val point = Point()
        display.getSize(point)
        val width: Int = point.x // screen width

        halfW = width / 2.0f // half the width or to any value required,global to class
        animatorSet = AnimatorSet()
        dotprogress.setDotsCount(6)

        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "Ble not supported", Toast.LENGTH_SHORT).show()
            finish()
        }

        Permissions.permissionList(this@ConnectionActivity)

        bluetoothConnection = BluetoothConnection(this, this)

        val back = findViewById<ImageView>(R.id.back)
        backarrow.setSafeOnClickListener {
            finish()
        }
        mServiceConnection = object : ServiceConnection {
            override fun onServiceDisconnected(name: ComponentName?) {
                bluetoothConnection?.stopScan()
            }

            override fun onServiceConnected(name: ComponentName?, iBinder: IBinder?) {
                if (iBinder is BluetoothLeService.LocalBinder) {
                    mBluetoothLeService = iBinder.service
                    mBluetoothLeService?.addCallback(this@ConnectionActivity)
                    if (scanAuto) {
                        bluetoothConnection?.startScan(null)
                        Log.d(TAG, "onServiceConnected: scannedstarted")
                    }
                }
            }

        }
        if (!isBluetoothEnabled()) {
            enableBluetooth()
        }
        if (checkGpsStatus()) {
            scan()
        } else {
            var intent1 = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivity(intent1)
        }

        bindService(
            Intent(this, BluetoothLeService::class.java),
            mServiceConnection!!,
            Context.BIND_AUTO_CREATE
        )
        next.setSafeOnClickListener {
            if (bletype == BLEType.SMART_TRACKER) {
                if (!isBluetoothEnabled()) {
                    enableBluetooth()
                }
                if (checkGpsStatus()) {
                    scan()
                } else {
                    var intent1 = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivity(intent1)
                }
            } else if (bletype == BLEType.SMART_LOCK) {
                if (!isBluetoothEnabled()) {
                    dialogBluetoothStatus();
                } else {
                    if (checkGpsStatus()) {
                        scan()

                    } else {
                        var intent1 = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivity(intent1)
                    }
                }
            }
        }
        rescanBtn.setSafeOnClickListener {
            if (!isBluetoothEnabled()) {
                enableBluetooth()
            }
            if (checkGpsStatus()) {
                scan()
            } else {
                var intent1 = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent1)
            }
        }

    }

    override fun onResume() {
        super.onResume()
        Tag8Application.getInstance().setActivity(this)
    }

    fun scan() {
        Utils.updateType = getString(R.string.addnew)
        clNotice.visibility = View.GONE
        clRescan.visibility = View.GONE
        beaconnotice.text = resources.getString(R.string.scanning)
        clFindBeacon.visibility = View.VISIBLE
        //handler.post(periodicupdate!!)
        /* timer.scheduleAtFixedRate(object: TimerTask(){
             override fun run() {

             }
         }, 0, 1400)*/

        if (bletype == BLEType.SMART_TRACKER) {
            device.setBackgroundResource(R.drawable.device)
        } else if (bletype == BLEType.SMART_LOCK) {
            device.setBackgroundResource(R.drawable.ic_locked_padlock)
        }

        Log.d(TAG, "scan: started scan 1")
        Handler(Looper.getMainLooper()).postDelayed({ anim() }, 100)
        dotprogress.start()
        if (mBluetoothLeService == null) {
            scanAuto = true
        } else {
            bluetoothConnection?.startScan(null)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        bluetoothConnection?.stopScan()
        mBluetoothLeService?.removeCallback(this@ConnectionActivity)
        if (mServiceConnection != null) {
            unbindService(mServiceConnection!!)
        }
    }

    override fun handleScanResult(device: BluetoothDevice, rssi: Int) {
        val trackName = device.name ?: return

        if (bletype == BLEType.SMART_TRACKER && trackName.compareTo("iTrack", true) == 0) {
            Log.d("TAG", "onDeviceAdded: ${bletype.toString()} , $trackName")
        } else if (bletype == BLEType.SMART_TRACKER && trackName.compareTo("tag8", true) == 0) {
            Log.d("TAG", "onDeviceAdded: ${bletype.toString()} , $trackName")
        } else if (bletype == BLEType.SMART_TRACKER && trackName.compareTo("aaa", true) == 0) {
            Log.d("TAG", "onDeviceAdded: ${bletype.toString()} , $trackName")
        } else if (bletype == BLEType.SMART_LOCK && trackName.compareTo("iLck3", true) == 0) {
            Log.d(TAG, "onDeviceAdded: ${bletype.toString()} , $trackName")

            val customDialog = Dialog(this)
            customDialog.setContentView(R.layout.dialog_lock_found)
            customDialog.window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            val okBtn = customDialog.findViewById(R.id.ok_opt) as TextView
            val cancelBtn = customDialog.findViewById(R.id.cancel_opt) as TextView
            val editbox = customDialog.findViewById(R.id.dialog_editbox) as EditText
            okBtn.setOnClickListener {
                customDialog.dismiss()
                var passcode = editbox.text.toString()
                mBluetoothLeService!!.writePasswordToLock(device.address, passcode)
            }
            cancelBtn.setOnClickListener {
                customDialog.dismiss()
            }
            customDialog.show()
            customDialog.setCanceledOnTouchOutside(false)

        } else {
            return
        }
        if (rssi < -75) {
            return
        }
        val tag8BluetoothGatt =
            mBluetoothLeService?.getTag8BluetoothGattByMacAddress(device.address)
        if (tag8BluetoothGatt != null) {
            DebugLogger.d(
                TAG,
                "Scan success, found Existing Track " + device.address + device.name + rssi
            )
        } else {
            DebugLogger.d(
                TAG,
                "Scan success, found new Track " + device.address + device.name + rssi
            )
            bluetoothConnection?.stopScan()
        }
        Log.d(TAG, "onDeviceAdded: device becon")
        mBluetoothLeService?.addOrUpdateDevice(device, rssi, bletype)

//        Log.d(BluetoothConnection.TAG, "onScanResult: th name ${trackName}")
//        val tag8BluetoothGatt =
//            mBluetoothLeService?.getTag8BluetoothGattByMacAddress(device.address)
//
//        if (bletype == BLEType.SMART_LOCK && trackName == "iLck3") {
//            CoroutineScope(Dispatchers.Main).launch {
//                val customDialog = Dialog(this@ConnectionActivity)
//                customDialog.setContentView(R.layout.dialog_lock_found)
//                customDialog.window?.setLayout(
//                    ViewGroup.LayoutParams.MATCH_PARENT,
//                    ViewGroup.LayoutParams.WRAP_CONTENT
//                )
//                val okBtn = customDialog.findViewById(R.id.ok_opt) as TextView
//                val cancelBtn = customDialog.findViewById(R.id.cancel_opt) as TextView
//                val editbox = customDialog.findViewById(R.id.dialog_editbox) as EditText
//                okBtn.setOnClickListener {
//                    customDialog.dismiss()
//                    val passcode = editbox.text.toString()
//                    // Log.d("Nirav", "tag8BluetoothGatt is dialog ${lastDevice!!.address}")
//                    mBluetoothLeService?.addOrUpdateDevice(device, rssi, bletype)
//                    CoroutineScope(Dispatchers.IO).launch {
//                        delay(1000)
//                        mBluetoothLeService!!.writePasswordToLock(device.address, passcode)
//                    }
//                    Toast.makeText(this@ConnectionActivity, "code : " + passcode, Toast.LENGTH_LONG)
//                        .show()
//                }
//                cancelBtn.setOnClickListener {
//                    customDialog.dismiss()
//                }
//                customDialog.show()
//                customDialog.setCanceledOnTouchOutside(false)
//            }
//            return
//        }
//
//        if (bletype == BLEType.SMART_LOCK) {
//            return
//        }
//
//        if (rssi < -75) {
//            return
//        }
//
//        if (!beaconsNameList.toString().contains(trackName)) {
//            return
//        }
//        if (tag8BluetoothGatt != null) {
//            DebugLogger.d(
//                TAG,
//                "Scan success, found Existing Track " + device.address + device.name + rssi
//            )
//        } else {
//            DebugLogger.d(
//                TAG,
//                "Scan success, found new Track " + device.address + device.name + rssi
//            )
//            bluetoothConnection?.stopScan()
//        }
//        mBluetoothLeService?.addOrUpdateDevice(device, rssi, bletype)
    }

    override fun onScanFailed(errorCode: Int, specificMacAddress: String?) {
        Log.d(TAG, "onDeviceAdded: device failed")
        clNotice.visibility = View.GONE
        clRescan.visibility = View.VISIBLE
        beaconnotice.text = resources.getString(R.string.scanfail)
        clFindBeacon.visibility = View.GONE
    }

    override fun onDeviceAdded(beaconDevice: BeaconDevice) {
        Log.d("TAG", "onDeviceAdded: On Added devices")
        Log.d(TAG, "onDeviceAdded: device added")
        SharedPreferenceManager.addFirstTimeConnect(beaconDevice.macAddress)
        val intent = Intent(this, EditBeaconActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(Constants.BEACON_DEVICE, beaconDevice)
        intent.putExtra(Constants.IS_NEW, true)
        startActivity(intent)
        Log.d(TAG, "onDeviceAdded: device added finished")
        finish()
    }

    override fun onDeviceUpdated(beaconDevice: BeaconDevice) {
        Log.d(TAG, "onDeviceUpdated: device updted ${beaconDevice}")
    }

    override fun onDeviceRemoved(beaconDevice: BeaconDevice) {

    }

    fun anim() {
        val yval = device.x / 2
        lftToRgt = ObjectAnimator.ofFloat(magnifier, "translationX", 0f, yval)
            .setDuration(3000) // to animate left to right
        rgtToLft = ObjectAnimator.ofFloat(magnifier, "translationX", yval, 0f)
            .setDuration(3000) // to animate right to left
        animatorSet!!.play(lftToRgt).before(rgtToLft) // manage sequence
        animatorSet!!.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                animatorSet!!.start()
            }
        })
        Handler(Looper.getMainLooper()).post { animatorSet!!.start() }

    }

    fun isBluetoothEnabled(): Boolean {
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        return mBluetoothAdapter.isEnabled
    }

    fun enableBluetooth() {
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        mBluetoothAdapter.enable()
    }

    fun checkGpsStatus(): Boolean {
        var locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        assert(locationManager != null)
        var gpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        return gpsStatus
    }


    fun getBLEType(): BLEType {
        val extras = intent.getStringExtra("ble_type")
        var ble: BLEType = BLEType.NONE;
        ble = if (extras == BLEType.SMART_LOCK.toString()) {
            BLEType.SMART_LOCK
        } else {
            BLEType.SMART_TRACKER
        }
        return ble;
    }


    private fun dialogBluetoothStatus() {
        val customDialog = Dialog(this)
        customDialog.setContentView(R.layout.dialog_bluetooth_status)
        customDialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        val okBtn = customDialog.findViewById(R.id.ok_opt) as TextView
        val cancelBtn = customDialog.findViewById(R.id.cancel_opt) as TextView
        okBtn.setOnClickListener {
            customDialog.dismiss()
            enableBluetooth()
            if (checkGpsStatus()) {
                scan()
            } else {
                var intent1 = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent1)
            }
        }
        cancelBtn.setOnClickListener {
            customDialog.dismiss()
        }
        customDialog.show()
    }

}

