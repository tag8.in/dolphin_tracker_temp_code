package com.digitaltag.tag8.tracker.ui.login

import com.google.firebase.auth.FirebaseUser

interface LoginCallBack {

    fun onGoogleLoginSuccess(user: FirebaseUser?)
    fun onGoogleLoginFail(message: String)
}