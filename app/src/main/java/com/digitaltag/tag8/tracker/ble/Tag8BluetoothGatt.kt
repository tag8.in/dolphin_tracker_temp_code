package com.digitaltag.tag8.tracker.ble

import android.Manifest
import android.app.*
import android.bluetooth.*
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.media.*
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.speech.tts.TextToSpeech
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.net.toUri
import com.digitaltag.tag8.tracker.AlertDialogShowActivity
import com.digitaltag.tag8.tracker.NavigationActivity
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.apis.ApiLiveData
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.addtrackerBattery
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.removetrackerOFF
import com.digitaltag.tag8.tracker.model.BeaconDevice
import com.digitaltag.tag8.tracker.ui.camera.fragments.CameraFragment
import com.digitaltag.tag8.tracker.ui.endless.log
import com.digitaltag.tag8.tracker.util.*
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.soumya.meats4all.utils.DialogButtonsCallBack
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.nio.ByteBuffer
import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.Locale
import kotlin.experimental.xor


class Tag8BluetoothGatt(
    private val mContext: Context, private val mBluetoothAdapter: BluetoothAdapter?,
    private val callbacks: ArrayList<BluetoothLeService.Callback>
) : BluetoothGattCallback() {

    private val ringTone: Ringtone? =
        RingtoneManager.getRingtone(
            mContext, SharedPreferenceManager.getRingtoneUris()!!
                .toUri()
        )!!
    private val handler = Handler(Looper.myLooper()!!)
    private val handlerDisconnected = Handler(Looper.myLooper()!!)

    companion object {
        val TAG = DebugLogger.makeLogTag(Tag8BluetoothGatt::class.java)
        var BEACON_NOTIFICATION = ""
        fun isAppIsInBackground(context: Context): Boolean {
            var isInBackground = true
            val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                val runningProcesses = am.runningAppProcesses
                for (processInfo in runningProcesses) {
                    if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        for (activeProcess in processInfo.pkgList) {
                            if (activeProcess == context.packageName) {
                                isInBackground = false
                            }
                        }
                    }
                }
            } else {
                val taskInfo = am.getRunningTasks(1)
                val componentInfo = taskInfo[0].topActivity
                if (componentInfo!!.packageName == context.packageName) {
                    isInBackground = false
                }
            }

            return isInBackground
        }
    }

    private var mKeyNotify: Int = 0
    private var mBluetoothGatt: BluetoothGatt? = null
    var beaconDevice: BeaconDevice? = null
    private var rssiHandler: Handler? = null
    private var rssiHandlerThread: HandlerThread? = null
    private var rssiRunnanle: Runnable? = null
    private var nTimesRing = 0
    private var fusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext)
    private var mode = Constants.Beacon.Mode.BEACON
    var isPhoneAlert: Boolean = false


    override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
        Log.d(TAG, "onclicked runned 4 ")
        val tmpBlePerp = gatt!!.device
        if (tmpBlePerp.uuids != null) {
            for (uuid in tmpBlePerp.uuids) {
                DebugLogger.d(TAG, "uuid => ${uuid.uuid?.toString()}")
            }
        }
        if (beaconDevice?.macAddress != tmpBlePerp.address) {
            return
        }

        if (mBluetoothGatt != gatt) {
            mBluetoothGatt = gatt
        }

        if (status == BluetoothGatt.GATT_SUCCESS) {
            Log.d("Nirav", "BluetoothGatt.GATT_SUCCESS")
            if (beaconDevice?.bleType == BLEType.SMART_TRACKER) {
                Log.d("Nirav", "writePassword2Itrack")
                writePassword2Itrack(gatt)
            } else {
                Log.d(TAG, "getDeviceInfo ")
                getDeviceInfo(beaconDevice?.macAddress!!)
            }
        } else {
            Log.d("Nirav", "OnServicesDiscovered connect()")
            connect()
        }
        if (status == BluetoothGatt.GATT_SUCCESS) {
            writePassword2Itrack(gatt)
        } else {
            connect()
        }
    }

    fun getDeviceInfo(macAddress: String): Boolean {
        var isDeviceInfo = false
        Log.d(TAG, "BluetoothGTT getDeviceInfo");
        if (beaconDevice?.macAddress?.equals(macAddress) == true) {
            val deviceInfoService: BluetoothGattService? =
                mBluetoothGatt?.getService(UUID.fromString(Constants.UUIDs.CUSTOM_SERVICE.toString()))
            if (deviceInfoService == null) {
                Log.d(TAG, "DeviceInfo service not found!");
            } else {
                val deviceInfoCheck: BluetoothGattCharacteristic? =
                    deviceInfoService?.getCharacteristic(UUID.fromString(Constants.UUIDs.CHAR_DEV_INFO_UUID.toString()))

                if (deviceInfoCheck == null) {
                    Log.d(TAG, "Battery characteristics not found!");
                } else {
                    isDeviceInfo = mBluetoothGatt!!.readCharacteristic(deviceInfoCheck)
                    Log.d(TAG, "DeviceInfo: ${isDeviceInfo}")
                }
            }
        }
        return isDeviceInfo
    }


    override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
        if (gatt == null) {
            return
        }
        Log.d(TAG, "onclicked runned 1 ")
        val tmpBlePerp = gatt.device
        if (tmpBlePerp.uuids != null) {
            for (uuid in tmpBlePerp.uuids) {
                DebugLogger.d(TAG, "uuid => ${uuid.uuid?.toString()}")
            }
        }
        if (beaconDevice?.macAddress != tmpBlePerp.address) {
            return
        }

        if (mBluetoothGatt != gatt) {
            mBluetoothGatt = gatt
        }

        beaconDevice?.connectionState = when (newState) {
            BluetoothAdapter.STATE_CONNECTING -> Constants.Beacon.State.CONNECTING
            BluetoothAdapter.STATE_CONNECTED -> Constants.Beacon.State.CONNECTED
            BluetoothAdapter.STATE_DISCONNECTING -> Constants.Beacon.State.DISCONNECTING
            else -> when {
                beaconDevice?.connectionState?.equals(Constants.Beacon.State.DISCONNECTED) == true -> {
                    Constants.Beacon.State.DISCONNECTED
                }
                beaconDevice?.connectionState?.equals(Constants.Beacon.State.BLUETOOTH_OFF) == true -> {
                    Constants.Beacon.State.BLUETOOTH_OFF
                }
                else -> {
                    Constants.Beacon.State.DISTANCE_OFF
                }
            }
        }
        if (beaconDevice!!.enablePhoneRing) {
//            if (SharedPreferenceManager.isDisconnected(beaconDevice?.macAddress.toString())) {
//                SharedPreferenceManager.isDisconnected(
//                    false,
//                    beaconDevice?.macAddress.toString()
//                )
//                phoneAlert("2")
//            }
            if (!isPhoneAlert) {
                isPhoneAlert = true

            }
        }


        if (newState == 0) {
            CoroutineScope(Dispatchers.IO).launch {
                ApiLiveData.disconnect(beaconDevice?.macAddress.toString())
            }

            if (!SharedPreferenceManager.isDisconnected(beaconDevice?.macAddress.toString())) {
                SharedPreferenceManager.isDisconnected(
                    true,
                    beaconDevice?.macAddress.toString()
                )
                handlerDisconnected.removeCallbacksAndMessages(null)
                handlerDisconnected.postDelayed({
                    phoneAlert("2")
                }, 1000)
            }
        }

        if (status != BluetoothGatt.GATT_SUCCESS) {
            if (beaconDevice?.connectionState?.equals(Constants.Beacon.State.DISCONNECTED) != true) {
                if (beaconDevice?.enablePhoneRing == true) {
                    if (nTimesRing < 3) {
                        reConnect()
                        log("140")
                        ringUnRingDevice("1")
                        nTimesRing++
                    } else {
                        beaconDevice?.isPhoneAlerting = false
                    }
                }
            }
            setLastLocation()
        } else {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                CoroutineScope(Dispatchers.IO).launch {
                    ApiLiveData.connect(beaconDevice?.macAddress.toString())
                }
                SharedPreferenceManager.isDisconnected(false, beaconDevice?.macAddress.toString())
                nTimesRing = 0
                beaconDevice?.isPhoneAlerting = false
                removetrackerOFF(beaconDevice?.macAddress!!)
                unRingDevice()

                if (!gatt.discoverServices()) {

                }
            } else if (newState == BluetoothProfile.STATE_CONNECTING) {
                nTimesRing = 0
                beaconDevice?.isPhoneAlerting = false
                unRingDevice()
            } else {
                DebugLogger.d(TAG, "onConnectionStateChange => gatt check => $status => $newState")
                if (beaconDevice?.connectionState?.equals(Constants.Beacon.State.DISCONNECTED) != true) {
                    if (beaconDevice?.enablePhoneRing == true) {

                        if (nTimesRing < 3) {
                            log("171")
                            ringUnRingDevice("2")
                            nTimesRing++
                        } else {
                            beaconDevice?.isPhoneAlerting = false
                        }
                    }
                }
                setLastLocation()
            }
        }
        updateDevice()
        if (newState == BluetoothProfile.STATE_CONNECTED) {
            SharedPreferenceManager.connectedTimeConnect(
                beaconDevice!!.macAddress,
                System.currentTimeMillis().toString()
            )
            if (SharedPreferenceManager.arrivalAlert(beaconDevice!!.macAddress)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    val channel = NotificationChannel(
                        mContext.getString(R.string.channel_name), "Tracker Alert",
                        NotificationManager.IMPORTANCE_HIGH
                    )
                    val manager =
                        mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    manager.createNotificationChannel(channel)
                }
                val builder = NotificationCompat.Builder(
                    mContext,
                    mContext.getString(R.string.channel_name)
                )
                val snoozeIntent = Intent(
                    mContext,
                    NavigationActivity::class.java
                )
                snoozeIntent.flags =
                    Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_REORDER_TO_FRONT

                builder.setContentTitle(mContext.getString(R.string.app_name))
                builder.setContentText("'" + beaconDevice!!.bleDeviceName + "' is arrived.")
                builder.setSmallIcon(R.mipmap.appicon)
                builder.setAutoCancel(true)
                val notificationManager =
                    mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.notify(1741, builder.build())
                //  startArrivalVoice()
            }
        }
    }

    var t1: TextToSpeech? = null
    private fun startArrivalVoice() {
        val notification =
            Uri.parse("android.resource://" + mContext.packageName.toString() + "/" + R.raw.arrivaltone)
        val r = RingtoneManager.getRingtone(
            mContext,
            notification
        )
        r.play()
        CoroutineScope(Dispatchers.Main).launch {
            delay(3000)
            Log.d(TAG, "startArrivalVoice: Starte voicess")
            r.stop()
            t1 = TextToSpeech(mContext) { status ->
                if (status != TextToSpeech.ERROR) {
                    t1?.language = Locale.US
                    t1?.speak(
                        "Your " + beaconDevice!!.bleDeviceName + " has arrived.",
                        TextToSpeech.QUEUE_ADD, null, null
                    )
                }
            }
        }
        Handler(Looper.myLooper()!!).postDelayed({
            Log.d(TAG, "startArrivalVoice: Starte voicess")
            r.stop()
            t1 = TextToSpeech(mContext) { status ->
                if (status != TextToSpeech.ERROR) {
                    t1?.language = Locale.US
                    Handler(Looper.myLooper()!!).postDelayed({
                        t1?.speak(
                            "Your " + beaconDevice!!.bleDeviceName + " has arrived.",
                            TextToSpeech.QUEUE_ADD, null, null
                        )
                    }, 1000)
                }
            }
        }, 3000)
    }

    fun setLastLocation() {
        Log.d(TAG, "setLastLocation: ")
        if (ActivityCompat.checkSelfPermission(
                mContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                mContext,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }

        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location == null) {
                    requestNewLocationData()
                } else {
                    sendLocationUpdate(location!!)
                }
            }
    }

    private fun requestNewLocationData() {
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext)
        if (ActivityCompat.checkSelfPermission(
                mContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                mContext,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLocationClient!!.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location = locationResult.lastLocation
            if (mLastLocation != null) {
                sendLocationUpdate(mLastLocation)
            }

        }
    }

    private fun sendLocationUpdate(location: Location) {
        // Got last known location. In some rare situations this can be null.
        Log.d(TAG, "sendLocationUpdate: location $location")
        beaconDevice?.latitude = location?.latitude.toString()
        beaconDevice?.longitude = location?.longitude.toString()
        var connection = "disconnected"

        if (beaconDevice?.isConnected() == true) {
            connection = "connected"
        }
        updateDevice()
    }

    override fun onCharacteristicRead(
        gatt: BluetoothGatt?,
        characteristic: BluetoothGattCharacteristic?,
        status: Int
    ) {
        val uuid = characteristic?.getUuid()!!
        val uuid2 = characteristic?.getService().getUuid()!!
        Log.d(
            "Nirav",
            "onCharacteristicRead : ${
                (characteristic.getValue().asUByteArray()
                    .joinToString { it.toString(16).padStart(1, '0') })
            } for UUID : ${uuid}"
        )
        if (status == BluetoothGatt.GATT_SUCCESS) {
            if (uuid2.equals(Constants.UUIDs.CUSTOM_SERVICE) && uuid.equals(Constants.UUIDs.CHAR_DEV_INFO_UUID)) {
                Log.d("Nirav", "Inside CHAR_DEV_INFO_UUID")
                startEnableLockNotifiation()

                if (readDevInfo(characteristic.getValue()) == null) {
                    Log.i("Nirav", "readDevInfo == null")
                    return;
                }

                //Log.d("Nirav","Service Id ${Constants.UUIDs.CUSTOM_SERVICE}")
                //Log.d("Nirav","Characteristic ID ${Constants.UUIDs.CHAR_DEV_INFO_UUID}")
            } else if (uuid.equals(Constants.UUIDs.CHAR_LOCK_NOTY_UUID)) {

                Log.d(
                    "Nirav",
                    "Response : ${
                        (characteristic.getValue().asUByteArray()
                            .joinToString { it.toString(16).padStart(1, '0') })
                    }"
                )
            } else {
                Log.d(
                    TAG,
                    "characteristic.getStringValue(0) BATTERY LEVEL => " + characteristic!!.getIntValue(
                        BluetoothGattCharacteristic.FORMAT_UINT8,
                        0
                    )
                )
                Log.d(
                    TAG,
                    "onCharacteristicRead: " + characteristic.getIntValue(
                        BluetoothGattCharacteristic.FORMAT_UINT8,
                        0
                    )
                )

                addtrackerBattery(
                    beaconDevice!!.macAddress,
                    characteristic!!.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0)
                        .toString()
                )

            }
        }
    }

    fun startChangePassword(str: String, str2: String): Boolean {
        return try {
            val bArr = ByteArray(7)
            val bytes = str2.toByteArray()
            val bytes2 = str.toByteArray()
            bArr[0] = 54
            var i = 0
            while (i < 6) {
                val i2 = i + 1
                bArr[i2] = (bytes2[i] xor bytes[i]) as Byte
                i = i2
            }
            val characteristicByID = getCharacteristicByID(
                Constants.UUIDs.CUSTOM_SERVICE,
                Constants.UUIDs.CHAR_DEV_CFG_UUID
            )
            if (characteristicByID == null) {
                Log.e(TAG, ":startChangePassword getCharacteristicByID failed")
                return false
            }
            characteristicByID.value = bArr
            if (!mBluetoothGatt!!.writeCharacteristic(characteristicByID)) {
                Log.e(TAG, ":startChangePassword writeCharacteristic failed")
                return false
            }
            true
        } catch (e: java.lang.RuntimeException) {
            Log.e(TAG, "catch exception: Find gatt failed" + e.message)
            false
        }
    }

    fun readDevInfo(bArr: ByteArray) {
        /*val temp : String
        val temp1 : String
        val temp2 : String*/
        //temp = bArr.asUByteArray().joinToString { it.toString(16).padStart(2,'0')}
        //Log.i("nirav", "byte array ${temp}")
        //temp1 = bArr.asUByteArray().copyOfRange(4,10).joinToString { it.toString(16).padStart(2,'0')}
        //Log.i("nirav","getMacAddress ${temp1}")
        //temp2 = bArr.asUByteArray().copyOfRange(15,19).joinToString { it.toString(16).padStart(2,'0')}
        //Log.i("nirav","getRandom ${temp2}")
        bArr.copyInto(this.mAuthLastMacAddr, 0, 4, 10)
        Log.i("nirav-brr", " brr1  ${bArr}")
        bArr.copyInto(mAuthLastRandom, 0, 15, 19)
        Log.i("nirav-brr", " brr2  ${bArr}")

        Log.i("Nirav", "size of ${this.mAuthLastMacAddr.size}")
        //Log.i("Nirav","size of ${this.mAuthLastRandom.size}")

        //val temp5 : String = mAuthLastMacAddr.asUByteArray().joinToString { it.toString(16).padStart(2,'0') }
        //Log.i("Nirav","mAuthLastMacAddr ${temp5}")
        //val temp6 : String = mAuthLastRandom.asUByteArray().joinToString { it.toString(16).padStart(2,'0') }
        //Log.i("Nirav","mAuthLastMacAddr ${temp6}")


    }

    override fun onCharacteristicWrite(
        gatt: BluetoothGatt?,
        characteristic: BluetoothGattCharacteristic?,
        status: Int
    ) {
        if (isNotSameDevice(gatt)) return
        val serviceUuid = characteristic?.service?.uuid
        val charUuid = characteristic?.uuid
        if (serviceUuid == Constants.UUIDs.CUSTOM_SERVICE && charUuid == Constants.UUIDs.CHARACTERISTIC_CUSTOM_VERIFIED) {
            if (beaconDevice != null) {
                SharedPreferenceManager.addOrUpdateDevice(beaconDevice!!)
                for (callback in callbacks) {
                    if (beaconDevice != null) {
                        callback.onDeviceAdded(beaconDevice!!)
                    }
                }
            }
            if (gatt != null) {
                enableKeyPressNotification(gatt)
                setLastLocation()
                val tmpBlePerp = gatt.device
                if (tmpBlePerp.uuids != null) {
                    for (uuid in tmpBlePerp.uuids) {
                        DebugLogger.d(TAG, "uuid => ${uuid.uuid?.toString()}")
                    }
                }
            }
            if (rssiHandler == null || rssiHandlerThread == null || rssiRunnanle == null) {
                if (rssiRunnanle != null) {
                    rssiHandler?.removeCallbacks(rssiRunnanle!!)
                }
                rssiHandlerThread = HandlerThread("$TAG${beaconDevice?.macAddress}")
                rssiHandlerThread!!.start()
                rssiHandler = Handler(rssiHandlerThread!!.looper)
                rssiRunnanle = RssiRunnanle()
                rssiHandler?.post(rssiRunnanle!!)
            }
        }
    }

    override fun onCharacteristicChanged(
        gatt: BluetoothGatt?,
        characteristic: BluetoothGattCharacteristic?
    ) {
        super.onCharacteristicChanged(gatt, characteristic)
        DebugLogger.d(TAG, gatt?.toString() ?: "")
        if (isNotSameDevice(gatt)) return
        val charUuid = characteristic!!.uuid
        if (charUuid == Constants.UUIDs.CHARACTERISTIC_KEY_PRESS_UUID) {
            val keyPressValue =
                characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0)
            Log.d(TAG, "onCharacteristicChanged: data $keyPressValue")
            mKeyNotify = if (keyPressValue == 2) {
                Constants.Beacon.KEY_HOLD_NOTIFY
            } else {
                if (mode.equals(Constants.Beacon.Mode.BEACON)) {
                    if (keyPressValue == 8) {
                        log("310")
                        ringUnRingDevice("3")
                    }
                } else {
                    for (callback in callbacks) {
                        if (beaconDevice?.macAddress?.equals(gatt?.device?.address) == true && callback is CameraFragment) {
                            callback.onBeaconButtonPress(beaconDevice!!)
                            Log.d(TAG, "onCharacteristicChanged: runned")
                            break
                        }
                    }
                }
                Constants.Beacon.KEY_PRESS_NOTIFY
            }
        }
    }


    private fun startEnableLockNotifiation() {
        Log.e(TAG, ":startEnableLockNotifiation ")
        val characteristicByID = getCharacteristicByID(
            Constants.UUIDs.CUSTOM_SERVICE,
            Constants.UUIDs.CHAR_LOCK_NOTY_UUID
        )
        if (characteristicByID == null) {
            Log.e(TAG, ":startWritePassword get error characteristic == null")

        } else if (!mBluetoothGatt!!.setCharacteristicNotification(characteristicByID, true)) {
            Log.e(TAG, ":setCharacteristicNotification get error characteristic == null")

        } else {
            val descriptor =
                characteristicByID.getDescriptor(Constants.UUIDs.CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID)
            if (descriptor == null) {
                Log.e(TAG, ":getDescriptor failed")

                return
            }
            descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            if (!mBluetoothGatt!!.writeDescriptor(descriptor)) {
                Log.e(TAG, ":writeDescriptor failed")

            }
        }
    }

    private fun ringUnRingDevice(value: String) {
        val time = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            DateTimeFormatter.ofPattern("HH:mm")
                .withZone(ZoneOffset.UTC)
                .format(Instant.now()).toString().replace("0", "")
        } else {
            SimpleDateFormat("HH:mm").format(Date()).toString().replace("0", "")
        }
        log("ringUnRingDevice: ${beaconDevice!!.isConnected()}")
        if (!SharedPreferenceManager.getFindmyPhone(beaconDevice!!.macAddress)!! && beaconDevice!!.isConnected()) {
            Log.d(
                TAG,
                "ringUnRingDevice: " + SharedPreferenceManager.getToTimer() + " " + SharedPreferenceManager.getFromTimer() + " " + time
            )
            try {
                if (SharedPreferenceManager.getToTimer()!! < time && SharedPreferenceManager.getFromTimer()!! > time) {
                    Log.d(TAG, "ringUnRingDevice: ")
                } else {
                    var ringtone = RingDevice.getRingtone(mContext)
                    val audioManager = RingDevice.getAudioManager(mContext)
                    if (audioManager != null && ringtone != null) {
                        if (ringtone.isPlaying) {
                            unRingDevice()
                            val notificationManager =
                                mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
                            notificationManager!!.cancel(1)
                        } else {
                            Log.d("TAG", "ringUnRingDevice: ")
                            val wifiManager =
                                mContext.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager?
                            val info = wifiManager!!.connectionInfo
                            val ssid = info.ssid
                            val checkhome = SharedPreferenceManager.getTrustedwifi(
                                mContext.resources.getString(R.string.home)
                            )
                            val checkoffice = SharedPreferenceManager.getTrustedwifi(
                                mContext.resources.getString(R.string.office)
                            )
                            val checkother = SharedPreferenceManager.getTrustedwifi(
                                mContext.resources.getString(R.string.others)
                            )
                            if (checkhome == ssid) {
                                return
                            }
                            if (checkoffice == ssid) {
                                return
                            }
                            if (checkother == ssid) {
                                return
                            }
                            ringDevice()
                            createNotification("1", value)
                        }
                    }
                }

            } catch (e: KotlinNullPointerException) {
                if (SharedPreferenceManager.getToTimer()!! < time && SharedPreferenceManager.getFromTimer()!! > time) {
                    Log.d(TAG, "ringUnRingDevice: ")
                } else {
                    var ringtone = RingDevice.getRingtone(mContext)
                    val audioManager = RingDevice.getAudioManager(mContext)
                    if (audioManager != null && ringtone != null) {
                        if (ringtone.isPlaying) {
                            unRingDevice()
                            val notificationManager =
                                mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
                            notificationManager!!.cancel(1)
                        } else {
                            Log.d("TAG", "ringUnRingDevice: ")
                            val wifiManager =
                                mContext.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager?
                            val info = wifiManager!!.connectionInfo
                            val ssid = info.ssid
                            val checkhome = SharedPreferenceManager.getTrustedwifi(
                                mContext.resources.getString(R.string.home)
                            )
                            val checkoffice = SharedPreferenceManager.getTrustedwifi(
                                mContext.resources.getString(R.string.office)
                            )
                            val checkother = SharedPreferenceManager.getTrustedwifi(
                                mContext.resources.getString(R.string.others)
                            )
                            if (checkhome == ssid) {
                                return
                            }
                            if (checkoffice == ssid) {
                                return
                            }
                            if (checkother == ssid) {
                                return
                            }
                            ringDevice()
                            createNotification("1", value)
                        }
                    }
                }
            }
        }

    }

    fun createNotification(value: String, upper: String) {
        unRingListener().ringCallback(object : unRingListener.UnRing {
            override fun stop() {
                unRingDevice()
            }
        })
        if (isAppIsInBackground(mContext)) {
            val i = Intent(mContext, AlertDialogShowActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            i.putExtra("ring_alarm", "ring")
            i.putExtra("beacon_name", beaconDevice?.bleDeviceName)
            i.action = Intent.ACTION_MAIN;
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            mContext.startActivity(i)
            return
            val intent = Intent(mContext, NavigationActivity::class.java)
            intent.putExtra("tag8call", true)
            intent.putExtra("macaddress", beaconDevice?.macAddress)
            val uniqueInt = (System.currentTimeMillis() and 0xfffffff).toInt()
            val pendingIntent = PendingIntent.getActivity(
                mContext, uniqueInt /* Request code */, intent,
                PendingIntent.FLAG_MUTABLE
            )
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(
                    mContext.getString(R.string.channel_name), "Tracker Alert",
                    NotificationManager.IMPORTANCE_HIGH
                )
                val manager =
                    mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                manager.createNotificationChannel(channel)
            }
            val builder =
                NotificationCompat.Builder(mContext, mContext.getString(R.string.channel_name))
            builder.setContentTitle(mContext.getString(R.string.app_name))
            builder.setContentText("'" + beaconDevice!!.bleDeviceName + "' is calling you.")
            builder.setSmallIcon(R.mipmap.appicon)
            builder.setAutoCancel(true)
                .setContentIntent(pendingIntent)
            val notificationManager =
                mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            builder.build().flags = builder.build().flags or Notification.FLAG_AUTO_CANCEL
            notificationManager.notify(1, builder.build())
        } else {
            /*if(callback != null){
                log("callback $callback")
                callback!!.onActivityCallback(this)
            }*/
            Tag8Application.getInstance().getActivity().runOnUiThread {
                log("tag8 ${Tag8Application.getInstance().getActivity()}")
                Utils.createAlertDialog(
                    Tag8Application.getInstance().getActivity(),
                    mContext.getString(R.string.app_name),
                    "'" + beaconDevice!!.bleDeviceName + "' is calling you.",
                    "STOP",
                    "",
                    object : DialogButtonsCallBack {
                        override fun setCallback(isPositive: Boolean) {
                            unRingDevice()
                        }
                    })
            }
        }
    }

    private fun ringDevice() {
        Log.d(TAG, "ringDevice")
        var ringtone = RingDevice.getRingtone(mContext)
        val audioManager = RingDevice.getAudioManager(mContext)
        if (audioManager != null && ringtone != null) {
            val volume = audioManager.getStreamMaxVolume(AudioManager.STREAM_RING)
            audioManager.setStreamVolume(
                AudioManager.STREAM_ALARM,
                volume,
                AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE
            )
            if (ringtone != null) {
                ringtone!!.audioAttributes = AudioAttributes.Builder()
                    .setFlags(AudioAttributes.FLAG_AUDIBILITY_ENFORCED)
                    .setLegacyStreamType(AudioManager.STREAM_ALARM)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build()
            }
            ringtone?.play()
            beaconDevice?.isPhoneAlerting = true
            updateDevice()
        }
    }

    private fun ringDeviceDisconnect() {
        var ringtone = RingDevice.getRingtone(mContext)
        val audioManager = RingDevice.getAudioManager(mContext)
        if (audioManager != null && ringtone != null) {
            val volume = audioManager.getStreamMaxVolume(AudioManager.STREAM_RING)
            audioManager.setStreamVolume(
                AudioManager.STREAM_ALARM,
                volume,
                AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE
            )
            if (ringtone != null) {
                ringtone!!.audioAttributes = AudioAttributes.Builder()
                    .setFlags(AudioAttributes.FLAG_AUDIBILITY_ENFORCED)
                    .setLegacyStreamType(AudioManager.STREAM_ALARM)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build()
            }
            ringtone?.play()
        }
    }

    fun unRingDevice() {
        //     mContext.stopService(Intent(mContext, RingtoneService::class.java))
        Log.d(TAG, "unRingDevice")
        var ringtone = RingDevice.getRingtone(mContext)
        val audioManager = RingDevice.getAudioManager(mContext)
        if (audioManager != null && ringtone != null) {
            if (ringtone!!.isPlaying) {
                ringtone!!.stop()
                beaconDevice?.isPhoneAlerting = false
                updateDevice()
            }
        }
    }

    override fun onReadRemoteRssi(gatt: BluetoothGatt?, rssi: Int, status: Int) {
        super.onReadRemoteRssi(gatt, rssi, status)
        if (isNotSameDevice(gatt)) return
        beaconDevice?.rssiResult = rssi
        if (beaconDevice != null) {
            for (callback in callbacks) {
                if (beaconDevice != null) {
                    log("440")
                    callback.onDeviceUpdated(beaconDevice!!)

                }
            }
        }
    }

    private fun isNotSameDevice(gatt: BluetoothGatt?): Boolean {
        val strMac = gatt?.device?.address
        if (strMac == null || strMac != beaconDevice?.macAddress) {
            return true
        }
        return false
    }

    private fun writePassword2Itrack(gatt: BluetoothGatt) {
        val service = gatt.getService(Constants.UUIDs.CUSTOM_SERVICE) ?: return
        val characteristic =
            service.getCharacteristic(Constants.UUIDs.CHARACTERISTIC_CUSTOM_VERIFIED)
                ?: return
        gatt.setCharacteristicNotification(characteristic, true)
        //set password

        characteristic.value = Constants.Beacon.DEFAULT_PASSWORD
        //21824521360
        //write password
        if (gatt.writeCharacteristic(characteristic)) {
            DebugLogger.d(TAG, "start write password success")
        }
//        CoroutineScope(Dispatchers.IO).launch {
//            delay(5000)
//            characteristic.value = Constants.Beacon.DEFAULT_PASSWORD_NAMECHANGE_
//            //21824521360
//            //write password
//            if (gatt.writeCharacteristic(characteristic)) {
//                DebugLogger.d(TAG, "start write password success")
//            }
//
//        }
    }

    private fun enableKeyPressNotification(gatt: BluetoothGatt) {
        log("enableKeyPressNotification")
        val service = gatt.getService(Constants.UUIDs.CHARACTERISTIC_KEY_PRESS_SRV_UUID) ?: return
        val characteristic =
            service.getCharacteristic(Constants.UUIDs.CHARACTERISTIC_KEY_PRESS_UUID)
                ?: return
        if (!gatt.setCharacteristicNotification(characteristic, true)) {
            return
        }
        val descriptor =
            characteristic.getDescriptor(Constants.UUIDs.CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID)
        descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
        if (!gatt.writeDescriptor(descriptor)) {
            log("inside write decscriptor")
        }
    }

    private fun enableDistanceAlert() {
        log("enableDistanceAlert")
        val byCallData = byteArrayOf(0x02)
        val callGatt = getCharacteristicByID(
            UUID.fromString(Constants.UUIDs.TI_KEYFOB_DISCONN_ALERT_SRV_UUID_STR),
            UUID.fromString(Constants.UUIDs.TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID)
        )
        if (callGatt != null) {
            mBluetoothGatt?.setCharacteristicNotification(callGatt, true)
            callGatt.value = byCallData
            var enable = mBluetoothGatt?.writeCharacteristic(callGatt) == true

            log("enable btn $enable")

            beaconDevice?.enableBeaconRing = true
            updateDevice()
        }

    }

    fun updateName() {
        val byCallData = byteArrayOf(0x6)
        val callGatt = getCharacteristicByID(
            UUID.fromString(Constants.UUIDs.Custom_Service),
            UUID.fromString(Constants.UUIDs.Custom_Service_uuid)
        )
        Log.d(TAG, "updateName: update runned")
        if (callGatt != null) {
            mBluetoothGatt?.setCharacteristicNotification(callGatt, true)
            callGatt.value = byCallData
            var enable = mBluetoothGatt?.writeCharacteristic(callGatt) == true

            Log.d(TAG, "updateName: update $enable")
        }

    }

    private fun disableDistanceAlert() {
        val byCallData = byteArrayOf(0x0)
        val callGatt = getCharacteristicByID(
            UUID.fromString(Constants.UUIDs.TI_KEYFOB_DISCONN_ALERT_SRV_UUID_STR),
            UUID.fromString(Constants.UUIDs.TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID)
        )
        if (callGatt != null) {
            mBluetoothGatt?.setCharacteristicNotification(callGatt, true)

            callGatt.value = byCallData
            var enablebeacon = mBluetoothGatt?.writeCharacteristic(callGatt) != true
            log("enbale beacon tagx $enablebeacon")
            beaconDevice?.enableBeaconRing = enablebeacon
        }
        updateDevice()
    }

    fun connect(): Boolean {
        val device = mBluetoothAdapter?.getRemoteDevice(beaconDevice?.macAddress)
        mBluetoothGatt = device?.connectGatt(mContext, true, this)
        beaconDevice?.connectionState = Constants.Beacon.State.CONNECTING
        return true
    }

    fun reConnect() {
        val device = mBluetoothAdapter?.getRemoteDevice(beaconDevice?.macAddress)
        beaconDevice?.connectionState = Constants.Beacon.State.CONNECTING
        mBluetoothGatt = device?.connectGatt(mContext, true, this)
    }

    private fun getCharacteristicByID(srvUUID: UUID, charaID: UUID): BluetoothGattCharacteristic? {
        if (mBluetoothGatt == null) {
            return null
        }
        val service: BluetoothGattService = mBluetoothGatt?.getService(srvUUID) ?: return null
        return service.getCharacteristic(charaID)
    }

    fun disconnect(isDelete: Boolean) {
        log("disconnect")
        if (turnOffTrack(isDelete)) {
            mBluetoothGatt?.disconnect()
            mBluetoothGatt?.close()
            mBluetoothGatt = null
            beaconDevice?.connectionState = Constants.Beacon.State.DISCONNECTED


            if (rssiRunnanle != null) {
                rssiHandler?.removeCallbacks(rssiRunnanle!!)
                rssiHandlerThread?.quitSafely()
            }
            rssiHandlerThread = null
            rssiRunnanle = null
            rssiHandler = null
            log("Delete tracker")
            updateDevice()
            setLastLocation()
        }
    }

    fun delete() {
        beaconDevice = null
        disconnect(true)
    }


    private val mAuthLastMacAddr = ByteArray(6)
    private val mAuthLastRandom = ByteArray(4)
    fun startWritePassword2Lock(passcode: String): Boolean {

        Log.d("Nirav", "writePasswordToLock address runned ")
        val characteristicByID = getCharacteristicByID(
            Constants.UUIDs.CUSTOM_SERVICE,
            Constants.UUIDs.CHARACTERISTIC_CUSTOM_VERIFIED
        )

        Log.d("Nirav", "writePasswordToLock address CharacteristicId $characteristicByID")


        //Log.d("Nirav","Service ${Constants.UUIDs.CUSTOM_SERVICE} , Characteristic ${Constants.UUIDs.CHARACTERISTIC_CUSTOM_VERIFIED}")
        if (characteristicByID == null) {
            Log.d("Nirav", "writePasswordToLock address CharacteristicId Null")
            return false
        } else {
            try {
                Log.d("Nirav", "writePasswordToLock address Passcode : $passcode")
                val allocate = ByteBuffer.allocate(16)
                allocate.put(mAuthLastMacAddr)
                allocate.put(passcode.toByteArray())
                allocate.put(mAuthLastRandom)
                val instance = MessageDigest.getInstance("MD5")
                instance.update(allocate.array())
                val digest = instance.digest()
                val bArr = ByteArray(17)
                bArr[0] = 1
                var str2 = ""
                var i = 0
                while (i < 16) {
                    val i2 = i + 1
                    bArr[i2] = digest[i]
                    str2 += String.format("%02x", java.lang.Byte.valueOf(digest[i]))
                    i = i2
                }
                characteristicByID.value = bArr
                if (!mBluetoothGatt!!.writeCharacteristic(characteristicByID))
                    Log.d(
                        TAG,
                        java.lang.String.format(
                            "%s: startWritePassword write characteristic failed:",
                            this.mAuthLastMacAddr
                        )
                    )
                Log.d("Nirav", "writePasswordToLock address Passcode---- : $passcode")

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return true
    }


    fun unlockLockDevice() {
        Log.d("Nirav", "unlockLockDevice")
        val byCallData = byteArrayOf(0x11, 0x02)
        val callGatt =
            getCharacteristicByID(Constants.UUIDs.CUSTOM_SERVICE, Constants.UUIDs.CHAR_DEV_CFG_UUID)
        if (callGatt != null) {
            Log.d("Nirav", "(callGatt != null)")
            mBluetoothGatt?.setCharacteristicNotification(callGatt, true)

            callGatt.value = byCallData

            if (mBluetoothGatt?.writeCharacteristic(callGatt) == true) {
                Log.d("Nirav", "(mBluetoothGatt?.writeCharacteristic(callGatt) == true")
                beaconDevice?.isDeviceLocked = false
                updateDevice()
            }
        }
    }

    private fun turnOffTrack(isDelete: Boolean): Boolean {
        Log.d(TAG, "turnOffTrack: isDelete $isDelete")
        var byCallData = byteArrayOf(0x03)
        if (isDelete) {
            byCallData = byteArrayOf(0x04)
        }

        val bluetoothGattChar: BluetoothGattCharacteristic? = getCharacteristicByID(
            Constants.UUIDs.TI_KEYFOB_DISCONN_ALERT_SRV_UUID,
            UUID.fromString(Constants.UUIDs.TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID)
        )
        Log.d(TAG, "turnOffTrack: bluetoothGattChar $bluetoothGattChar ")
        if (bluetoothGattChar != null) {
            mBluetoothGatt?.setCharacteristicNotification(bluetoothGattChar, true)

            bluetoothGattChar.value = byCallData
            if (bluetoothGattChar.getProperties() and BluetoothGattCharacteristic.PROPERTY_WRITE == 0
                && (bluetoothGattChar.getProperties()
                        and BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) == 0
            ) {
                Log.d(TAG, "turnOffTrack: writecha inside if")
                return false
            }
            Log.d(TAG, "turnOffTrack: writecha ")
            if (mBluetoothGatt?.writeCharacteristic(bluetoothGattChar) == true) {
                Log.d(TAG, "turnOffTrack: writecha inside ")
                return true
            }
        }
        return false
    }

    fun isConnected() =
        beaconDevice?.connectionState?.equals(Constants.Beacon.State.CONNECTED) == true

    fun updateDeviceRssi(nNewRssi: Int) {
        if (!isConnected()) {
            connect()
        }
        beaconDevice?.rssiResult = if (beaconDevice?.rssiResult == 0) {
            nNewRssi
        } else {
            ((beaconDevice?.rssiResult ?: 0) + nNewRssi) / 2
        }
        beaconDevice?.lastUpdateTick = System.currentTimeMillis()
        beaconDevice?.rssiResult = (beaconDevice?.rssiResult ?: 0)


    }

    fun updateDevice(beaconDevice: BeaconDevice) {
        log("updateDevice")
        this.beaconDevice?.type = beaconDevice.type
        this.beaconDevice?.bleDeviceName = beaconDevice.bleDeviceName
        updateDevice()
    }

    fun updateEnableBeaconDevice(enablePhoneRing: Boolean, enableBeaconRing: Boolean) {
        this.beaconDevice?.enablePhoneRing = enablePhoneRing
        this.beaconDevice?.enableBeaconRing = enableBeaconRing
        Log.d("Tag8bluetoothgatt", "updateEnableBeaconDevice: beacon ring" + enableBeaconRing)
        if (enableBeaconRing) {
            enableDistanceAlert()
        } else {
            disableDistanceAlert()
        }
    }

    fun makeTrackAlertOnOff() {
        try {
            if (!isConnected()) {
                return
            }
            if (beaconDevice?.isBeaconAlerting == true) {
                makeTrackAlertOff()
            } else {
                makeTrackAlertOn()
            }
        } catch (e: RuntimeException) {

        }
    }

    private fun makeTrackAlertOn() {
        Log.d(TAG, "makeTrackAlertOn 731")
        val byCallData = byteArrayOf(0x02)
        val callGatt = getCharacteristicByID(
            UUID.fromString(Constants.UUIDs.TI_KEYFOB_PROXIMITY_ALERT_UUID),
            UUID.fromString(Constants.UUIDs.TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID)
        )
        if (callGatt != null) {
            mBluetoothGatt?.setCharacteristicNotification(callGatt, true)

            callGatt.value = byCallData

            if (mBluetoothGatt?.writeCharacteristic(callGatt) == true) {
                beaconDevice?.isBeaconAlerting = true
                updateDevice()
            }
        }
    }

    private fun updateDevice() {
        if (beaconDevice != null) {
            beaconDevice?.lastUpdated = System.currentTimeMillis()
            SharedPreferenceManager.addOrUpdateDevice(beaconDevice!!)
            for (callback in callbacks) {
                log("716")
                callback.onDeviceUpdated(beaconDevice!!)
            }
        }
    }


    fun getBattery(macAddress: String): Boolean {
        var isBatteryLevel = false
        if (beaconDevice?.macAddress?.equals(macAddress) == true) {
            val batteryService: BluetoothGattService? =
                mBluetoothGatt?.getService(UUID.fromString(Constants.UUIDs.Battery_Service_UUID))
            if (batteryService == null) {
                Log.d(TAG, "Battery service not found!");
            } else {
                val batteryLevel: BluetoothGattCharacteristic? =
                    batteryService?.getCharacteristic(UUID.fromString(Constants.UUIDs.Battery_Level_UUID))

                if (batteryLevel == null) {
                    Log.d(TAG, "Battery characteristics not found!");
                } else {
                    isBatteryLevel = mBluetoothGatt!!.readCharacteristic(batteryLevel)
                    Log.d(TAG, "getBattery: ${isBatteryLevel}")
                }

            }
        }
        return isBatteryLevel
    }

    private fun makeTrackAlertOff() {
        log("makeTrackAlertOff")
        val byCallData = byteArrayOf(0x0)
        val callGatt = getCharacteristicByID(
            UUID.fromString(Constants.UUIDs.TI_KEYFOB_PROXIMITY_ALERT_UUID),
            UUID.fromString(Constants.UUIDs.TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID)
        )
        if (callGatt != null) {
            mBluetoothGatt?.setCharacteristicNotification(callGatt, true)

            callGatt.value = byCallData

            if (mBluetoothGatt?.writeCharacteristic(callGatt) == true) {
                beaconDevice?.isBeaconAlerting = false
                updateDevice()
            }
        }
    }

    fun getRssi() {
        val getData = SharedPreferenceManager.getAlertData(beaconDevice?.macAddress)

        var getLastAlert: String? = ""
        try {
            getLastAlert =
                SharedPreferenceManager.getLastAlertService(beaconDevice!!.macAddress)
        } catch (e: Exception) {
            e.message
        }
        var checkRssi = ""
        try {

            if (mBluetoothGatt?.readRemoteRssi() == true) {
                if (rssiRunnanle != null) {
                    rssiHandler?.postDelayed(rssiRunnanle!!, Constants.RSSI_RETRIEVAL_INTERVAL)
                    val currentTime: String =
                        SimpleDateFormat("mm", Locale.getDefault()).format(Date())
                    if (SharedPreferenceManager.getSnoozeHours(beaconDevice!!.macAddress) == currentTime) {
                        return
                    }
                    SharedPreferenceManager.addSnoozeHours(beaconDevice!!.macAddress, currentTime)
                    if (getData?.contains("Very Near")!! || getData.contains("Near")
                        || getData.contains("Far")
                    ) {
                        checkRssi = when {
                            (beaconDevice?.rssiResult ?: -100) >= -60 -> "Very Near"
                            (beaconDevice?.rssiResult ?: -100) >= -80 -> "Near"
                            else -> "Far"
                        }
                        Log.d(TAG, "getRssi: The data $getLastAlert $checkRssi")
                        if (getLastAlert == checkRssi) {
                            return
                        }
                        if (getData == "Very Near") {
                            if (checkRssi == "Near" || checkRssi == "Far") {
                                if (getLastAlert != checkRssi) {
                                    Log.d(TAG, "getRssi: The data 1 $getLastAlert $checkRssi")
                                    getLastAlert = checkRssi
                                    SharedPreferenceManager.addLastAlertService(
                                        beaconDevice!!.macAddress, getLastAlert
                                    )
                                    showAlertAlert(checkRssi)
                                } else {
                                    handler.removeCallbacksAndMessages(null)
                                    mContext.stopService(
                                        Intent(
                                            mContext,
                                            RingtoneService::class.java
                                        )
                                    )
//                                if (ringTone != null) {
//                                    ringTone.stop()
//                                    Log.d(TAG, "phoneAlert: ring")
//                                }
                                    SharedPreferenceManager.addLastAlertService(
                                        beaconDevice!!.macAddress,
                                        ""
                                    )
                                }
                            } else {
                                handler.removeCallbacksAndMessages(null)
                                mContext.stopService(Intent(mContext, RingtoneService::class.java))
                            }
                        } else {
                            handler.removeCallbacksAndMessages(null)
                        }

                        if (getData == "Far") {
                            if (getLastAlert != checkRssi) {
                                getLastAlert = checkRssi
                                SharedPreferenceManager.addLastAlertService(
                                    beaconDevice!!.macAddress, getLastAlert
                                )
                                Log.d(TAG, "getRssi: $getData")
                            } else {
                                handler.removeCallbacksAndMessages(null)
                            }
                            handler.removeCallbacksAndMessages(null)
                        } else {
                            handler.removeCallbacksAndMessages(null)
                        }
                    }
                }
            } else {
                SharedPreferenceManager.addShowNotiStatus(beaconDevice!!.macAddress, false)
                disconnect(false)
                val currentTime: String = SimpleDateFormat("HH", Locale.getDefault()).format(Date())
                if (SharedPreferenceManager.getSnoozeHours(beaconDevice!!.macAddress) == currentTime) {
                    return
                }
                if (getLastAlert == "disconnect") {
                    return
                }
                if (getData?.contains("Far") == true ||
                    getData?.contains("Very Near") == true ||
                    getData?.contains("Very Near") == true
                ) {
                    if (getLastAlert != "disconnect") {
                        getLastAlert = "disconnect"
                        SharedPreferenceManager.addLastAlertService(
                            beaconDevice!!.macAddress, getLastAlert
                        )
                        showAlertAlert(checkRssi)
                        SharedPreferenceManager.addLastAlertService(
                            beaconDevice!!.macAddress,
                            "disconnect"
                        )
                    } else {
                        handler.removeCallbacksAndMessages(null)
                    }
                }
            }

        } catch (e: Exception) {
            e.message
        }
    }

    private fun showAlertAlert(checkRssi: String) {
        SharedPreferenceManager.addLastAlertService(
            beaconDevice!!.macAddress,
            checkRssi
        )

        Log.d(TAG, "showAlertAlert: The data $checkRssi")
        playAlert()

        handler.postDelayed({
            SharedPreferenceManager.addLastAlertService(
                beaconDevice!!.macAddress,
                ""
            )
        }, 40000)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                mContext.getString(R.string.channel_name), "Tracker Alert",
                NotificationManager.IMPORTANCE_HIGH
            )
            val manager =
                mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(channel)
        }
        val builder = NotificationCompat.Builder(
            mContext,
            mContext.getString(R.string.channel_name)
        )
        val snoozeIntent = Intent(
            mContext,
            NavigationActivity::class.java
        )
        snoozeIntent.flags =
            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_REORDER_TO_FRONT

        snoozeIntent.putExtra("stop_snooze", "stop")
        snoozeIntent.putExtra("mac_address", beaconDevice!!.macAddress)
        val snoozePendingIntent = PendingIntent.getBroadcast(
            mContext,
            0,
            snoozeIntent,
            PendingIntent.FLAG_MUTABLE
        )

        builder.setContentTitle(mContext.getString(R.string.app_name))
        builder.setContentText("'" + beaconDevice!!.bleDeviceName + "' is $checkRssi from your device.")
        builder.setSmallIcon(R.mipmap.appicon)
        builder.setAutoCancel(true)
        builder.addAction(0, "DND for an hour", snoozePendingIntent)
        val notificationManager =
            mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(141, builder.build())
    }

    fun updateBluetoothOff() {
        log("updateBluetoothOff")
        if (isConnected()) {
            this.beaconDevice?.connectionState = Constants.Beacon.State.BLUETOOTH_OFF
            updateDevice()
        }
        if (beaconDevice!!.enablePhoneRing) {
            phoneAlert("1")
        }
        //updateEnableBeaconDevice(beaconDevice!!.enablePhoneRing, beaconDevice!!.enableBeaconRing)
    }

    private fun playAlert() {
        if (ringTone != null) {
            ringTone.play()
        }
        handler.postDelayed({
            if (ringTone != null) {
                ringTone.stop()
                Log.d(TAG, "phoneAlert: ring")
            }
        }, 20000)
    }

    private fun phoneAlert(num: String) {
        if (!SharedPreferenceManager.getFindmyPhone(beaconDevice!!.macAddress)!!) {
            if (beaconDevice != null && beaconDevice!!.bleDeviceName.isNotEmpty()) {
                Log.d(TAG, "phoneAlert: 931 runned $num")
                if (mBluetoothAdapter?.isEnabled == false)
                    return

                val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
                val player: MediaPlayer = MediaPlayer.create(
                    Tag8Application.getInstance().applicationContext,
                    notification
                )
                player.start()

                // updateEnableBeaconDevice(beaconDevice!!.enablePhoneRing, beaconDevice!!.enableBeaconRing)
                Handler(Looper.getMainLooper()).postDelayed({
                    if (player != null) {
                        player.stop()
                        Log.d(TAG, "phoneAlert: ring")
                        isPhoneAlert = false
                    }
                }, SharedPreferenceManager.getRingtoneDuration().toLong())
            }
        }

    }

    fun updateBluetoothOn() {
        Log.d(TAG, "updateBluetoothOff: ble on")
        if (beaconDevice?.connectionState == Constants.Beacon.State.BLUETOOTH_OFF) {
            connect()
        }

    }

    override fun equals(other: Any?): Boolean {
        return if (other is Tag8BluetoothGatt) {
            other.beaconDevice?.macAddress?.equals(this.beaconDevice?.macAddress) == true
        } else {
            super.equals(other)
        }
    }

    inner class RssiRunnanle : Runnable {
        override fun run() {
            log("rssi runnable")
            getRssi()
        }
    }

    fun toggleMode(macAddress: String) {
        if (this.beaconDevice?.macAddress?.equals(macAddress) == true) {
            mode = if (mode == Constants.Beacon.Mode.CAMERA) {
                Constants.Beacon.Mode.BEACON
            } else {
                Constants.Beacon.Mode.CAMERA
            }
        }
    }
}