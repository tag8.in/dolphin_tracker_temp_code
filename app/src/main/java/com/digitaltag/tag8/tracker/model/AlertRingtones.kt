package com.digitaltag.tag8.tracker.model

data class AlertRingtones(val title: String, val path: String)
