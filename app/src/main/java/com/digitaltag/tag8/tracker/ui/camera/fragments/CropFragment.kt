// "Therefore those skilled at the unorthodox
// are infinite as heaven and earth,
// inexhaustible as the great rivers.
// When they come to an end,
// they begin again,
// like the days and months;
// they die and are reborn,
// like the four seasons."
//
// - Sun Tsu,
// "The Art of War"
package com.digitaltag.tag8.tracker.ui.camera.fragments

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.ui.camera.CropImageActivity
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import com.theartofdev.edmodo.cropper.CropImageView.*

/** The fragment that will show the Image Cropping UI by requested preset.  */
class CropFragment : Fragment(), OnSetImageUriCompleteListener, OnCropImageCompleteListener {
    // region: Fields and Consts
    private var mCropImageView: CropImageView? = null
    private var cropImage: Button? = null
    private var mActivity: CropImageActivity? = null

    /** Set the image to show for cropping.  */
    fun setImageUri(imageUri: Uri?) {
        mCropImageView!!.setImageUriAsync(imageUri)
    }


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView: View = inflater.inflate(R.layout.activity_crop_image, container, false)
        return rootView
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mActivity = activity as CropImageActivity
        mActivity!!.setCurrentFragment(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mCropImageView = view.findViewById(R.id.cropImageView)
        cropImage = view.findViewById(R.id.crop)
        mCropImageView!!.setOnSetImageUriCompleteListener(this)
        mCropImageView!!.setOnCropImageCompleteListener(this)
        mActivity!!.setImageUri()
        cropImage!!.setOnClickListener { v: View? -> mCropImageView!!.getCroppedImageAsync() }
    }

    override fun onDetach() {
        super.onDetach()
        if (mCropImageView != null) {
            mCropImageView!!.setOnSetImageUriCompleteListener(null)
            mCropImageView!!.setOnCropImageCompleteListener(null)
        }
    }

    override fun onSetImageUriComplete(view: CropImageView?, uri: Uri?, error: Exception?) {
        if (error == null) {
            Toast.makeText(activity, "Image load successful", Toast.LENGTH_SHORT).show()
        } else {
            Log.e("AIC", "Failed to load image by URI", error)
            Toast.makeText(activity, "Image load failed: " + error.message, Toast.LENGTH_LONG)
                    .show()
        }
    }

    override fun onCropImageComplete(view: CropImageView, result: CropResult) {
        handleCropResult(result)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            handleCropResult(result)
        }
    }

    private fun handleCropResult(result: CropResult) {
        if (result.error == null) {
            val bitmap = CropImage.toOvalBitmap(result.bitmap)
            mActivity!!.setResult(bitmap)
        } else {
            Toast.makeText(
                    activity,
                    "Image crop failed: " + result.error.message,
                    Toast.LENGTH_LONG)
                    .show()
        }
    }

    companion object {
        // endregion
        /** Returns a new instance of this fragment for the given section number.  */
        @JvmStatic
        fun newInstance(): CropFragment {
            return CropFragment()
        }
    }
}