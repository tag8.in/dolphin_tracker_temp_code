package com.digitaltag.tag8.tracker.ui.ringtone

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.widget.NumberPicker
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.model.AlertRingtones
import com.digitaltag.tag8.tracker.util.setSafeOnClickListener
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_ringtone.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class RingtoneActivity : AppCompatActivity(), NotifyListener.Notify {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ringtone)
        Tag8Application.getInstance().setActivity(this)

        val adapters = RingtoneAdapters(this@RingtoneActivity, getNotificationsNames())

        /* val uri: String? = SharedPreferenceManager.getRingtoneUri()
         val intent = Intent(RingtoneManager.ACTION_RINGTONE_PICKER)
         intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION)
         intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone")
         if (uri != null) {
             intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Uri.parse(uri))
         }
         this.startActivityForResult(intent, 5)*/
        val minutes = SharedPreferenceManager.getRingtoneDuration() / 1000
        duration.text = "$minutes sec"

        NotifyListener().getNotifyListener(object : NotifyListener.Notify {
            @SuppressLint("NotifyDataSetChanged")
            override fun notify(value: String) {
                adapters.notifyDataSetChanged()
            }

        })

        backarrow.setOnClickListener {
            finish()
        }
        ringtoneView.apply {
            adapter = adapters
        }

        materialCardView.setOnClickListener {
            val bottomDialog = BottomSheetDialog(this)
            bottomDialog.setContentView(R.layout.ringtone_duartion_dialog)

            bottomDialog.setOnDismissListener {
                val minutes = SharedPreferenceManager.getRingtoneDuration() / 1000
                duration.text = "$minutes sec"
            }

            val picker: NumberPicker? = bottomDialog.findViewById(R.id.picker)
            val done: AppCompatTextView? = bottomDialog.findViewById(R.id.done)
            done?.setSafeOnClickListener {
                bottomDialog.dismiss()
            }
            val data = arrayOf("3 sec", "5 sec", "10 sec", "15 sec", "30 sec")
            picker?.minValue = 0
            picker?.maxValue = data.size - 1
            picker?.displayedValues = data
            picker?.setOnValueChangedListener { _, oldVal, newVal ->
                val text = data[newVal]
                updatedDuration(text)
            }
            bottomDialog.show()
        }
    }

    private fun updatedDuration(duration: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val text = duration.replace("sec", "").trim().toInt() * 1000
            SharedPreferenceManager.addRingtoneDuration(text)
        }
    }

    override fun onResume() {
        super.onResume()
        Tag8Application.getInstance().setActivity(this)
    }

    private fun getNotificationsNames(): ArrayList<AlertRingtones> {
        val manager = RingtoneManager(this)
        val lists: ArrayList<AlertRingtones> = ArrayList()
        manager.setType(RingtoneManager.TYPE_RINGTONE)
        val cursor = manager.cursor
        val list: MutableMap<String, String> = HashMap()
        while (cursor.moveToNext()) {
            val notificationTitle = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX)
            val notificationUri =
                cursor.getString(RingtoneManager.URI_COLUMN_INDEX) + "/" + cursor.getString(
                    RingtoneManager.ID_COLUMN_INDEX
                )
            lists.add(AlertRingtones(notificationTitle, notificationUri))
            list[notificationTitle] = notificationUri
        }
        return lists
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 5) {
            val uri = data?.getParcelableExtra<Uri>(RingtoneManager.EXTRA_RINGTONE_PICKED_URI)
            val ringTone = RingtoneManager.getRingtone(applicationContext, uri)
            if (uri != null) {
                SharedPreferenceManager.setRingtoneUri(uri.toString())
                SharedPreferenceManager.setRingtoneName(ringTone.getTitle(this))
            } else {
                Toast.makeText(this, "unable to set ringtone", Toast.LENGTH_SHORT).show()
            }
            finish()
        } else {
            finish()
        }
    }

    override fun notify(value: String) {
        TODO("Not yet implemented")
    }

}