package com.digitaltag.tag8.tracker.ui.settings

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.util.setSafeOnClickListener
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_feedback.*


class FeedbackActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feedback)
        Tag8Application.getInstance().setActivity(this)
        auth = Firebase.auth
        val uid = auth.currentUser?.uid
        backarrow.setSafeOnClickListener {
            finish()
        }
        val email = auth.currentUser?.email.toString()
        emailtxt.setText(email)
        inputbtn.setSafeOnClickListener {
            if (feedbacktxt.text.toString().isEmpty()) {
                Toast.makeText(this, "Empty Feedback cannot be submitted.", Toast.LENGTH_LONG).show()
            } else {
                MaterialAlertDialogBuilder(this)
                        .setTitle(resources.getString(R.string.confirm))
                        .setMessage("Please confirm the e-mail address is correct, we will reply to you as soon as possible.")
                        .setNegativeButton(resources.getString(R.string.cancel)) { dialog, which ->
                            dialog.dismiss()
                        } .setPositiveButton(resources.getString(R.string.ok)) { dialog, which ->

                            val it = Intent(Intent.ACTION_SEND)
                            it.putExtra(Intent.EXTRA_EMAIL, arrayOf<String>("support@tag8.in"))
                            it.putExtra(Intent.EXTRA_SUBJECT, "Feedback")
                            it.putExtra(Intent.EXTRA_TEXT, feedbacktxt.text.toString())
                            it.type = "message/rfc822"
                            startActivity(Intent.createChooser(it, "Choose Mail App"))

                        } .show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        Tag8Application.getInstance().setActivity(this)
    }
}