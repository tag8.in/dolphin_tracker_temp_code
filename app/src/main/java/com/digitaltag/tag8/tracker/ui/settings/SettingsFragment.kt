package com.digitaltag.tag8.tracker.ui.settings

import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.media.RingtoneManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.digitaltag.tag8.tracker.NavigationActivity
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.Tag8Application
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.getFromTimer
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.getToTimer
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.removeFromTimer
import com.digitaltag.tag8.tracker.managers.SharedPreferenceManager.removeToTimer
import com.digitaltag.tag8.tracker.slider.SliderActivity
import com.digitaltag.tag8.tracker.ui.beacon.AboutActivity
import com.digitaltag.tag8.tracker.ui.beacon.NodisturbActivity
import com.digitaltag.tag8.tracker.ui.beacon.WifiActivity
import com.digitaltag.tag8.tracker.ui.ringtone.RingtoneActivity
import com.digitaltag.tag8.tracker.ui.signup.RegisterActivity
import com.digitaltag.tag8.tracker.util.setSafeOnClickListener
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.fragment_settings.*


class SettingsFragment : Fragment() {
    var mActivity: NavigationActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mActivity = requireActivity() as NavigationActivity
        logout.setSafeOnClickListener {
            Firebase.auth.signOut()
            val i = Intent(activity, SliderActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
        }
        trustedwifi.setSafeOnClickListener {
            val connectivityManager =
                requireActivity().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            val connected =
                connectivityManager!!.getNetworkInfo(ConnectivityManager.TYPE_WIFI)!!.state == NetworkInfo.State.CONNECTED
            if (connected) {
                val i = Intent(activity, WifiActivity::class.java)
                startActivity(i)
            } else {
                Toast.makeText(
                    requireContext(),
                    "You need to be connected to a wifi network.",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
        alertphone.setSafeOnClickListener {
            val intent = Intent(activity, RingtoneActivity::class.java)
            activity?.startActivity(intent)
        }

        editname.setSafeOnClickListener {
            Intent(requireActivity(), RegisterActivity::class.java).apply {
                putExtra("isSettings", "true")
            }.also {
                startActivity(it)
            }
        }
        fromtotimer.setSafeOnClickListener {
            SharedPreferenceManager.setFromTimer(fromtimershow.text.toString())
            SharedPreferenceManager.setToTimer(totimershow.text.toString())
            startActivity(Intent(context, NodisturbActivity::class.java))
        }
        feedback.setSafeOnClickListener {
            startActivity(Intent(context, FeedbackActivity::class.java))
        }
        about.setSafeOnClickListener {
            val intent = Intent(activity, AboutActivity::class.java)
            activity?.startActivity(intent)
        }

        sleeptimeswitch.isChecked = SharedPreferenceManager.sleepSwitch()

        sleeptimeswitch.setOnCheckedChangeListener { _, isChecked ->
            SharedPreferenceManager.sleepSwitch(isChecked)
            if (isChecked) {
                fromtotimer.visibility = VISIBLE
                textView4.visibility = VISIBLE
            } else {
                fromtotimer.visibility = GONE
                textView4.visibility = View.INVISIBLE
                removeFromTimer()
                removeToTimer()
            }
        }
    }


    override fun onResume() {
        super.onResume()
        Tag8Application.getInstance().setActivity(mActivity!!)
    }

    override fun onStart() {
        super.onStart()

        val givenString = SharedPreferenceManager.getRingtoneName()
        if (givenString != null && givenString!!.isNotEmpty()) {
            ringtonename.text = givenString
        }
        var isTrustedWifiEnable = SharedPreferenceManager.getTrustedwifiswitch()!!
        if (isTrustedWifiEnable) {
            trustedwifistatus.text = getString(R.string.trusted_wifi_on)
        } else {
            trustedwifistatus.text = getString(R.string.trusted_wifi_off)
        }

        if (getFromTimer().isNullOrEmpty() && getFromTimer().isNullOrEmpty()) {
            fromtotimer.visibility = GONE
            textView4.visibility = View.INVISIBLE
            sleeptimeswitch.isChecked = false
        } else {
            fromtotimer.visibility = VISIBLE
            textView4.visibility = VISIBLE
            sleeptimeswitch.isChecked = true
            fromtimershow.text = getFromTimer()
            totimershow.text = getToTimer()
        }
    }

    fun getNotifications(): Map<String, String>? {
        val manager = RingtoneManager(activity)
        manager.setType(RingtoneManager.TYPE_RINGTONE)
        val cursor: Cursor = manager.cursor
        val list: MutableMap<String, String> = HashMap()
        while (cursor.moveToNext()) {
            val notificationTitle: String = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX)
            val notificationUri: String = cursor.getString(RingtoneManager.URI_COLUMN_INDEX)
                .toString() + "/" + cursor.getString(RingtoneManager.ID_COLUMN_INDEX)
            list[notificationTitle] = notificationUri
        }
        return list
    }

}