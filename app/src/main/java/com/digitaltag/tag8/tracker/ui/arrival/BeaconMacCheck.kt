package com.digitaltag.tag8.tracker.ui.arrival

data class BeaconMacCheck(val macAddress: String)
