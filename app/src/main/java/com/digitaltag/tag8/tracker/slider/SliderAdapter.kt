package com.digitaltag.tag8.tracker.slider

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.viewpager.widget.PagerAdapter
import com.digitaltag.tag8.tracker.R

class SliderAdapter(private val context: Context) : PagerAdapter() {

    var layoutInflater: LayoutInflater? = null

    private var slide_images = intArrayOf(
            R.drawable.banner1,
            R.drawable.banner2
    )

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as ConstraintLayout
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = layoutInflater?.inflate(R.layout.slides_layout, container, false)!!
        val imageView = view.findViewById<ImageView>(R.id.mainimg)
        if(position < slide_images.size){
            imageView.setImageResource(slide_images[position])
        }
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as ConstraintLayout)
    }

    override fun getCount(): Int {
        return slide_images.size
    }
}