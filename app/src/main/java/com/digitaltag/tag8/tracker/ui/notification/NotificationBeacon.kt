package com.digitaltag.tag8.tracker.ui.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.digitaltag.tag8.tracker.R
import com.digitaltag.tag8.tracker.ble.BluetoothLeService
import com.digitaltag.tag8.tracker.constants.Constants
import com.digitaltag.tag8.tracker.constants.Constants.NotificationChannelBeacon.BEACON_NOTIFICATION_CHANNEL_DESCRIPTION
import com.digitaltag.tag8.tracker.slider.SliderActivity

class NotificationBeacon(val bluetoothLeService: BluetoothLeService) {

    private val TAG: String = NotificationBeacon::class.java.simpleName

    val notificationId = Constants.NotificationIds.BEACON_NOTIFICATION_ID

    private val channelId = Constants.NotificationChannelBeacon.BEACON_NOTIFICATION_CHANNEL_ID

    private val channelName = Constants.NotificationChannelBeacon.BEACON_NOTIFICATION_CHANNEL_NAME

    private val channelDescription =
        Constants.NotificationChannelBeacon.BEACON_NOTIFICATION_CHANNEL_NAME

    private val channelImportance =
        Constants.NotificationChannelBeacon.BEACON_NOTIFICATION_CHANNEL_IMPORTANCE

    private val requestCode = Constants.RequestCodes.BEACON_NOTIFICATION_REQUEST_CODE

    private val mNotificationManager =
        bluetoothLeService.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    fun getNotification(): Notification {
        return buildNotification().build()
    }

    fun buildNotification(): NotificationCompat.Builder {

        if (isAndroidOOrHigher()) {
            createChannel()
        }

        val builder = NotificationCompat.Builder(bluetoothLeService, channelId)

        builder.setContentTitle("Tag8")
            .setContentIntent(createContentIntent())
            .setColor(ContextCompat.getColor(bluetoothLeService, R.color.colorPrimary))
            .setSmallIcon(R.mipmap.appicon)
            .setContentText(BEACON_NOTIFICATION_CHANNEL_DESCRIPTION)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setVibrate(longArrayOf(0))

        return builder
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannel() {
        var channel = mNotificationManager.getNotificationChannel(channelId)
        if (channel == null) {
            channel = NotificationChannel(channelId, channelName, channelImportance)
            channel.description = channelDescription
            channel.enableLights(true)
            channel.lightColor = Color.RED
            channel.vibrationPattern = longArrayOf(0)
            channel.enableVibration(false)
            channel.setSound(null, null)
            mNotificationManager.createNotificationChannel(channel)
        } else {
            try {
                mNotificationManager.deleteNotificationChannel(channel.id)
                createChannel()
            } catch (e: Exception) {
                e.message
            }
        }
    }

    private fun isAndroidOOrHigher(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
    }

    private fun createContentIntent(): PendingIntent? {
        val openUI = Intent(bluetoothLeService, SliderActivity::class.java)
        openUI.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        openUI.action = "beacon_notification_click"
        return PendingIntent.getActivity(
            bluetoothLeService, requestCode, openUI,  PendingIntent.FLAG_MUTABLE
        )
    }

}