package com.digitaltag.tag8.tracker.ui.arrival

import com.digitaltag.tag8.tracker.model.BeaconDevice

interface ArrivalListener {
    fun connected(mac: BeaconDevice)
    fun dialog(mac: BeaconDevice)
    fun startScanning(device: BeaconDevice)
}